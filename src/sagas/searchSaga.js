
import {  call, put } from 'redux-saga/effects';
import Action from '../config/action';
import { placeAutoComplete,getWithLogin } from '../api/api'

function* searchapiCall(action){
  try {

    const response = yield call(placeAutoComplete, action);
    const entries = response.data;
    yield put({type:Action.SEARCH_SUCCESS,value:entries});
  } catch(error) {
  }
}


function* companyApiCall() {

  try {
      const response = yield call(getWithLogin, "get/all/company");
      const entries = response.data;
      yield put({ type: Action.GET_COMP_LIST_SUCCESS, value: entries});
  } catch (error) {
      console.log(error);
      var response = error.response.data;
      const entries = Object.values(response);
      if (entries && entries.length > 0) {
          yield put({ type: Action.GET_COMP_LIST_FAIL, value: entries[0][0] });
      } else {
          yield put({ type: Action.GET_COMP_LIST_FAIL, value: "Something went wrong. please try again." });
      }
  }

}


export function* search(action) {
    try {
      yield call(searchapiCall, action.value)
    }
    catch (error) {
      console.log(error);
    }
}



export function* getAllCompany() {
  try {
    yield call(companyApiCall)
  }
  catch (error) {
    console.log(error);
  }
}
