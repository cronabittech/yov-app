import { call, put } from 'redux-saga/effects';
import Action from '../config/action';
import {  postWithLogin } from '../api/api';


function* starAddApiCall(action) {
    try {
        const response = yield call(postWithLogin, 'star-rating/add', action);
        const entries = Object.values(response.data);
        yield put({ type: Action.ADD_STAR_RAING_SUCCESS, value: entries[0][0] });
    } catch (error) {
        var response = error.response.data;
        const entries = Object.values(response);
        if (entries && entries.length > 0) {
            yield put({ type: Action.ADD_STAR_RAING_FAIL, value: entries[0][0] });
        } else {
            yield put({ type: Action.ADD_STAR_RAING_FAIL, value: "Something went wrong. please try again." });
        }
    }
}

export function* resetAddStartRateState(){
    yield put({ type: Action.ADD_STAR_RAING_RESET});

}



export function* AddStarRating(action) {
    try {
        yield call(starAddApiCall, action.value)
    }
    catch (error) {
        console.log(error);
    }
}