import { call, put } from 'redux-saga/effects';
import Action from '../config/action';
import { getWithLogin } from '../api/api'

function* companyapiCall(action) {

    try {
        const response = yield call(getWithLogin, "company/add/" + action);
        const entries = response.data;
        yield put({ type: Action.GET_COMP_SUCCESS, value: entries});
    } catch (error) {
        console.log(error);
        var response = error.response.data;
        console.log(error.response);
        const entries = Object.values(response);
        if (entries && entries.length > 0) {
            yield put({ type: Action.GET_COMP_FAIL, value: entries[0][0] });
        } else {
            yield put({ type: Action.GET_COMP_FAIL, value: "Something went wrong. please try again." });
        }
    }
}

export function* getCompany(action) {
    try {
        yield call(companyapiCall, action.value)
    }
    catch (error) {
        console.log(error);
    }
}