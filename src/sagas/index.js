// Imports: Dependencies
import { all, takeLatest } from 'redux-saga/effects';
import Action from '../config/action';

// Imports: Redux Sagas
import { registerUser } from './registerSaga';
import { loginUser } from './loginSaga';
import { resetPassword } from './resetPasswordSaga';
import { search, getAllCompany } from './searchSaga';
import { getCompany } from './CompanyDetailSaga';
import { AddStarRating, resetAddStartRateState } from "./StarRatingSaga";

// Redux Saga: Root Saga
export function* rootSaga() {
  yield all([
    takeLatest(Action.REGISTER, registerUser),
    takeLatest(Action.LOGIN, loginUser),
    takeLatest(Action.RESETP, resetPassword),
    takeLatest(Action.SEARCH, search),
    takeLatest(Action.GET_COMP, getCompany),
    takeLatest(Action.GET_COMP_LIST, getAllCompany),
    takeLatest(Action.ADD_STAR_RAING, AddStarRating),
    takeLatest(Action.ADD_STAR_RAING_RESET, resetAddStartRateState),
  ])
};
