import { cos } from 'react-native-reanimated';
import { fork, takeLatest, call,take, put } from 'redux-saga/effects';
import Action from '../config/action';
import { postWithOutLogin } from '../api/api'

function* registerapiCall(action){
  try {

    const response = yield call(postWithOutLogin, '/register-app', action);
    const entries = Object.values(response.data);
    yield put({type:Action.REGISTER_SUCCESS,value:entries[0][0]});
  } catch(error) {
    var response = error.response.data;
    const entries = Object.values(response);
    if(entries && entries.length > 0){
      yield put({type:Action.REGISTER_FAIL,value:entries[0][0]});
    }else{
      yield put({type:Action.REGISTER_FAIL,value:"Something went wrong. please try again."});
    }
  }
}


export function* registerUser(action) {
    try {
      const token = yield call(registerapiCall, action.value)
    }
    catch (error) {
      console.log(error);
    }
}