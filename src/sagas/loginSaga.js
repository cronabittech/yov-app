import { cos } from 'react-native-reanimated';
import { fork, takeLatest, call,take, put } from 'redux-saga/effects';
import Action from '../config/action';
import { postWithOutLogin } from '../api/api'

function* loginapiCall(action){
  try {
    console.log('login call', action);
    const response = yield call(postWithOutLogin, '/login', action);
    const entries = Object.values(response.data);
    yield put({type:Action.LOGIN_SUCCESS,value:entries[0]});
  } catch(error) {
    console.log(error.response);
    var response = error.response.data;
    const entries = Object.values(response);
    if(entries && entries.length > 0){
      yield put({type:Action.LOGIN_FAIL,value:entries[0][0]});
    }else{
      yield put({type:Action.LOGIN_FAIL,value:"Something went wrong. please try again."});
    }
  }
}


export function* loginUser(action) {
    try {
      const token = yield call(loginapiCall, action.value)
    }
    catch (error) {
      console.log(error);
    }
}