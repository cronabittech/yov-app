import { cos } from 'react-native-reanimated';
import { fork, takeLatest, call,take, put } from 'redux-saga/effects';
import Action from '../config/action';
import { postWithOutLogin } from '../api/api'

function* resetapiCall(action){
  try {

    const response = yield call(postWithOutLogin, '/forgot-password', action);
    const entries = Object.values(response.data);
    yield put({type:Action.RESETP_SUCCESS,value:entries[0][0]});
  } catch(error) {
    var response = error.response.data;
    const entries = Object.values(response);
    if(entries && entries.length > 0){
      yield put({type:Action.RESETP_FAIL,value:entries[0][0]});
    }else{
      yield put({type:Action.RESETP_FAIL,value:"Something went wrong. please try again."});
    }
  }
}


export function* resetPassword(action) {
    try {
      const token = yield call(resetapiCall, action.value)
    }
    catch (error) {
      console.log(error);
    }
}