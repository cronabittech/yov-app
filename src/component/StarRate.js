/** @format */

import React from "react";
import StarRating from 'react-native-star-rating';
import Colors from '../config/colors';


const StarRate = (props) => {

    return <StarRate1 {...props} />;
};

const StarRate1 = (props) => (
    <StarRating
        disabled={props.disabled}
        maxStars={5}
        emptyStar={'star'}
        fullStar={'star'}
        halfStar={'star-half'}
        iconSet={'FontAwesome'}
        containerStyle={[{ justifyContent: 'flex-start'},props.containerStyle]}
        rating={props.rating}
        fullStarColor={Colors.orange}
        buttonStyle={[props.buttonStyle]}
        emptyStarColor={props.emptyStarColor ? props.emptyStarColor : Colors.lightGrey}
        starSize={props.starSize}
        selectedStar={(rating)=>props.selectedStar(props.order,rating)}
    />
)

export default StarRate;
