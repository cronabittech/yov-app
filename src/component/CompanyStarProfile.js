/** @format */

import React from "react";
import {
    Image,
    Pressable, StyleSheet,
    Text,
     View
} from "react-native";
import StarRating from 'react-native-star-rating';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import Button from "./Button";



const VibrateStart = () => {
    
}
const VibrateStop = () => {
   
}

const CompanyStarProfile = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.iconContainer}>
                <Image style={styles.icon} resizeMode="contain"
                    source={{ uri: props.icon }} />
            </View>
            <View style={{ flex: 1 }}>
                <View style={{ marginLeft: 10, flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Pressable
                        onPress={props.onHeaderClick}
                        onPressIn={() => VibrateStart()}
                        onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={pressed ? styles.headerContainerActive : styles.headerContainer}>
                                <Text numberOfLines={1} style={[pressed ? styles.headerActive : styles.header, { color: Colors.deepPurple }]}>
                                    {props.name}
                                </Text>
                                <Text numberOfLines={1} style={[pressed ? styles.subTitle : styles.subTitle, { color: Colors.lightGrey }]}>
                                 {props.type}
                                </Text>
                            </View>
                        )}
                    </Pressable>
                </View>
                <View style={{ marginTop: 5, marginLeft: 10, flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Pressable
                        onPress={props.onRecoClick}
                        onPressIn={() => VibrateStart()}
                        onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end' }}>
                                <View style={pressed ? styles.recoContinerActive : styles.recoContainer}>
                                    <View>
                                        <StarRating
                                            disabled={true}
                                            maxStars={5}
                                            emptyStar={'star'}
                                            fullStar={'star'}
                                            containerStyle={{ justifyContent: 'space-between', padding: 0, margin: 0 }}
                                            halfStar={'star-half'}
                                            iconSet={'FontAwesome'}
                                            fullStarColor={pressed ? Colors.white : Colors.orange}
                                            rating={props.rating}
                                            buttonStyle={{ marginLeft: 3 }}
                                            emptyStarColor={pressed ? Colors.white : Colors.lightGrey}
                                            starSize={18}
                                        />
                                    </View>
                                    <Text note note style={[pressed ? styles.buttonTextStyleActive : styles.buttonTextStyle, { color: Colors.lightGrey, alignItems: 'flex-end' }]}>
                                        {props.starDate}
                                    </Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginRight:-120}}>
                                    <Button style={{width:'27%'}} type="footerButtonNoicon" iconName="envelope"  onPress={props.mail}/>
                                    <Button style={{width:'27%'}} type="footerButtonNoicon" iconName="phone" onPress={props.phone} />
                                </View>
                            </View>
                        )}
                    </Pressable>
                </View>
            </View>
        </View>

    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        zIndex: 999999,
        padding: 7,
        marginBottom: 2,

    }, iconContainer: {
        width: 80,
        height: 80,
        justifyContent: 'center',
        elevation: 5,
        backgroundColor: Colors.lightGrey2,
        justifyContent: 'center',
        borderRadius: 10,
    },
    icon: {
        width: 80, height: 80, alignSelf: 'center'
    },
    header: {
        color: '#585858', width: 280, fontSize: FontSize.large, fontWeight: 'bold', flexWrap: 'wrap',
    },
    headerActive: {
        color: Colors.white, width: 230, fontSize: FontSize.large, fontWeight: 'bold', flexWrap: 'wrap',
    },
    subTitle: {
        marginBottom: 0, color: Colors.lessDarkGrey,
        maxWidth: 220, width: 220, flexWrap: 'wrap', flex: 1, fontSize: FontSize.medium, flexWrap: 'wrap', textAlign: 'left'
    }, subTitleActive: {
        opacity: 0,
        marginBottom: 0, color: Colors.white, maxWidth: 220, width: 220, flex: 1, flexWrap: 'wrap', fontSize: FontSize.medium, flexWrap: 'wrap', textAlign: 'left'
    }, buttonTextStyle: {
        color: Colors.deepPurple, fontSize: FontSize.medium
    }, buttonTextStyleActive: {
        color: Colors.white, fontSize: FontSize.medium
    },
    startRatingContainer: {
        flexDirection: 'row', flex: 1, paddingBottom: 3, paddingTop: 5,
        borderRadius: 5,
        width: 135,
    },
    startRatingContainerActive: {
        flexDirection: 'row', flex: 1, paddingBottom: 3, paddingTop: 5,
        backgroundColor: Colors.primary,
        borderRadius: 5,
        width: 135,
        opacity: 0,
    },
    recoContainer: {
        backgroundColor: Colors.white,
    },
    recoContinerActive: {
        backgroundColor: Colors.primary,
        opacity: 0
    },
    headerContainer: {
        backgroundColor: Colors.white,
    },
    headerContainerActive: {
        backgroundColor: Colors.primary,
        opacity: 0,
    }
});

export default CompanyStarProfile;
