import React, { useRef } from "react";
import {
    Animated
} from "react-native";
const FadeOutButton = (props) => {

    const fadeAnim = useRef(new Animated.Value(props.visible ?  0 : 1)).current  // Initial value for opacity: 0

    React.useEffect(() => {
        Animated.timing(
            fadeAnim,
            {
                toValue:props.visible ?  1 : 0,
                duration: 1000, useNativeDriver: true
            }
        ).start();
    }, [fadeAnim])

    return (
        <Animated.View
            style={{
                ...props.style,
                opacity: fadeAnim,
            }}
        >
            {props.children}
        </Animated.View>
    );
}


export default FadeOutButton    ;

