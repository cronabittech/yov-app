import PropTypes from "prop-types";
import React from "react";
import {
  Pressable, StyleSheet,
  Text, View
} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';


const VibrateStart = () => {
  }
const VibrateStop = () => {
}

const Button = (props) => {
  if (props.type === "iconButton") {
    return <IconButton {...props} />;
  } else if (props.type === "iconTextButton") {
    return <IconTextButton {...props} />;
  } else if (props.type === "onlyIcon") {
    return <IconWithOBackgroud {...props} />;
  }
  else if (props.type === "textButton") {
    return <TextButton {...props} />;
  }
  else if (props.type === "footerButton") {
    return <FooterButtton {...props} />;
  }
  else if (props.type === "footerButtonNoicon") {
    return <FooterButttonNoIcon {...props} />;
  }
  else if (props.type === "trasnparent") {
    return <TransparentButton {...props} />;
  }
  else if (props.type === "border") {
    return <BorderButton {...props} />;
  }
  else if (props.type === "border2") {
    return <BorderButtonTwo {...props} />;
  }
  else if (props.type === "header") {
    return <HeaderRightButton {...props} />;
  }
  else if (props.type === "headerleft") {
    return <HeaderLeftButton {...props} />;
  }
  else if (props.type === "blink") {
    return <HeaderBlinkButton {...props} />;
  } else if (props.type === 'FullWithButton') {
    return <FullWithButton {...props} />;
  } else if (props.type === 'FullWithButton2') {
    return <FullWithButtonTwo {...props} />;
  } else if (props.type === 'FullWidthhButton2') {
    return <FullWidthButton {...props} />;
  } else if (props.type === 'profileButton') {
    return <ProfileButton {...props} />;
  } else if (props.type == "option") {
    return <OptionFavButton {...props} />;
  }
  return <IconButton {...props} />;
};

Button.propTypes = {
  type: PropTypes.string,
};

const OptionFavButton = (props) => (
  <Pressable
    style={props.is_active ? styles.optionActive : styles.optionInActive}
    onPress={props.selectOption}>
    <View>
      <Text style={props.is_active ? styles.optionTextActive : styles.optionTextInActive}>{props.text}</Text>
    </View>
  </Pressable>
)

const ProfileButton = (props) => (
  <Pressable
    style={({ pressed }) => [
      pressed ? styles.fullButtonContainerActive : styles.fullButtonContainer, {
        borderRadius: 5,
        backgroundColor: props.backgroundColor ? props.backgroundColor : Colors.white
      }
    ]}
    onPress={props.isActive ? props.onPress : null}
    onPressIn={props.onPressIn} onPressOut={props.onPressOut}>
    {({ pressed }) => (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text style={{ marginLeft: 10, color: pressed ? Colors.white : props.textColor, fontSize: FontSize.large }}>{props.text}</Text>
        { props.isActive ?
          <Icon style={{ marginTop: 5, marginRight: 10 }} name='chevron-right' color={props.textColor} />
          :
          null
        }
      </View>
    )}
  </Pressable>

)

const FullWidthButton = (props) => (
  <Pressable
    style={({ pressed }) => [
      pressed ? styles.fullButtonContainerActive : styles.fullButtonContainer, { borderRadius: 5, backgroundColor: props.color ? props.color : '#F5F5F5', }
    ]}
    onPress={props.onPress}
    onPressIn={props.onPressIn} onPressOut={props.onPressOut}>
    {({ pressed }) => (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <Icon style={{ marginTop: 5, marginLeft: 15 }} name='chevron-left' color={props.textColor} />
        <Text style={{ color: pressed ? Colors.white : props.textColor, fontSize: FontSize.large }}>{props.text}</Text>
        <Icon style={{ marginTop: 5, marginRight: 15 }} name='chevron-right' color={props.textColor} />
      </View>
    )}
  </Pressable>
)

const FullWithButton = (props) => (
  <Pressable
    style={({ pressed }) => [
      pressed ? styles.fullButtonContainerActive : styles.fullButtonContainer
    ]}
    onPress={props.onPress}
    onPressIn={props.onPressIn} onPressOut={props.onPressOut}>
    {({ pressed }) => (
      <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
        <Text style={{ color: pressed ? Colors.white : props.active ? Colors.deepPurple : Colors.deepPurple, fontSize: FontSize.large }}>{props.text}</Text>
        <Icon style={{ marginTop: 5, position: 'absolute', right: 15 }} name='chevron-right' color={props.active ? Colors.primary : Colors.deepPurple} />
      </View>
    )}
  </Pressable>
)

const FullWithButtonTwo = (props) => (
  <Pressable
    style={({ pressed }) => [
      pressed ? styles.fullButtonContainerActive : styles.fullButtonContainer, { maxHeight: 25, height: 25 }
    ]}
    onPress={props.onPress}
    onPressIn={props.onPressIn} onPressOut={props.onPressOut}>
    {({ pressed }) => (
      <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
        <Text style={{ color: pressed ? Colors.white : props.active ? Colors.deepPurple : Colors.deepPurple, fontSize: FontSize.large }}>{props.text}</Text>
        <Icon style={{ marginTop: 5, position: 'absolute', right: 15 }} name='chevron-right' color={props.active ? Colors.primary : Colors.deepPurple} />
      </View>
    )}
  </Pressable>
)

const HeaderBlinkButton = (props) => (
  <Pressable style={{ opacity: props.visible ? 1 : 0, marginRight: 15 }}
    onPress={props.onPress}>
    {({ pressed }) => (
      <Text style={{ fontSize: 18, color: Colors.deepPurple }}>
        {props.text}
      </Text>
    )}
  </Pressable>
)

const HeaderRightButton = (props) => (
  <Pressable style={({ pressed }) => [
    pressed ? { opacity: 0, marginRight: 15 } : { opacity: 1, marginRight: 15 }
  ]}
    onPress={props.onPress}>
    {({ pressed }) => (
      <Text style={{ fontSize: 18, color: Colors.deepPurple , fontWeight:'bold' }}>
        {props.text}
      </Text>
    )}
  </Pressable>
)

const HeaderLeftButton = (props) => (
  <Pressable style={({ pressed }) => [
    pressed ? { opacity: 0, marginLeft: 15 } : { opacity: 1, marginLeft: 15 }
  ]}
    onPress={props.onPress}>
    {({ pressed }) => (
      <Text style={{ fontSize: 18, color: Colors.deepPurple }}>
        {props.text}
      </Text>
    )}
  </Pressable>
)

const TransparentButton = (props) => (
  <Pressable
    style={({ pressed }) => [
      pressed ? styles.transparentContainerActive : styles.transparentContainer
    ]}
    onPress={props.onPress}
    onPressIn={() => VibrateStart()}
    onPressOut={() => VibrateStop()}>
    {({ pressed }) => (
      <Text style={[pressed ? styles.textActive : styles.text, props.textStyle]}>
        {props.text}
      </Text>
    )}
  </Pressable>
)

const BorderButton = (props) => (
  <Pressable style={({ pressed }) => [
    pressed ? styles.BorderButtonActive : styles.borderButton
  ]} onPress={props.onPress}
    onPressIn={() => VibrateStart()}
    onPressOut={() => VibrateStop()} >
    {({ pressed }) => (
      <Text style={pressed ? styles.borderButtonTextActive : styles.borderButtonText}>
        {props.text}
      </Text>
    )}
  </Pressable>
)
const BorderButtonTwo = (props) => (
  <Pressable style={({ pressed }) => [
    pressed ? styles.BorderButtonActive : styles.borderButton,{height:45}
  ]} onPress={props.onPress}
    onPressIn={() => VibrateStart()}
    onPressOut={() => VibrateStop()} >
    {({ pressed }) => (
      <Text style={[pressed ? styles.borderButtonTextActive : styles.borderButtonText,{fontSize:FontSize.heading, paddingLeft:15, paddingRight:15,}]}>
        {props.text}
      </Text>
    )}
  </Pressable>
)

const FooterButttonNoIcon = (props) => (
  <Pressable style={({ pressed }) => [
    pressed ? styles.footerButttonActivate : styles.footerButtton, {
      height: 40, width: '23%', borderRadius: 5, marginLeft: 15
    }, props.style
  ]} onPress={props.onPress}
    onPressIn={() => VibrateStart()}
    onPressOut={() => VibrateStop()} >
    {({ pressed }) => (
      <View >
        <Icon name={props.iconName} size={22}
          style={{ alignSelf: 'center' }} color={Colors.deepPurple} />
      </View>
    )}
  </Pressable>
);
const FooterButtton = (props) => (
  <Pressable style={({ pressed }) => [
    pressed ? styles.footerButttonActivate : styles.footerButtton
  ]} onPress={props.onPress}
    onPressIn={() => VibrateStart()}
    onPressOut={() => VibrateStop()} >
    {({ pressed }) => (
      <View style={{ flexDirection: 'column' }}>
        <Text style={pressed ? styles.footerButtonTextActvie : styles.footerButtonText} > {props.text} </Text>
        <Icon name={props.iconName} size={30}
          style={{ alignSelf: 'center', marginTop: 4, marginBottom: 2 }} color={pressed ? Colors.white : props.active ? Colors.primary : Colors.lightGrey} />
      </View>
    )}

  </Pressable>
);

const IconWithOBackgroud = (props) => (
  <Pressable hitSlop={{ left: 30, right: 30, top: 10, bottom: 30 }}
    style={[styles.buttonStyle]}
    onPress={props.onPress}
    onPressIn={() => VibrateStart()}
    onPressOut={() => VibrateStop()} >
    {({ pressed }) => (
      <Icon name={props.iconName} size={25}
        style={{ alignSelf: 'center' }} color={pressed ? Colors.white : props.active ? Colors.primary : Colors.lightGrey} />
    )}
  </Pressable>
);

const IconTextButton = (props) => (
  <Pressable style={({ pressed }) => [
    pressed ? styles.iconButtonActive : styles.iconButton, { opacity: props.visible ? 0 : 1 }
  ]} onPress={props.onPress}
    onPressIn={() => VibrateStart()}
    onPressOut={() => VibrateStop()} >

    {({ pressed }) => (
      <Text style={pressed ? styles.iconTextActive : styles.iconText}>
        {props.text}
      </Text>
    )}
  </Pressable>
)

const TextButton = (props) => (
  <Pressable hitSlop={10, 10, 10, 10} style={({ pressed }) => [
    pressed ? styles.textButtonActive : styles.textButton
  ]} onPress={props.onPress} >

    {({ pressed }) => (
      <Text style={{ color: Colors.deepPurple, fontSize: 18, fontWeight: 'bold' }}>
        {props.text}
      </Text>
    )}
  </Pressable>
)

const IconButton = (props) => (

  <Pressable style={({ pressed }) => [
    pressed ? styles.iconButtonActive : styles.iconButton
  ]} onPress={props.onPress}
    onPressIn={() => VibrateStart()}
    onPressOut={() => VibrateStop()} >
    {({ pressed }) => (
      <View>
        <Icon name={props.iconName} size={25}
          style={{ alignSelf: 'center' }} color={pressed ? Colors.white : Colors.white} />
      </View>

    )}
  </Pressable>

);

const styles = StyleSheet.create({
  iconButton: {
    flex: 1,
    margin: 1, height: 45, backgroundColor: Colors.primary,
    marginTop: 5,
    justifyContent: 'center', alignContent: 'center', borderRadius: 10
  }, textButton: {
    margin: 1, height: 40,
    marginTop: 5,
    justifyContent: 'center', alignContent: 'center', borderRadius: 5
  },
  iconTextActive: {
    color: Colors.primary,
    textAlign: 'center',
    paddingLeft: 16,
    paddingRight: 16
  },
  iconText: {
    color: Colors.white,
    textAlign: 'center',
    paddingLeft: 16,
    paddingRight: 16
  },
  iconButtonActive: {
    flex: 1,
    margin: 1, height: 45, backgroundColor: Colors.primary,
    marginTop: 5,
    justifyContent: 'center', alignContent: 'center', borderRadius: 10, opacity: 0,
  },
  textButtonActive: {
    margin: 1, height: 40, backgroundColor: Colors.white,
    marginTop: 5,
    justifyContent: 'center', alignContent: 'center', borderRadius: 5,
    opacity: 0
  },
  buttonStyle: {
    height: 25,
  },
  footerButtton: {
    width: '31%',
    justifyContent: 'center',
    flexDirection: 'column',
    borderWidth: 0.3,
    borderColor: "#CDCDCD",
    height: 60,
    backgroundColor: Colors.white,
    borderRadius: 10, shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.11,
    shadowRadius: 2.22,
    elevation: 2,
  },
  footerButttonActivate: {
    width: '31%',
    borderWidth: 1,
    borderColor: "#CDCDCD",
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: Colors.primary,
    height: 60,
    elevation: 2,
    borderRadius: 10,
    opacity: 0
  },
  footerButtonText: {
    color: Colors.deepPurple,
    paddingLeft: 3,
    fontSize: FontSize.medium,
    paddingRight: 3,
    textAlign: 'center'
  },
  footerButtonTextActvie: {
    color: Colors.white,
    paddingLeft: 3,
    fontSize: FontSize.medium,
    paddingRight: 3,
    textAlign: 'center',

  },
  borderButton: {
    backgroundColor: Colors.white,
    justifyContent: 'center',
    borderRadius: 5,
    paddingLeft: 1,
    paddingRight: 1,
    height: 35,
    paddingTop: 3,
    paddingBottom: 3,
    elevation: 2,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.11,
    shadowRadius: 2.22,

  },
  BorderButtonActive: {
    borderColor: Colors.primary,
    borderWidth: 2,
    height: 35,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    borderRadius: 5,
    paddingLeft: 1,
    paddingRight: 1,
    paddingTop: 3,
    paddingBottom: 3,
    elevation: 2,
    opacity: 0
  },
  borderButtonText: {
    color: Colors.deepPurple,
    paddingRight: 10,
    paddingLeft: 10,
    fontSize: FontSize.medium,
    textAlign: 'center'
  }, borderButtonTextActive: {
    color: Colors.white,
    paddingRight: 10,
    paddingLeft: 10,
    fontSize: FontSize.medium,
    textAlign: 'center'
  },
  transparentContainer: {
    backgroundColor: Colors.white,
  },
  transparentContainerActive: {
    backgroundColor: Colors.primary,
    opacity: 0
  },
  text: {
    color: Colors.deepPurple
  },
  textActive: {
    color: Colors.white
  }, fullButtonContainer: {
    flex: 1,
    backgroundColor: '#F5F5F5', elevation: 2, height: 30, justifyContent: 'center'
    , maxHeight: 30,
  }, fullButtonContainerActive: {
    flex: 1,
    backgroundColor: Colors.primary, elevation: 2, height: 30, justifyContent: 'center',
    opacity: 0, maxHeight: 30,
  },
  optionInActive: {
    height: 33,
    width: 33,
    justifyContent: 'center',
    backgroundColor: Colors.lightGrey2,
    elevation: 2,
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: "#CDCDCD",
  },
  optionActive: {
    height: 33,
    width: 33,
    justifyContent: 'center',
    backgroundColor: Colors.white,
    elevation: 2,
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: "#CDCDCD",
  },
  optionTextInActive: {
    textAlign: 'center',
    color: Colors.darkGrey,
    fontSize: FontSize.small
  },
  optionTextActive: {
    textAlign: 'center',
    color: Colors.primary,
    fontSize: FontSize.small
  },

});

export default Button;
