/** @format */

import React from "react";
import {
    Image,
    Pressable, StyleSheet,
    Text,
    View
} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import Button from "./Button";



const VibrateStart = () => {
}
const VibrateStop = () => {
}

const LogHistoryItem = (props) => {

    const checkRedText = (log) => {
        if (log.includes('del') || log.includes('rem') || log.includes('Del') || log.includes('Rem')) {
            return Colors.red;
        } else {
            return Colors.dark;
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.iconContainer}>
                <Icon style={{ alignSelf: 'center' }} name={props.logIcon} size={40} color={Colors.primary} />
            </View>

            <View>
                <Pressable
                    onPress={props.onHeaderClick}
                    onPressIn={() => VibrateStart()}
                    onPressOut={() => VibrateStop()}>
                    {({ pressed }) => (
                        <View style={pressed ? styles.headerContainerActive : styles.headerContainer}>
                            <Text numberOfLines={1} style={[pressed ? styles.headerActive : styles.header,]}>
                                {props.name}
                            </Text>
                            <View style={pressed ? styles.recoContinerActive : styles.recoContainer}>
                                <Text style={[pressed ? styles.subTitleActive : styles.subTitle, { color: checkRedText(props.log) }]}>
                                    {props.log}
                                </Text>
                                <Text style={[pressed ? styles.subTitleActive : styles.subTitle, { color: Colors.dark }]}>
                                    {props.starDate}
                                </Text>
                            </View>
                        </View>
                    )}
                </Pressable>
            </View>

        </View>
    );
};
const styles = StyleSheet.create({
    iconButton: {
        width: 50,
        marginRight: 5,
        backgroundColor: Colors.primary,
        justifyContent: 'center', alignContent: 'center', borderRadius: 5
    },
    container: {
        flex: 1,
        alignSelf: 'stretch',
        flexDirection: 'row',
        zIndex: 999999,
        padding: 10
    }, iconContainer: {
        width: 60,
        height: 60,
        backgroundColor: Colors.white,
        justifyContent: 'center',
        borderRadius: 10,
    },
    icon: {
        width: 60, height: 60, alignSelf: 'center'
    },
    header: {
        color: Colors.deepPurple, width: 280, fontSize: FontSize.large, fontWeight: 'bold', flexWrap: 'wrap',
    },
    headerActive: {
        color: Colors.white, width: 280, fontSize: FontSize.large, fontWeight: 'bold', flexWrap: 'wrap',
    },
    subTitle: {
        color: Colors.lessDarkGrey,
        maxWidth: 280, width: 280, flexWrap: 'wrap', flex: 1,
        fontSize: FontSize.medium, alignSelf: 'stretch',
        flexWrap: 'wrap', textAlign: 'left'
    }, subTitleActive: {
        opacity: 0,
        color: Colors.white,
        maxWidth: 280, width: 280,
        flex: 1, flexWrap: 'wrap',
        fontSize: FontSize.medium, flexWrap: 'wrap', textAlign: 'left'
    }, buttonTextStyle: {
        color: Colors.deepPurple, fontSize: FontSize.medium
    }, buttonTextStyleActive: {
        color: Colors.white, fontSize: FontSize.medium
    },
    startRatingContainer: {
        flexDirection: 'row', flex: 1, paddingBottom: 3, paddingTop: 5,
        borderRadius: 5,
        width: 135,
    },
    startRatingContainerActive: {
        flexDirection: 'row', flex: 1, paddingBottom: 3, paddingTop: 5,
        backgroundColor: Colors.primary,
        borderRadius: 5,
        width: 135,
        opacity: 0,
    },
    recoContainer: {
        backgroundColor: Colors.white,
    },
    recoContinerActive: {
        backgroundColor: Colors.primary,
        opacity: 0,
    },
    headerContainer: {
        backgroundColor: Colors.white,
        maxWidth: 180,
        width: 180,
        marginLeft: 10
    },
    headerContainerActive: {
        backgroundColor: Colors.primary,
        opacity: 0,
    }
});
export default LogHistoryItem;
