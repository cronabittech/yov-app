import { Text } from 'native-base';
import React from "react";
import { Image, Pressable, StyleSheet, TouchableOpacity,  View } from 'react-native';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';


const VibrateStart = () => {
}
const VibrateStop = () => {
}

const CustomHeader = (props) => {
  return (
    <View style={styles.continer}>
      { props.backButton &&
        <Pressable hitSlop={{ right: 100 }}
          style={({ pressed }) => [
            pressed ? styles.backContainerActive : styles.backContainer
          ]}
          onPress={props.backPress}
          onPressIn={() => VibrateStart()}
          onPressOut={() => VibrateStop()}  >

          {({ pressed }) => (
            <Image style={pressed ? styles.backButtonActive : styles.backButton}
              source={require('../assets/img/back.png')}
            />
          )}

        </Pressable>
      }

      <TouchableOpacity style={{ justifyContent: 'center' }} onPress={props.logoPress}>
        <Image style={styles.logoImage} source={require('../assets/img/logo.png')} />
      </TouchableOpacity>
      <RightMenu {...props} />

    </View>
  );
};

const RightMenu = (props) => {
  if (props.rightMenu == "image") {
    return <ImageMenu {...props} />;
  } else if (props.rightMenu == "text") {
    return <TextMenu {...props} />;
  }
}



const ImageMenu = (props) => (
  <Pressable hitSlop={{ left: 100, top: 50, bottom: 50, right: 50 }}
    style={({ pressed }) => [
      pressed ? styles.rightImageContainerActive : styles.rightImageContainer
    ]}
    onPress={props.rightMenuClick}
    onPressIn={() => VibrateStart()}
    onPressOut={() => VibrateStop()}>
    {({ pressed }) => (
      <Image style={pressed ? styles.rightMenuImageActive : styles.rightMenuImage}
        source={props.imageSrc}
      />
    )}
  </Pressable>
);

const TextMenu = (props) => (

  <Pressable hitSlop={{ left: 100, top: 50, bottom: 50, right: 50 }}
    style={styles.rightTextContainer}
    onPress={props.rightMenuClick}
    onPressIn={() => VibrateStart()}
    onPressOut={() => VibrateStop()}>
    {({ pressed }) => (
      <Text style={pressed ? styles.rightTextActive : styles.rightText}>{props.rightMenuText}</Text>
    )}

  </Pressable>
);

const styles = StyleSheet.create({

  continer: {
    flexDirection: 'row', justifyContent: 'space-between',
  },
  backContainer: {
    width: 40, height: 40,
    marginTop: 10,
    justifyContent: 'center', marginLeft: 5
  },
  backContainerActive: {
    width: 40, height: 40, backgroundColor: Colors.primary, borderRadius: 20,
    marginTop: 10,
    justifyContent: 'center', marginLeft: 5
  },
  backButton: {
    height: 35, width: 35, tintColor: Colors.deepPurple, marginLeft: 1
  },
  backButtonActive: {
    height: 35, width: 35, tintColor: Colors.white, marginLeft: 1
  },
  logoImage: {
    width: 80, height: 60, alignSelf: 'center'
  },
  rightText: {
    marginRight: 5,
    marginTop: 0, textTransform: 'capitalize', color: Colors.darkBlue, fontSize: FontSize.heading, paddingLeft: 0, paddingRight: 0
  },
  rightTextActive: {
    marginRight: 5,
    marginTop: 0, textTransform: 'capitalize', color: Colors.primary, fontSize: FontSize.heading, paddingLeft: 0, paddingRight: 0
  },
  rightImageContainer: {
    alignSelf: 'center',
    alignContent: 'center',
    marginRight: 10,
    height: 40,
    width: 40,
    justifyContent: 'center'

  }, rightTextContainer: {
    alignSelf: 'center',
    alignContent: 'center',
    marginRight: 10,
    justifyContent: 'center'
  },

  rightImageContainerActive: {
    alignSelf: 'center', alignContent: 'center', marginRight: 10, height: 40, width: 40,
    backgroundColor: Colors.primary,
    borderRadius: 50,
    justifyContent: 'center'
  },
  rightMenuImage: {
    height: 35, width: 40, tintColor: Colors.deepPurple
  }, rightMenuImageActive: {
    height: 35, width: 40, tintColor: Colors.white
  }

});


export default CustomHeader;
