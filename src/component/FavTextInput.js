import { Item } from 'native-base';
import React from "react";
import {
    StyleSheet,
    TextInput
} from "react-native";
import Colors from '../config/colors';
import FontSize from '../config/fontSize';


const FavTextInput = (props) => {
    return(
        <Item regular style={styles.item} >
            <TextInput placeholder={props.placeHolder} returnKeyType="done"
            blurOnSubmit={false}
            style={styles.textInput}
            />
        </Item>
    );
}

const styles = StyleSheet.create({
    textInput: {
        flex: 1,
        backgroundColor: Colors.white,
        color: Colors.deepPurple,
        fontSize: FontSize.large,
        paddingTop: 0,
        height: 25, 
        paddingBottom: 0
    },
    item:{
        marginBottom:3,
        flex: 1, 
        height: 35, 
        borderRadius: 5,  
        backgroundColor: Colors.white
    }
});

export default FavTextInput;