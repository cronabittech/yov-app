import {View, Text} from 'native-base';
import {StyleSheet} from 'react-native';
import React from 'react';
import Colors from '../config/colors';
import {doMail} from '../config/util';
function TermCondition() {
  return (
    <View style={{
     paddingBottom: 20
    }}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 21,
          fontWeight: 'bold',
          textDecorationLine: 'underline',
        }}>
        Terms &amp; Conditions
      </Text>
      <Text style={style.m5}>
        These user terms and conditions (hereinafter referred to as the "User
        Terms") apply to any use of YOV's App (hereinafter referred to as the
        "YOV App").
      </Text>
      <Text style={style.m5}>
        These User Terms are deemed to include all other operating rules,
        policies, and guidelines that are referred to herein or that we may
        otherwise publish on the YOV App.
      </Text>
      <Text style={style.m5}>
        In the event of any conflict between the terms contained herein and
        those of the User Guidelines, the User Guidelines will govern and
        control.
      </Text>
      <Text style={style.m5}>
        By using the YOV App, you accept to be subject to the User Terms,
        including the User Guidelines. If you do not accept these User Terms,
        you are not permitted to use the YOV App and are kindly requested not to
        use the YOV App any further. The registration as a user requires your
        express acceptance of these User Terms.
      </Text>

      <Text style={style.h1}>THE SERVICES ON THE YOV APP</Text>
      <Text style={style.h1}>1. Registered User</Text>
      <Text style={style.m5}>
        1.1 YOV grants you the non-exclusive, non-transferable, revocable,
        limited right to access and use the YOV App. In order to gain full
        access and use of the YOV App, you must create a profile and register as
        a user (hereinafter referred to as "Registered User").
      </Text>
      <Text style={style.m5}>
        1.2 You are only permitted to register one profile per person on the YOV
        App. The profile is personal and you must not transfer it to others.
      </Text>
      <Text style={style.m5}>
        1.3 In order to become a Registered User, you need a password. You
        choose your own password which must be used with your email address when
        logging onto the YOV App. The password is personal and you must not
        transfer it or in other ways make it available to others. It is your
        responsibility to ensure that the password does not fall into the hands
        of a third party. If you become aware that the password is or may have
        been compromised, you are obligated to change it immediately.
      </Text>
      <Text style={style.m5}>
        1.4 During the registration process, you must choose a username. The
        username will be shown on the YOV App whenever you write or comment on
        reviews or produce user-generated content (see 2.1 below) on the YOV
        App. Therefore, you must consider whether you wish to use a username
        from which you can be identified by others. The username must not (i) be
        offensive or in other ways insulting, (ii) contain the terms "Guest",
        "Admin", ".co.uk", ".com", etc. or (iii) contain characteristics which
        belong to a third party, including names of famous persons, or personal
        names to which you do not own the rights. You warrant that your username
        does not infringe on any rights (including any intellectual property
        rights) belonging to any third party and/or pertaining to the User Terms
      </Text>

      <Text style={style.m5}>
        1.5 Changes to the username may only be made by us. If you want to
        change your username, please contact us at{' '}
        <Text style={style.link} onPress={() => doMail('support@YOV.com')}>
          support@YOV.com
        </Text>
        .
      </Text>

      <Text style={style.m5}>
        1.6 We are entitled at any time, without notice and without prejudice,
        to delete, suspend or change your profile in the event of your violation
        or suspected violation of these User Terms or applicable law.
      </Text>

      <Text style={style.m5}>
        When deleting your profile, you will no longer have access to services
        on the YOV App which require your registration and/or login as a
        Registered User. When deleting your profile, we reserve the right to
        delete the user-generated content (see 2.1) you have made on the YOV
        App.
      </Text>

      <Text style={style.m5}>
        1.7 Furthermore, we reserve the right, at any time and without notice or
        explanation, to delete your profile and user-generated content (see
        2.1). In this case, our disclaimer applies without limitations.
      </Text>

      <Text style={style.m5}>
        1.8 You are not permitted to gain access or attempt to gain access to
        the parts of the YOV App requiring user registration if you are not a
        Registered User.
      </Text>

      <Text style={style.h1}>
        2. User-Generated Content from Registered Users
      </Text>
      <Text style={style.m5}>
        2.1 You hereby grant us the worldwide, non-exclusive, perpetual,
        irrevocable, royalty-free right and license to publish, display,
        reproduce, modify, create derivative works of and commercially exploit
        any material, information, notifications, reviews, articles or other
        types of communication (hereinafter referred to as the "User-Generated
        Content" or "UGC") which you create on the YOV App as a Registered User.
        We may freely use and transfer the UGC and disclose the UGC to third
        parties.
      </Text>

      <Text style={style.m5}>
        2.2 Registered Users are liable for the UGC they publish on the YOV App.
      </Text>

      <Text style={style.m5}>
        2.3 Registered Users warrant that all UGC posted on the YOV App is
        correct and true (where they state facts) or genuinely held (where they
        state opinions).
      </Text>

      <Text style={style.m5}>
        2.4 UGC must relate to a company or organization from which the
        Registered User has purchased or can otherwise document using the
        company's or organization's products or services.
      </Text>

      <Text style={style.m5}>
        2.5 You may not publish UGC regarding companies to which you have
        personal or professional relations.
      </Text>
      <Text style={style.m5}>
        2.6 Registered Users must not, and must not allow any third party to,
        publish UGC on the YOV App which:
      </Text>

      <Text style={style.m5}>
        2.6.1) is of a marketing nature or has marketing purposes,
      </Text>
      <Text style={style.m5}>
        2.6.2) is unlawful, deceptive, misleading, fraudulent, threatening,
        abusive, harassing, libelous, defamatory, tortious, obscene,
        pornographic or profane, has sexist, political or racial character,
        violates other people's rights, including any intellectual property
        rights, rights of privacy and/or rights of publicity,
      </Text>
      <Text style={style.m5}>
        2.6.3) is offensive or in any way breaches any applicable local,
        national or international law or regulation,
      </Text>
      <Text style={style.m5}>
        2.6.4) violates these User Terms, including the User Guidelines, reveals
        any personal information about another individual, including another
        person's name, address, phone number, email address, credit card
        information or any other information that could be used to track,
        contact or impersonate that person,
      </Text>
      <Text style={style.m5}>
        2.6.4) has a disloyal or unlawful purpose and/or content (or promotes
        unlawful purposes), or
      </Text>
      <Text style={style.m5}>
        2.6.4) is technically harmful (including without limitation computer
        viruses, logic bombs, Trojan horses, worms, harmful components,
        corrupted data or other malicious software, harmful data or conduct).
      </Text>
      <Text style={style.m5}>
        2.7 Contributors of UGC warrant in every context that the UGC is lawful
        and in compliance with the User Terms. If YOV receives notice or
        otherwise becomes aware that UGC violates current legislation and/or the
        User Terms, we may delete the UGC without any notice, and we - dependent
        on the character of the violation - may inform the violated party and/or
        the authorities of the violation. Our right to delete will not be
        conditioned on an explanation, although we will strive to inform the
        Registered User about the deletion and the reason hereof.
      </Text>
      <Text style={style.m5}>
        2.8 The Registered User hereby grants us the right to initiate and take
        any legal actions which we deem necessary in case of infringement of the
        Registered User's UGC.
      </Text>
      <Text style={style.m5}>
        The Registered User must guarantee to indemnify us for any claims which
        may be made against us as a consequence of the Registered User's
        violation of the User Terms or current legislation. The Registered User
        must indemnify and hold us harmless from and against any claim or loss
        due to third party claims against us resulting from the UGC of the
        Registered User.
      </Text>
      <Text style={style.m5}>
        2.9 We may at any time request information about the UGC from the
        Registered User, including documentation supporting the information
        included in the UGC, e.g. documentation evidencing that the UGC is based
        on an actual buying experience in an actual customer relation to the
        company to which the UGC relates.
      </Text>
      <Text style={style.h1}>GENERAL TERMS</Text>
      <Text style={style.h1}>3. Rights</Text>
      <Text style={style.m5}>
        3.1 The YOV App and the services we offer via the YOV App, including all
        underlying technology and intellectual property rights embodied therein,
        are and remain our sole and exclusive property, and no license or any
        other right is granted to any such underlying technology. If you provide
        feedback, ideas or suggestions regarding the YOV App or the services
        offered on the YOV App ("Feedback"), we are free to fully exploit such
        Feedback.
      </Text>
      <Text style={style.m5}>
        3.2 The content on the YOV App, including but not limited to the
        intellectual property rights, text, characteristics, graphics, icons,
        photos, calculations, references and software is or will be our
        property, including without limitation applicable copyright and
        trademark laws.
      </Text>
      <Text style={style.m5}>
        3.3 Unauthorized copying, distribution, presentation or other use of the
        YOV App or part hereof is a violation of U.S. law and may thus result in
        civil and/or criminal penalties.
      </Text>
      <Text style={style.m5}>
        3.4 To the fullest extent permitted by law, the rights to free use of
        the UGC are transferred to us irrevocably, without any time limitation
        and without territorial limitations, by submitting the UGC to us.
      </Text>
      <Text style={style.m5}>
        3.5 Downloading and other digital copying of the content on the YOV App
        or parts hereof are only permitted for personal non-commercial use
        unless otherwise agreed with us in writing or allowed under applicable
        mandatory law.
      </Text>
      <Text style={style.m5}>
        3.6 All company names, trademarks and other business characteristics on
        the YOV App is or will be our property or the property of a third party
        (other than the Registered User) and must only be used for business
        purposes upon prior approval from us or the third party owner,
        respectively.
      </Text>
      <Text style={style.h1}>4. Personal data</Text>
      <Text style={style.m5}>
        4.1 We perform different types of processing of personal data in
        connection with the use of the YOV App. Our processing of personal data
        takes place under observance of our Privacy Policy, which can be
        obtained here: (insert link for PP)
      </Text>
      <Text style={style.h1}>5. Disclaimers</Text>
      <Text style={style.m5}>
        5.1 THE YOV APP, CONTENT AND SERVICES OFFERED ON THE YOV APP ARE
        PROVIDED 'AS IS' AND AS AVAILABLE WITHOUT REPRESENTATIONS OR WARRANTIES
        OF ANY KIND. YOV EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS,
        IMPLIED OR STATUTORY, INCLUDING WITHOUT LIMITATION ANY WARRANTIES OF
        NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
        THE YOV APP AND SERVICES MAY BE MODIFIED, UPDATED, INTERRUPTED,
        SUSPENDED OR DISCONTINUED AT ANY TIME WITHOUT NOTICE OR LIABILITY.
      </Text>
      <Text style={style.m5}>
        5.2 We make no representations or warranties with respect to any UGC
        published on the YOV App. Notwithstanding the foregoing, YOV may at all
        times investigate and edit (including anonymizing) UGC, e.g. if such
        actions are (i) prompted by third party requests, (ii) required under
        applicable law or (iii) necessary for the UGC's compliance with our User
        Guidelines.
      </Text>
      <Text style={style.m5}>
        5.3 We disclaim all liability for the content of UGC. Our non-liability
        applies, without limitation, to any UGC, including UGC which has been
        edited by us (see 5.2). We are not liable for any links to third party
        YOV Apps in the UGC, including for the content of the page to which the
        UGC links
      </Text>
      <Text style={style.m5}>
        5.4 Recommendations, reviews, comments, etc. of specific companies,
        services, e-businesses, etc. on the YOV App are provided by Registered
        Users and are not endorsements made by us. We disclaim all liability for
        the content of the YOV App. The use of our services is in any respect
        the sole responsibility of the Registered Users. We cannot be held
        liable for the availability of the YOV App.
      </Text>
      <Text style={style.h1}>6. Limitation of liability</Text>
      <Text style={style.m5}>
        6.1 WE SHALL IN NO CASE BE HELD LIABLE, WHETHER IN CONTRACT, TORT
        (INCLUDING NEGLIGENCE) OR OTHERWISE FOR DAMAGES FOR THE USE OF THE YOV
        APP, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES,
        INCLUDING (I) LOSS OF PROFITS, CONTRACTS, TURNOVER, BUSINESS, BUSINESS
        OPPORTUNITY, LOSS OR CORRUPTION OF DATA OR RECOVERY OF DATA, GOODWILL,
        SECURITY BREACH RESULTING FROM A FAILURE OF THIRD PARTY
        TELECOMMUNICATIONS AND/OR THE INTERNET, ANTICIPATED SAVINGS OR REVENUE
        (REGARDLESS OF WHETHER ANY OF THESE ARE DIRECT, INDIRECT OR
        CONSEQUENTIAL) (II) ANY LOSS OR DAMAGE ARISING IN CONNECTION WITH
        LIABILITIES TO THIRD PARTIES (WHETHER DIRECT, INDIRECT OR CONSEQUENTIAL)
        OR (III) ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL
        LOSS OR DAMAGE WHATSOEVER. SOME STATES AND OTHER JURISDICTIONS DO NOT
        ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR INCIDENTAL OR
        CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATIONS AND EXCLUSIONS MAY NOT
        APPLY TO YOU.
      </Text>
      <Text style={style.m5}>
        6.2 OUR TOTAL AGGREGATE LIABILITY, INCLUDING WITHOUT LIMITATION
        LIABILITY FOR BREACH OF CONTRACT, MISREPRESENTATION (WHETHER TORTIOUS OR
        STATUTORY), TORT (INCLUDING NEGLIGENCE), BREACH OF STATUTORY DUTY OR
        OTHERWISE, ARISING FROM OR IN CONNECTION WITH THE USER TERMS, THE YOV
        APP OR OUR SERVICES WILL NEVER FOR ANY AND ALL ACTIONABLE CIRCUMSTANCES
        EXCEED ONE HUNDRED DOLLARS ($100.00). YOU FURTHER AGREE THAT NO CLAIMS
        OR ACTIONS ARISING OUT OF, OR RELATED TO, THE USE OF OUR YOV APP OR
        SERVICES OR THESE USER TERMS MAY BE BROUGHT BY YOU MORE THAN ONE (1)
        YEAR AFTER THE ACTIONABLE EVENT. NOTHING IN THE USER TERMS EXCLUDES OR
        LIMITS EITHER PARTY'S LIABILITY FOR MATTERS WHICH CANNOT BE EXCLUDED OR
        LIMITED UNDER APPLICABLE LAW.
      </Text>
      <Text style={style.h1}>7. Other User Terms</Text>
      <Text style={style.m5}>
        7.1 We may at any time, in our sole discretion, revise or change these
        User Terms. We will make an effort to provide reasonable advance notice
        of any such changes. We may at any time, in our own discretion and
        without notice, close, change or reorganize the YOV App. As a Registered
        User you accept to be covered by the at all times current User Terms.
        Any revision or change of the User Terms will be stated on the YOV App.
        We will furthermore strive to inform the Registered Users about the
        change of the User Terms. The Registered Users agree that the continued
        use of the YOV App after any posted modified version of the User Terms
        is an acceptance of the modified User Terms.
      </Text>
      <Text style={style.m5}>
        7.2 Should any of these User Terms be regarded as unlawful or without
        effect and therefore not to be enforced, this will not have any effect
        on the applicability and enforcement of the remaining part of the User
        Terms.
      </Text>
      <Text style={style.h1}>8. Term and termination</Text>
      <Text style={style.m5}>
        8.1 We may terminate your right to access and use the services offered
        on the YOV App at any time for any reason without liability. If we do
        so, or if you elect to delete your profile, any rights granted to you
        herein will immediately cease. Sections 2-10 will survive any
        termination of the User Terms.
      </Text>
      <Text style={style.h1}>9. Applicable law and venue</Text>
      <Text style={style.m5}>
        9.1 The User Terms and the relationship between you and YOV are governed
        by the laws applicable in your country
      </Text>
      <Text style={style.h1}>10. Copyright dispute policy</Text>
      <Text style={style.m5}>
        10.1 It is YOV's policy to (i) block access to or remove material that
        it believes in good faith to be copyrighted material that has been
        illegally copied and distributed by any of our advertisers, affiliates,
        content providers or members or users; and (ii) remove and discontinue
        service to repeat offenders.
      </Text>
      <Text style={style.h1}>11. Miscellaneous</Text>
      <Text style={style.m5}>
        11.1 You acknowledge and agree that these User Terms constitute the
        complete and exclusive agreement between you and YOV concerning your use
        of the YOV App and supersede and govern all prior proposals, agreements
        or other communications.
      </Text>
      <Text style={style.m5}>
        11.2 Nothing contained in these User Terms can be construed as creating
        any agency, partnership or other form of joint enterprise between us.
        Our failure to require your performance of any provision hereof will not
        affect our full right to require such performance at any time
        thereafter, nor may our waiver of a breach of any provision hereof be
        taken or held to be a waiver of the provision itself. You may not assign
        any rights granted to you hereunder, and any such attempts are void.
      </Text>
      <Text style={style.h1}>12. Contact</Text>
      <Text style={style.m5}>
        12.1 You may contact YOV via email at{' '}
        <Text style={style.link} onPress={() => doMail('support@YOV.com')}>
          support@YOV.com
        </Text>
        .
      </Text>
    </View>
  );
}

const style = StyleSheet.create({
  m5: {
    marginTop: 10,
    textAlign: 'justify',
  },
  h1: {
    marginTop: 15,
    fontWeight: 'bold',
    fontSize: 18,
  },
  link: {
    color: Colors.darkBlue,
  },
});

export default TermCondition;
