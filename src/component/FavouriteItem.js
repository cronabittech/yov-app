/** @format */

import React from "react";
import {
    Image,
    Pressable, StyleSheet,
    Text,
    Vibration, View
} from "react-native";
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import Button from "./Button";



const VibrateStart = () => {
  
}
const VibrateStop = () => {
 
}

const FavouriteItem = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.iconContainer}>
                <Image style={styles.icon} resizeMode="contain"
                    source={{ uri: props.icon }} />
            </View>

            <View style={{ flex: 1, marginLeft: 10, flexDirection: 'column', justifyContent: 'space-between' }}>

                <Pressable
                    onPress={props.onHeaderClick}
                    onPressIn={() => VibrateStart()}
                    onPressOut={() => VibrateStop()}>
                    {({ pressed }) => (
                        <View style={pressed ? styles.headerContainerActive : styles.headerContainer}>
                            <Text numberOfLines={1} style={[pressed ? styles.headerActive : styles.header,]}>
                                {props.cname}
                            </Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Text numberOfLines={1} style={[pressed ? styles.subTitleActive : styles.subTitle,{color:Colors.deepPurple}]}>
                                    {props.post}
                                </Text>
                            </View>
                        </View>
                    )}
                </Pressable>

                <Pressable
                    onPress={props.onRecoClick}
                    onPressIn={() => VibrateStart()}
                    onPressOut={() => VibrateStop()}>
                    {({ pressed }) => (
                        <View style={{ flexDirection: 'row', marginTop:6,justifyContent: 'space-between' }}>
                            <View style={pressed ? styles.recoContinerActive : styles.recoContainer}>
                                <Text numberOfLines={1} style={[pressed ? styles.subTitleActive : styles.subTitle, { color: Colors.darkGrey }]}>
                                {props.name}</Text>
                                <Text   numberOfLines={1}  style={[pressed ? styles.subTitleActive : styles.subTitle, { color: Colors.darkGrey }]}>
                                {props.title}
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row-reverse', marginRight: -93, alignSelf: 'flex-end' }}>
                                <Button type="footerButtonNoicon" iconName="phone" onPress={props.phone} />
                                <Button type="footerButtonNoicon" iconName="envelope"  onPress={props.mail} />
                            </View>
                        </View>
                    )}
                </Pressable>
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        zIndex: 999999,
        padding: 10
    }, iconContainer: {
        width: 80,
        height: 80,
        elevation: 5,
        backgroundColor: Colors.lightGrey2,
        justifyContent: 'center',
        borderRadius: 10,
    },
    icon: {
        width: 80, height: 80, alignSelf: 'center'
    },
    header: {
        color: Colors.deepPurple, width: 280, fontSize: FontSize.large, fontWeight: 'bold', flexWrap: 'wrap',
    },
    headerActive: {
        color: Colors.white, width: 280, fontSize: FontSize.large, fontWeight: 'bold', flexWrap: 'wrap',
    },
    subTitle: {
         color: Colors.lessDarkGrey,
        maxWidth: 170, width: 170, flexWrap: 'wrap', flex: 1, 
        fontSize: FontSize.medium, alignSelf:'stretch',
        flexWrap: 'wrap', textAlign: 'left'
    },  subTitleActive: {
        opacity: 0,
        color: Colors.white, 
        maxWidth: 280, width: 280, 
        flex: 1, flexWrap: 'wrap', 
        fontSize: FontSize.medium, flexWrap: 'wrap', textAlign: 'left'
    }, buttonTextStyle: {
        color: Colors.deepPurple, fontSize: FontSize.medium
    }, buttonTextStyleActive: {
        color: Colors.white, fontSize: FontSize.medium
    },
    startRatingContainer: {
        flexDirection: 'row', flex: 1, paddingBottom: 3, paddingTop: 5,
        borderRadius: 5,
        width: 135,
    },
    startRatingContainerActive: {
        flexDirection: 'row', flex: 1, paddingBottom: 3, paddingTop: 5,
        backgroundColor: Colors.primary,
        borderRadius: 5,
        width: 135,
        opacity: 0,
    },
    recoContainer: {
        backgroundColor: Colors.white,
        width: 165,
        alignSelf: 'flex-end'
    },
    recoContinerActive: {
        backgroundColor: Colors.primary,
        width: 165,
        opacity: 0,
        alignSelf: 'flex-end'
    },
    headerContainer: {
        backgroundColor: Colors.white,
    },
    headerContainerActive: {
        backgroundColor: Colors.primary,
        opacity: 0,
    }
});
export default FavouriteItem;
