/** @format */

import React from 'react';
import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';

const VibrateStart = () => {};
const VibrateStop = () => {};

const FeedbackItem = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.iconContainer}>
        <Image
          style={styles.icon}
          resizeMode="contain"
          source={{uri: props.icon}}
        />
      </View>
      <View
        style={{
          flex: 1,
          marginLeft: 10,
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}>

        <Pressable
          onPress={props.onHeaderClick}
          onPressIn={() => VibrateStart()}
          onPressOut={() => VibrateStop()}>
          {({pressed}) => (
            <View
              style={
                pressed ? styles.headerContainerActive : styles.headerContainer
              }>
              <Text
                numberOfLines={1}
                style={[pressed ? styles.headerActive : styles.header]}>
                {props.cname}
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Text
                  numberOfLines={1}
                  style={[
                    pressed ? styles.subTitleActive : styles.subTitle,
                    {color: Colors.deepPurple},
                  ]}>
                  {props.post}
                </Text>
              </View>
            </View>
          )}
        </Pressable>

        <Pressable
          onPress={props.onRecoClick}
          onPressIn={() => VibrateStart()}
          onPressOut={() => VibrateStop()}>
          {({pressed}) => (
            <View
              style={{
                flexDirection: 'row',
                marginTop: 2,
                justifyContent: 'space-between',
              }}>
              <View
                style={
                  pressed ? styles.recoContinerActive : styles.recoContainer
                }>
                <Text
                  numberOfLines={1}
                  style={[
                    pressed ? styles.subTitleActive : styles.subTitle,
                    {color: Colors.darkGrey},
                  ]}>
                  {props.feedback}
                </Text>
              </View>
            </View>
          )}
        </Pressable>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            width: '100%',
            marginTop:5,
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              color: Colors.lightGrey,
            }}>
            Sent: {props.created_at}
          </Text>
          {props.count > 0 ? (
            <Text style={{color: Colors.darkBlue}}>{props.count} Replies</Text>
          ) : null}
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    zIndex: 999999,
    padding: 10,
  },
  iconContainer: {
    width: 80,
    height: 80,
    justifyContent: 'center',
    borderRadius: 15,
    overflow: "hidden",
    alignItems: 'center',
  },
  icon: {
    height: '140%', width: undefined, aspectRatio: 1
  },
  header: {
    color: Colors.deepPurple,
    width: 280,
    fontSize: FontSize.large,
    fontWeight: 'bold',
    flexWrap: 'wrap',
  },
  headerActive: {
    color: Colors.white,
    width: 280,
    fontSize: FontSize.large,
    fontWeight: 'bold',
    flexWrap: 'wrap',
  },
  subTitle: {
    color: Colors.lessDarkGrey,
    maxWidth: 280,
    width: 280,
    flexWrap: 'wrap',
    flex: 1,
    fontSize: FontSize.medium,
    alignSelf: 'stretch',
    flexWrap: 'wrap',
    textAlign: 'left',
  },
  subTitleActive: {
    opacity: 0,
    color: Colors.white,
    maxWidth: 280,
    width: 280,
    flex: 1,
    flexWrap: 'wrap',
    fontSize: FontSize.medium,
    flexWrap: 'wrap',
    textAlign: 'left',
  },
  recoContainer: {
    backgroundColor: Colors.white,
    alignSelf: 'flex-end',
  },
  recoContinerActive: {
    backgroundColor: Colors.primary,
    opacity: 0,
    alignSelf: 'flex-end',
  },
  headerContainer: {
    backgroundColor: Colors.white,
  },
  headerContainerActive: {
    backgroundColor: Colors.primary,
    opacity: 0,
  },
});
export default FeedbackItem;
