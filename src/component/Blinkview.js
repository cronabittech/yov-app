import React, { PureComponent } from "react";
import { Pressable, StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../config/colors';



class BlinkView extends PureComponent {

    state = {
        blink: false,
    }
    constructor(props) {
        super();
    }
    componentDidMount() {
        setInterval(() => {
            this.setState({ blink: this.state.blink ? false : true });
        }, 1500);
    }

    render() {
        if (this.props.twoArrow) {
            return (
                <Pressable style={({ pressed }) => [
                    this.state.blink ? styles.activeButton : styles.deactiveButton
                ]}
                    onPress={this.props.onPress}>
                    {({ pressed }) => (
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Icon style={{ marginTop: 5, marginLeft: 15 }} name='chevron-left' color={Colors.white} />
                            <Text style={{ fontSize: 18, textAlign: 'center', color: this.state.blink ? Colors.white : Colors.deepPurple }}>
                                {this.props.text}
                            </Text>
                            <Icon style={{ marginTop: 5, marginRight: 15 }} name='chevron-right' color={Colors.white} />

                        </View>
                    )}
                </Pressable>
            );
        }
        return (
            <Pressable style={({ pressed }) => [
                this.state.blink ? styles.activeButton : styles.deactiveButton
            ]}
                onPress={this.props.onPress}>
                {({ pressed }) => (
                    <Text style={{ fontSize: 18, textAlign: 'center', color: this.state.blink ? Colors.white : Colors.deepPurple }}>
                        {this.props.text}
                    </Text>
                )}
            </Pressable>
        );
    }
}

export default BlinkView;
const styles = StyleSheet.create({
    activeButton: {
        backgroundColor: Colors.primary,
        height: 35,
        flex: 1,
        justifyContent: 'center', alignContent: 'center', borderRadius: 5
    },
    deactiveButton: {
        backgroundColor: Colors.white,
        height: 35,
        flex: 1,
        justifyContent: 'center', alignContent: 'center', borderRadius: 5
    }
});