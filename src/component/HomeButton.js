/** @format */

import React from "react";
import {
    Pressable, StyleSheet,
    View
} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';

const VibrateStart = () => {
}
const VibrateStop = () => {
}
const HomeButton = (props) => {
    return (
        <Pressable style={({ pressed }) => [
            pressed ? styles.footerButttonActive : styles.footerButtton
        ]} onPress={props.onPress}
            onPressIn={() => VibrateStart()}
            onPressOut={() => VibrateStop()}>
            {({ pressed }) => (
                <View style={{ justifyContent: 'center' }}>
                    <View style={{ alignSelf: 'center', width: 30, height: 30 }}>
                        <Icon name={props.iconName} size={30}
                            style={{ alignSelf: 'center' }} color={pressed ? Colors.white : Colors.primary} />
                    </View>
                </View>
            )}
        </Pressable>
    );
};
const styles = StyleSheet.create({
    footerButtton: {
        width: 70, height: 70, borderWidth: 0.3, backgroundColor: Colors.white, borderColor: "#CDCDCD", elevation: 1, padding: 15, margin: 10, justifyContent: 'center',
        backgroundColor: Colors.white,
        borderRadius: 10, shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
    },
    footerButttonActive: {
        opacity: 0,
        width: 70, height: 70, borderWidth: 0, backgroundColor: Colors.white, borderColor: "#CDCDCD", elevation: 1, padding: 15, margin: 10, borderRadius: 5, justifyContent: 'center'
    },
    footerButtonText: {
        fontSize: FontSize.large, color: Colors.deepPurple, textAlign: 'center'
    },
    footerButtonTextActive: {
        fontSize: FontSize.large, color: Colors.white, textAlign: 'center'
    },

    subHeaderStyle: {
        backgroundColor: Colors.primary,
        color: Colors.white,
        textAlign: 'center',
        padding: 5,
        fontSize: FontSize.heading
    }
});
export default HomeButton;
