/** @format */
import React from 'react';
import {Modal, Platform, StyleSheet, View} from 'react-native';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';

const ModelLayout = (props) => {
  return (
    <Modal animationType="fade" transparent={true} visible={props.visible}>
      <View
        style={[styles.centeredView, props.centeredViewStyle]}
        onTouchStart={props.outsideClick}>
        <View style={[styles.modalView, props.modalViewStyle]}>
          {props.children}
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  subHeaderStyle: {
    backgroundColor: Colors.primary,
    color: Colors.white,
    textAlign: 'center',
    padding: 5,
    fontSize: FontSize.heading,
  },
  centeredView: {
    marginTop: Platform.OS === 'ios' ? 240 : 215,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fafafad4',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 25,
    elevation: 5,
    borderWidth: 0.3,
    borderColor: '#CDCDCD',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
  },
});
export default ModelLayout;
