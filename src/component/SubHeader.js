/** @format */

import React from "react";
import {
  StyleSheet,
  Text
} from "react-native";
import Colors from '../config/colors';
import FontSize from '../config/fontSize';



const SubHeader = (props) => {
  return (
    <Text  onLayout={props.onLayout} style={styles.subHeaderStyle}>{props.text}</Text>
  );
};
const styles = StyleSheet.create({
    subHeaderStyle:{
        backgroundColor: Colors.primary, 
        color: Colors.white, 
        textAlign: 'center', 
        padding: 5, 
        fontSize: FontSize.heading 
    }
});
export default SubHeader;
