/** @format */

import React from "react";
import {
    Image,
    Pressable, StyleSheet,
    Text,
     View
} from "react-native";
import StarRating from 'react-native-star-rating';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';



const VibrateStart = () => {

}
const VibrateStop = () => {
 
}

const CompanyShortProfile = (props) => {
    return (
        <View onLayout={props.onLayout} style={styles.container}>
            <View style={styles.iconContainer}>
                <Image style={styles.icon} resizeMode="contain"
                    source={{ uri: props.icon }} />
            </View>

            <View style={{ marginLeft: 10, justifyContent: 'space-between', }}>

                <Pressable
                    onPress={props.onHeaderClick}
                    onPressIn={() => VibrateStart()}
                    onPressOut={() => VibrateStop()}>
                    {({ pressed }) => (
                        !props.sheetOpen ?
                            <View style={pressed ? styles.headerContainerActive : styles.headerContainer}>
                                <Text numberOfLines={1} style={[pressed ? styles.headerActive : styles.header,]}>
                                    {props.name}
                                </Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text numberOfLines={2} style={[pressed ? styles.subTitleActive : styles.subTitle]}>
                                        {props.formatted_address}
                                    </Text>
                                </View>
                            </View>
                            :
                            <View style={pressed ? styles.headerContainerActive : styles.headerContainer}>
                                <Text numberOfLines={1} style={[pressed ? styles.headerActive : styles.header, { color: props.startDone ? Colors.deepPurple : '#585858' }]}>
                                    {props.name}
                                </Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text numberOfLines={2} style={[pressed ? styles.subTitleActive : styles.subTitle, { color: props.startDone ? Colors.deepPurple : Colors.deepPurple }]}>
                                        {props.formatted_address}
                                    </Text>
                                </View>
                            </View>
                    )}
                </Pressable>
                <View>
                    <Pressable
                        style={{ height: 25 }}
                        onPress={props.onStarClick}
                        onPressIn={() => VibrateStart()}
                        onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={pressed ? styles.startRatingContainerActive : styles.startRatingContainer}>
                                <StarRating
                                    disabled={true}
                                    maxStars={5}
                                    emptyStar={'star'}
                                    fullStar={'star'}
                                    containerStyle={{ justifyContent: 'space-between', padding: 0, margin: 0 }}
                                    halfStar={'star-half'}
                                    iconSet={'FontAwesome'}
                                    fullStarColor={pressed ? Colors.white : Colors.orange}
                                    rating={props.rating}
                                    buttonStyle={{ marginLeft: 3 }}
                                    emptyStarColor={pressed ? Colors.white : Colors.lightGrey}
                                    starSize={18}
                                />
                                <View>
                                    <Text style={{ marginLeft: 10, color: pressed ? Colors.white : Colors.lightGrey }} note>({props.rating.toFixed(1)})</Text>
                                </View>
                            </View>
                        )}

                    </Pressable>

                    <Pressable
                        onPress={props.onRecoClick}
                        onPressIn={() => VibrateStart()}
                        onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={pressed ? styles.recoContinerActive : styles.recoContainer}>
                                <Text note style={[pressed ? styles.buttonTextStyleActive : styles.buttonTextStyle, { color: props.startDone ? Colors.dark : Colors.deepPurple }]}>
                                    Recommended by {props.totalReco}
                                </Text>
                                <Text note note style={[pressed ? styles.buttonTextStyleActive : styles.buttonTextStyle, , { color: props.startDone ? Colors.dark : Colors.deepPurple }]}>
                                    {props.lastRecomended}
                                </Text>
                            </View>

                        )}
                    </Pressable>

                </View>

            </View>
        </View>

    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        zIndex: 999999
    },
    iconContainer: {
        width: 110,
        height: 110,
        backgroundColor: Colors.white,
        justifyContent: 'center',
        borderRadius: 10,
        overflow: "hidden",
        alignItems: 'center',
    },
    icon: {
        height: '140%', width: undefined, aspectRatio: 1
    },
    header: {
        color: '#585858', width: 230, fontSize: FontSize.large, fontWeight: 'bold', flexWrap: 'wrap',
    },
    headerActive: {
        color: Colors.white, width: 230, fontSize: FontSize.large, fontWeight: 'bold', flexWrap: 'wrap',
    },
    subTitle: {
        marginBottom: 0, color: Colors.lessDarkGrey,
        maxWidth: 220, width: 220, flexWrap: 'wrap', flex: 1, fontSize: FontSize.small, flexWrap: 'wrap', textAlign: 'left'
    }, subTitleActive: {
        opacity: 0,
        marginBottom: 0, color: Colors.white, maxWidth: 220, width: 220, flex: 1, flexWrap: 'wrap', fontSize: FontSize.small, flexWrap: 'wrap', textAlign: 'left'
    }, buttonTextStyle: {
        color: Colors.deepPurple, fontSize: FontSize.medium
    }, buttonTextStyleActive: {
        color: Colors.white, fontSize: FontSize.medium
    },
    startRatingContainer: {
        flexDirection: 'row', flex: 1, paddingBottom: 3, paddingTop: 5,
        borderRadius: 5,
        width: 135,
    },
    startRatingContainerActive: {
        flexDirection: 'row', flex: 1, paddingBottom: 3, paddingTop: 5,
        backgroundColor: Colors.primary,
        borderRadius: 5,
        width: 135,
        opacity: 0,
    },
    recoContainer: {
        backgroundColor: Colors.white,
        width: 230,
    },
    recoContinerActive: {
        backgroundColor: Colors.primary,
        width: 230,
        opacity: 0
    },
    headerContainer: {
        backgroundColor: Colors.white,
    },
    headerContainerActive: {
        backgroundColor: Colors.primary,
        opacity: 0,
    }
});
export default CompanyShortProfile;
