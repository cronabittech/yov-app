import React from "react";
import RBSheet from "./Rbsheet";

const VibrateStart = () => {
}
const VibrateStop = () => {
}
const BottomPopUp = (props) => {
    return (
        <RBSheet
        ref={ props.Ref }
        closeOnDragDown={true}
        dragFromTopOnly={false}
        openDuration={500}
        height={props.height}
        isFull={props.isFull}
        onClose={props.onClose}
        customStyles={{
            wrapper: {
                backgroundColor: "transparent"
            },
            container: {
                backgroundColor: "transparent"
            }
        }}>
        {props.children}
        
        </RBSheet>
    );

}

export default BottomPopUp;