/** @format */

import React from "react";
import {
    Pressable, StyleSheet,
    Text,
    View
} from "react-native";
import Colors from '../config/colors';



const VibrateStart = () => {
}
const VibrateStop = () => {
}

const OptionItem = (props) => {
    return (
        <Pressable
            onPress={props.onPress}
            onPressIn={() => VibrateStart()}
            onPressOut={() =>  VibrateStop()}>
            {({ pressed }) => (
                    <View style={pressed ? styles.itemActive : styles.item}>
                         <Text style={styles.textStyle}>{props.text}</Text>   
                    </View>
                 )}
        </Pressable>
    );
};
const styles = StyleSheet.create({
    item:{
        flex:1,
        borderBottomWidth:0.7,
        borderBottomColor:'#cfcfcf'
    },  
    itemActive:{    
        flex:1,
        opacity: 0,
    },textStyle:{
        color:Colors.deepPurple,
        marginTop:15,
        fontSize:18,
        marginBottom:15
    }
});
export default OptionItem;
