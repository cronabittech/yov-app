import axios from 'axios';
import { getToken } from '../config/global';

const baseUrl = "https://api.yov.world/";
// const baseUrl = "http://localhost/recomendByPeople/api/";

// const baseUrl = "http://192.168.1.6/recomendByPeople/api/";
// const baseUrl = "http://192.168.43.226/recomendByPeople/api/";
const placesKey = "AIzaSyAjuSUUhXqOAZTWa1ZteHq3C5MApL-5o4E";

export function postWithOutLogin(api, data) {
    return axios.post(baseUrl + api, data);
}

export function placeAutoComplete(data) {
    // var options = {
    //     types: ['(cities)'],
    //     componentRestrictions: {country: "us"}
    //    };
    return axios.get("https://maps.googleapis.com/maps/api/place/autocomplete/json?input="+data+"&key="+placesKey);
}

export function getWithLogin(api) {
    console.log(baseUrl + api);
    console.log('token:', getToken());
    return axios.get(baseUrl + api, {
        headers: {
            Authorization: getToken() //the token is a variable which holds the token
        }
    });
}

export function deleteWithLogin(api) {
    return axios.delete(baseUrl + api, {
        headers: {
            Authorization: getToken() //the token is a variable which holds the token
        }
    });
}

export function postWithLogin(api, data) {
    return axios.post(baseUrl + api, data, {
        headers: {
            Authorization: getToken() //the token is a variable which holds the token
        }
    });
}

