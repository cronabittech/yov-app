import moment from 'moment';
import {Container, Content, Footer, Header, Item, Text} from 'native-base';
import React, {PureComponent, useRef} from 'react';
import {
  FlatList,
  Pressable,
  StatusBar,
  StyleSheet,
  TextInput,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {SubHeader} from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import {getWithLogin, postWithLogin} from '../api/api';

let intervals = null;
export default class MessageScreen extends PureComponent {
  flatList = null;

  state = {
    chat: [],
    company: undefined,
    message: '',
  };

  async doneThings(type) {
    await getWithLogin('user/state/' + 'message');
  }
  async readMessage() {
    await getWithLogin('msg/com/usr/read/' + this.state.company.id);
  }

  momentFormateDate(date) {
    if (date) {
      return moment(date + '', 'YYYY-MM-DD HH:mm:ss').format(
        'DD/MM/YYYY HH:mm',
      );
    } else {
      return moment().format('DD/MM/YYYY HH:mm');
    }
  }

  componentDidMount() {
    this.setState({company: this.props.route.params.company}, () => {
      
      this.getMessage();
      intervals = setInterval(() => {
        console.log('chat Interval');
        this.getMessage();
      }, 50000);
    });
    this.doneThings();
  }

  componentWillUnmount() {
    clearInterval(intervals);
  }

  async getMessage() {
    try {
      const response = await getWithLogin(
        'msg/com/usr/' + this.state.company.id,
      );
      this.setState({chat: response.data}, () => {
        this.readMessage();
      });
    } catch (e) {}
  }

  async sendMessage() {
    try {
      const response = await postWithLogin('msg/usr/send', {
        comp_id: this.state.company.id,
        msg: this.state.message,
      });
    } catch (e) {
      console.log(e.response);
    }
    this.setState({message: ''});
  }

  renderItem = ({item}) => {
    if (item.is_sent == 1) {
      return (
        <View style={{marginBottom: 10}}>
          <View style={styles.sendContainer}>
            <Text style={styles.sendText}>{item.msg}</Text>
          </View>
          <Text
            style={{fontSize: 12, color: Colors.lightGrey, textAlign: 'right'}}>
            {this.momentFormateDate(item.created_at)}
          </Text>
        </View>
      );
    } else {
      return (
        <View style={{marginBottom: 10}}>
          <View style={styles.receivedContainer}>
            <Text style={styles.receivedText}>{item.msg}</Text>
          </View>
          <Text style={{fontSize: 12, color: Colors.lightGrey}}>
            {this.momentFormateDate(item.created_at)}
          </Text>
        </View>
      );
    }
  };
  render() {
    return (
      <Container>
        <SubHeader text="Messages" />
        <Header
          style={{
            height: 45,
            backgroundColor: Colors.white,
            flexDirection: 'row',
            elevation: 0,
          }}>
          <View>
            <Text
              style={{
                textAlign: 'center',
                fontWeight: 'bold',
                color: Colors.lightGrey,
              }}>
              {this.state.company ? this.state.company.name : null}
            </Text>
            <Text style={{textAlign: 'center', color: Colors.lightGrey}}>
              {this.state.company ? this.state.company.formatted_address : null}
            </Text>
          </View>
        </Header>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <View style={{flex: 1, padding: 10,paddingBottom: 70}}>
          <FlatList
            contentContainerStyle={{marginBottom: 100}}
            keyboardShouldPersistTaps="always"
            data={this.state.chat}
            ref={(ref) => (this.flatList = ref)}
            keyExtractor={(item) => String(item.id)}
            maxToRenderPerBatch={1000000000}
            onContentSizeChange={() =>
              this.flatList.scrollToEnd({animated: true})
            }
            onLayout={() => this.flatList.scrollToEnd({animated: true})}
            renderItem={this.renderItem}
          />
        </View>
   
        <View
            style={{
              backgroundColor: Colors.white,
              flex: 1,
              position: 'absolute',
              alignSelf: 'baseline',
              flexDirection: 'row',
              bottom: 0,
              padding: 10,
              justifyContent: 'center',
            }}>
            <TextInput
              placeholder="Start your reply here"
              multiline={true}
              returnKeyType="done"
              blurOnSubmit={false}
              onFocus={() => this.setState({focus: true})}
              onBlur={() => this.setState({focus: false})}
              value={this.state.message}
              onChangeText={(text) => this.setState({message: text})}
              style={[
                styles.textInput,
                {
                  borderColor: this.state.focus
                    ? Colors.darkBlue
                    : Colors.lightGrey,
                },
              ]}
            />
            <Pressable
              style={styles.fullButtonContainer}
              onPress={() => this.sendMessage()}>
              <Icon
                style={{alignSelf: 'center'}}
                name="arrow-up"
                size={25}
                color={Colors.primary}></Icon>
            </Pressable>
          </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    flex: 1,
    color: Colors.deepPurple,
    textAlign: 'left',
    fontSize: FontSize.large,
    alignSelf: 'flex-end',
    padding: 0,
    paddingLeft: 5,
    minHeight: 45,
    borderRadius: 5,
    flex: 4,
    marginRight: 10,
    borderWidth: 1,
    borderColor: Colors.lightGrey,
  },
  receivedContainer: {
    borderRadius: 20,
    backgroundColor: Colors.primary,
    borderTopLeftRadius: 0,
    padding: 10,
    marginBottom: 5,
    minWidth: 10,
    maxWidth: 250,
    alignSelf: 'flex-start',
    elevation: 2,
  },
  receivedText: {
    textAlign: 'left',
    color: Colors.white,
  },
  sendContainer: {
    borderRadius: 20,
    backgroundColor: Colors.lightGrey2,
    borderTopRightRadius: 0,
    padding: 10,
    marginBottom: 5,
    minWidth: 10,
    maxWidth: 250,
    alignSelf: 'flex-end',
    elevation: 2,
  },
  sendText: {
    textAlign: 'left',
    color: Colors.dark,
  },
  fullButtonContainer: {
    flex: 1,
    borderRadius: 5,
    backgroundColor: '#F5F5F5',
    elevation: 2,
    height: 45,
    justifyContent: 'center',
    maxHeight: 45,
  },
  fullButtonContainerActive: {
    flex: 1,
    borderRadius: 5,
    backgroundColor: Colors.primary,
    elevation: 2,
    height: 45,
    justifyContent: 'center',
    opacity: 0,
    maxHeight: 45,
  },
});
