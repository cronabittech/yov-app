import moment from "moment";
import { Container, Content, Text } from 'native-base';
import React, { PureComponent } from "react";
import { FlatList, Pressable, StyleSheet, View } from 'react-native';
import { deleteWithLogin, getWithLogin } from "../api/api";
import { Button, ModelLayout, StarRate } from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';

const VibrateStart = () => {
}

const VibrateStop = () => {
}

class MyStarRatingHistory extends PureComponent {
    state = {
        company: undefined,
        manageRating: undefined,
        starHasDone: false,
        showDone: false,
        deleteAvaliable: false,
        currentStarRating: undefined,
        deletCalled: false,
        deleteItem: undefined,
        deletPopup: false,
        restorePopup: false,
        cantDelete: false,
        starRateOpacity: 0,
        isDeleted: false
    }

    momentFormateDate(date) {
        if (date) {
            return moment(date + "", "YYYY-MM-DD HH:mm:ss").format('DD/MM/YYYY hh:mm A');
        } else {
            return moment().format('DD/MM/YYYY hh:mm A');
        }
    }


    deleteItem() {
        this.setState({ deletPopup: false });
        this.deleteStarItem();
    }
    restoreItem(item) {
        this.setState({ deleteItem: item, restorePopup: true });
    }

    async deleteStarItem() {
        const response = await deleteWithLogin('star-rating-hostory/delete/' + this.state.deleteItem.id);
        const entries = Object.values(response.data);
        // var message = entries[0][0];
        // this._showMessage(message);
        this.getAllStarRating(this.props.company.id);
    }

    async restoreStarItem() {
        this.setState({ restorePopup: false });
        const response = await deleteWithLogin('star-rating-hostory/restore/' + this.state.deleteItem.id);
        const entries = Object.values(response.data);
        // var message = entries[0][0];
        // this._showMessage(message);
        this.getAllStarRating(this.props.company.id);
    }

    deleteStarRating(item, index) {
        if (index == 0) {
            this.setState({ cantDelete: true });
        } else {
            this.setState({ deleteItem: item, deletPopup: true });
        }
    }
    _alertDeleteStarRating() {
        return (
            <ModelLayout
                centeredViewStyle={{ marginTop: 220 }}
                visible={this.state.deletPopup}>
                <Text style={{ textAlign: 'center', fontSize: 18 }}>Do you want to delete this star rating from your history?</Text>
                <View style={{ width: 150, flexDirection: 'row', marginTop: 15, justifyContent: 'space-evenly' }}>
                    <Button type="textButton" text="Yes" onPress={() => this.deleteItem()} />
                    <Button type="textButton" text="No" onPress={() => this.setState({ deletPopup: false })} />
                </View>
            </ModelLayout>
        );
    }

    _alertCantDeleteStarRating() {
        return (
            <ModelLayout
                centeredViewStyle={{ marginTop: 220 }}
                visible={this.state.cantDelete}>
                <Text style={{ textAlign: 'center', fontSize: 18 }}>The latest star rating cannot be deleted</Text>
                <View style={{ width: 150, flexDirection: 'row', marginTop: 15, justifyContent: 'space-evenly' }}>
                    <Button type="textButton" text="Ok" onPress={() => this.setState({ cantDelete: false })} />
                </View>
            </ModelLayout>
        );
    }

    _alertRestoreStarRating() {
        return (
            <ModelLayout
                centeredViewStyle={{ marginTop: 220 }}
                visible={this.state.restorePopup}>
                <Text style={{ textAlign: 'center', fontSize: 18 }}>Do you want to restore this star rating?</Text>
                <View style={{ width: 150, flexDirection: 'row', marginTop: 15, justifyContent: 'space-evenly' }}>
                    <Button type="textButton" text="Yes" onPress={() => this.restoreStarItem()} />
                    <Button type="textButton" text="No" onPress={() => this.setState({ restorePopup: false })} />
                </View>
            </ModelLayout>
        );
    }

    componentDidMount() {
        this.getAllStarRating(this.props.company.id);
    }

    async getAllStarRating(data) {
        const response = await getWithLogin('star-rating/' + data);
        this.setState({ starList: response.data, currentStarRating: response.data[0] });
    }
    _renderStarItem = ({ item, index }) => {
        if (item.per_delete == 1) {
            return (
                <View style={{ flexDirection: 'column' }}>
                    <Button type="FullWithButton" text={"Star rating no: " + (this.state.starList.length - index)}
                        onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}
                        onPress={() => this.deleteStarRating(item, index)} />
                    <Pressable
                        onPress={() => this.restoreItem(item)}
                        onPressIn={() => VibrateStart()}
                        onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={[pressed ? style.ratingContainerActive : style.ratingContainer]}>
                                <Text style={{ color: Colors.deepPurple, textAlign: 'center' }}>Star rating was deleted on:</Text>
                                <Text style={{ color: Colors.deepPurple, textAlign: 'center' }}>{this.momentFormateDate(item.updated_at)}</Text>
                                <Text style={{ color: Colors.deepPurple, textAlign: 'center' }}>This star rating will be completely deleted within:21 days</Text>
                            </View>
                        )}
                    </Pressable>
                </View>
            );
        } else {
            return (

                <View style={{ flexDirection: 'column' }}>
                    <Button type="FullWithButton" text={"Star rating no: " + (this.state.starList.length - index)}
                        onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}
                        onPress={() => this.deleteStarRating(item, index)} />
                    <Pressable
                        onPress={() => this.deleteStarRating(item, index)}
                        onPressIn={() => VibrateStart()}
                        onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={[pressed ? style.ratingContainerActive : style.ratingContainer]}>
                                <Text
                                    style={[style.transparentTextStyle, { color: pressed ? Colors.white : Colors.lessDarkGrey }]}>
                                    {item.is_delete == 1 ? "You deleted our star-rating on:" :
                                        (index + 1) == this.state.starList.length ? "First star rated on:" : "You amended our star-rating on:"}
                                </Text>
                                <Text
                                    style={[style.transparentTextStyle, { color: pressed ? Colors.white : Colors.lessDarkGrey, marginTop: 5 }]}>
                                    {this.momentFormateDate(item.created_at)}
                                </Text>
                                <View style={{ marginTop: 10 }}>
                                    <View style={style.starRatingContainer}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={pressed ? style.starLabelActive : style.starLabel} >Customer services</Text>
                                        </View>
                                        <View style={style.starContainer}>
                                            <StarRate
                                                disabled={true}
                                                starSize={20}
                                                buttonStyle={{ marginLeft: 3 }}
                                                rating={item.customer_service}
                                            />
                                        </View>
                                    </View>
                                    <View style={style.starRatingContainer}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={pressed ? style.starLabelActive : style.starLabel}>Product(s)/ service</Text>
                                        </View>
                                        <View style={style.starContainer}>
                                            <StarRate
                                                disabled={true}
                                                starSize={20}
                                                buttonStyle={{ marginLeft: 3 }}
                                                rating={item.quality_of_product}
                                            />
                                        </View>
                                    </View>
                                    <View style={style.starRatingContainer}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={pressed ? style.starLabelActive : style.starLabel} >Value for money</Text>
                                        </View>
                                        <View style={style.starContainer}>
                                            <StarRate
                                                disabled={true}
                                                starSize={20}
                                                buttonStyle={{ marginLeft: 3 }}
                                                rating={item.value_for_money}
                                            />
                                        </View>
                                    </View>
                                </View>
                            </View>
                        )}
                    </Pressable>
                </View>
            );
        }
    }
    render() {
        return (
            <Container>
                <Content style={{ paddingLeft: 10, paddingRight: 10 }}>
                    {
                        this.state.starList && this.state.starList.length > 0 ?
                            <FlatList
                                keyboardShouldPersistTaps={'handled'}
                                data={this.state.starList}
                                renderItem={this._renderStarItem}
                                keyExtractor={item => item.id}
                            />
                            :
                            <Text style={{ textAlign: 'center' }}>We are getting your History</Text>
                    }
                    {
                        this._alertDeleteStarRating()
                    }
                    {
                        this._alertCantDeleteStarRating()
                    }
                    {
                        this._alertRestoreStarRating()
                    }
                    <View style={{ height: 300 }}>
                    </View>


                </Content>
            </Container>
        );
    }
}

const style = StyleSheet.create({


    button: {
        borderColor: Colors.primary,
        borderWidth: 2,
        justifyContent: 'center',
        borderRadius: 5,
        paddingLeft: 1,
        paddingRight: 1,
        paddingTop: 3,
        paddingBottom: 3,
        elevation: 2
    },
    transparentTextStyle: {
        alignSelf: 'center', fontSize: FontSize.heading
    }, ratingContainer: {
        marginTop: 15, paddingBottom: 20,
        backgroundColor: Colors.white,
    }, ratingContainerActive: {
        backgroundColor: Colors.primary,
        marginTop: 15, paddingBottom: 20, opacity: 0
    },
    starLabel: {
        color: Colors.lessDarkGrey, textAlign: 'right', fontSize: FontSize.heading
    },
    starLabelActive: {
        color: Colors.white, textAlign: 'right', fontSize: FontSize.medium
    },
    starRatingContainer: {
        flex: 1, flexDirection: 'row',
        marginTop: 8
    },
    starContainer: {
        flex: 1, marginLeft: 10,
        alignSelf: 'center'
    },
});

export default MyStarRatingHistory;