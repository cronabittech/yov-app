import moment from "moment";
import { Text } from 'native-base';
import React, { PureComponent } from "react";
import { Pressable, ScrollView, StyleSheet, View } from 'react-native';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import { deleteWithLogin, getWithLogin, postWithLogin } from "../api/api";
import { Blinkview, Button, ModelLayout, StarRate } from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';

const VibrateStart = () => {
}

const VibrateStop = () => {
}

class StarRatingScreen extends PureComponent {

    state = {
        company: undefined,
        manageRating: undefined,
        starCount1: 0,
        starCount2: 0,
        starCount3: 0,
        starHasDone: false,
        showDone: false,
        deleteAvaliable: false,
        currentStarRating: undefined,
        deletCalled: false,
        deleteItem: undefined,
        deletPopup: false,
        deletLogPopup: false,
        restorePopup: false,
        cantDelete: false,
        starRateOpacity: 1,
        isDeleted: false,
    }

    constructor(props) {
        super(props);
        this._nodes = new Map();
    }

    gotoReview() {
    }


    componentDidMount() {
        console.log(this.props.currentStarRating);
        this.getAllStarRating(this.props.company.id);

        this.setState({
            starCount1: this.props.star1,
            starCount2: this.props.star2,
            starCount3: this.props.star3,
       
            currentStarRating: this.props.currentStarRating ? this.props.currentStarRating : undefined
        }, () => {
            console.log("Current Star Rating:", this.state.currentStarRating);

        });
        if (this.props.star1 > 0 || this.props.star2 > 0 || this.props.star3 > 0) {
            this.setState({ deleteAvaliable: true });
        } else {
            this.setState({ deleteAvaliable: false });
        }
     

    }

   

    async addStarRating() {
        if (this.state.starCount1 > 0 || this.state.starCount2 > 0 || this.state.starCount3 > 0) {
            var data = {
                com_id: this.props.company.id,
                customer_service: this.state.starCount1,
                quality_of_product: this.state.starCount2,
                value_for_money: this.state.starCount3,
            }
            var response = await postWithLogin('star-rating/add', data);
            this.setState({ currentStarRating: response.data });
            this.props.updateCurrentStarRating(response.data);
            this.getAllStarRating(this.props.company.id);
        }
    }

    async deleteStarHistory() {
        
        this.manageRating(1, 0);
        this.manageRating(2, 0);
        this.manageRating(3, 0);
        this.setState({
            starCount1: 0,
            starCount2: 0,
            starCount3: 0,
            deleteAvaliable: false,
            deletCalled: true
        });
        this.props.changeTitle("Star rate us");
    } 
    async stardone(){
        var response  = await getWithLogin('user/state/'+"star");
    }  

    manageRating(order, rating) {
        this.stardone();
        this.setState({ starHasDone: true, deleteAvaliable: true, deletCalled: false });
        switch (order) {
            case 1: {
                if (rating > this.state.starCount1) {
                    this.setState({ starCount1: rating, showDone: true });
                    this.props.manageStarRating(order, rating);
                } else {
                    var newRating = rating - 1;
                    if (newRating <= 0) {
                        this.setState({ starCount1: 0, showDone: true });
                        this.props.manageStarRating(order, 0);
                    } else {
                        this.setState({ starCount1: newRating, showDone: true });
                        this.props.manageStarRating(order, newRating);
                    }
                }
            }
                break;
            case 2: {
                if (rating > this.state.starCount2) {
                    this.setState({ starCount2: rating, showDone: true });
                    this.props.manageStarRating(order, rating);
                } else {
                    var newRating = rating - 1;
                    if (newRating <= 0) {
                        this.setState({ starCount2: 0, showDone: true });
                        this.props.manageStarRating(order, 0);
                    } else {
                        this.setState({ starCount2: newRating, showDone: true });
                        this.props.manageStarRating(order, newRating);
                    }

                }
            } break;
            case 3: {
                if (rating > this.state.starCount3) {
                    this.setState({ starCount3: rating, showDone: true });
                    this.props.manageStarRating(order, rating);
                } else {
                    var newRating = rating - 1;
                    if (newRating <= 0) {
                        this.setState({ starCount3: 0, showDone: true });
                        this.props.manageStarRating(order, 0);
                    } else {
                        this.setState({ starCount3: newRating, showDone: true });
                        this.props.manageStarRating(order, newRating);
                    }

                }
            } break;
        }
        this.props.changeTitle("Edit / Delete star ratings");
        setTimeout(()=>{
            if(this.props.currentStarRating){
                if (this.state.starCount1 > 0 || this.state.starCount2 > 0 || this.state.starCount3 > 0) {
                    this.setState({ deletCalled: false });
                } else {
                    this.setState({ deletCalled: true });
                }
            }
        },50);
    }

    _showMessage(msg) {
        Snackbar.show({
            text: msg,
            duration: Snackbar.LENGTH_INDEFINITE,
            action: {
                text: 'close',
                textColor: 'red',
                onPress: () => { Snackbar.dismiss(); },
            },
        });
    }

    scrollToElement = (indexOf) => {
        var node = 0.0;
        for (var i = 0; i <= indexOf; i++) {
            node = node + this._nodes.get(i);
        }
        console.log("node " + indexOf, this._nodes.get(0));
        this.myScroll.scrollTo({ x: 0, y: (node), animated: true });
    }

    momentFormateDate(date) {
        if (date) {
            return moment(date + "", "YYYY-MM-DD HH:mm:ss").format('DD/MM/YYYY hh:mm A');
        } else {
            return moment().format('DD/MM/YYYY hh:mm A');
        }
    }

    _renderStarItem = ({ item, index }) => {
        if (item.per_delete == 1) {
            return (
                <View style={{ flexDirection: 'column' }}>

                    <Button type="FullWithButton" text={"Star rating no: " + (this.state.starList.length - index)}
                        onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}
                        onPress={() => this.deleteStarRating(item, index)} />
                    <Pressable
                        onPress={() => this.restoreItem(item)}
                        onPressIn={() => VibrateStart()}
                        onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={[pressed ? style.ratingContainerActive : style.ratingContainer]}>
                                <Text style={{ color: Colors.deepPurple, textAlign: 'center' }}>Star rating was deleted on:</Text>
                                <Text style={{ color: Colors.deepPurple, textAlign: 'center' }}>{this.momentFormateDate(item.updated_at)}</Text>
                                <Text style={{ color: Colors.deepPurple, textAlign: 'center' }}>This star rating will be completely deleted within:21 days</Text>
                            </View>
                        )}
                    </Pressable>
                </View>
            );
        } else {
            return (
                <View style={{ flexDirection: 'column' }}>
                    <Button type="FullWithButton" text={"Star rating no: " + (this.state.starList.length - index)}
                        onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}
                        onPress={() => this.deleteStarRating(item, index)} />
                    <Pressable
                        onPress={() => this.deleteStarRating(item, index)}
                        onPressIn={() => VibrateStart()}
                        onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={[pressed ? style.ratingContainerActive : style.ratingContainer]}>
                                <Text
                                    style={[style.transparentTextStyle, { color: pressed ? Colors.white : Colors.lessDarkGrey }]}>
                                    {item.is_delete == 1 ? "You deleted our star-rating on:" :
                                        (index + 1) == this.state.starList.length ? "First star rated on:" : "You amended our star-rating on:"}
                                </Text>
                                <Text
                                    style={[style.transparentTextStyle, { color: pressed ? Colors.white : Colors.lessDarkGrey, marginTop: 5 }]}>
                                    {this.momentFormateDate(item.created_at)}
                                </Text>
                                <View style={{ marginTop: 10 }}>
                                    <View style={style.starRatingContainer}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={pressed ? style.starLabelActive : style.starLabel} >Customer services</Text>
                                        </View>
                                        <View style={style.starContainer}>
                                            <StarRate
                                                disabled={true}
                                                starSize={16}
                                                buttonStyle={{ marginLeft: 3 }}
                                                rating={item.customer_service}
                                            />
                                        </View>
                                    </View>
                                    <View style={style.starRatingContainer}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={pressed ? style.starLabelActive : style.starLabel} >Quality of product / service</Text>
                                        </View>
                                        <View style={style.starContainer}>
                                            <StarRate
                                                disabled={true}
                                                starSize={16}
                                                buttonStyle={{ marginLeft: 3 }}
                                                rating={item.quality_of_product}
                                            />
                                        </View>
                                    </View>
                                    <View style={style.starRatingContainer}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={pressed ? style.starLabelActive : style.starLabel} >Value for money</Text>
                                        </View>
                                        <View style={style.starContainer}>
                                            <StarRate
                                                disabled={true}
                                                starSize={16}
                                                buttonStyle={{ marginLeft: 3 }}
                                                rating={item.value_for_money}
                                            />
                                        </View>
                                    </View>
                                </View>
                            </View>
                        )}
                    </Pressable>
                </View>
            );
        }
    }

    _renderButton() {
        if (this.state.showDone) {
            return (
                <View style={{ marginTop: 20, justifyContent: 'center' }} >
                    <Blinkview twoArrow={true} text="Done" onPress={() => this.doneStarRating()} />
                </View>
            );
        } else {
            return (
                <View style={{ marginTop: 20, justifyContent: 'center' }} >
                    <Button type="FullWidthhButton2" textColor={Colors.deepPurple} text="Close" onPress={() => this.props.close()} />
                </View>
            );
        }
    }

    _renderDeleteButton() {
        if (this.state.deleteAvaliable) {
            return (
                <Button type="FullWidthhButton2" textColor={Colors.white} color={Colors.red} text="Delete all stars selected" onPress={() => this.deleteStarHistory()} />
            );
        } else {
            return (<Text style={{
                marginTop: 0, marginBottom: 0,
                backgroundColor: "#F5F5F5",
                color: Colors.primary, textAlign: 'center',
                padding: 5,
                fontSize: FontSize.heading
            }}>Star rate us</Text>);
        }
    }

    async getAllStarRating(data) {
        const response = await getWithLogin('star-rating/' + data);
        this.setState({ starList: response.data, currentStarRating: response.data[0] });
    }

    deleteStarRating(item, index) {
        if (index == 0) {
            this.setState({ cantDelete: true });
        } else {
            this.setState({ deleteItem: item, deletPopup: true });
        }
    }

    restoreItem(item) {
        this.setState({ deleteItem: item, restorePopup: true });
    }

    async doneStarRating() {
        if (this.state.deletCalled == true) {
            if (this.state.currentStarRating) {
               
                const response = await deleteWithLogin('star-rating/delete/current/' + this.state.currentStarRating.id);
                const entries =response.data;
                this.props.updateCurrentStarRating(entries);
            }

            this.getAllStarRating(this.props.company.id);
            this.setState({ isDeleted: true, deleteAvaliable: false, showDone: false });
            this.props.unDoneStar();
            this.props.changesMade();

        } else {
            this.addStarRating();
            this.props.doneStar();
            this.setState({ showDone: false, isDeleted: false });
            this.setState({
                deleteAvaliable: true
            });
            this.props.changesMade();
        }
        
        this.props.close();
    }

    deleteItem() {
        this.setState({ deletPopup: false });
        this.deleteStarItem();
    }

    async deleteStarItem() {
        const response = await deleteWithLogin('star-rating-hostory/delete/' + this.state.deleteItem.id);
        const entries = Object.values(response.data);
        var message = entries[0][0];
        this._showMessage(message);
        this.getAllStarRating(this.props.company.id);
    }

    async restoreStarItem() {
        this.setState({ restorePopup: false });
        const response = await deleteWithLogin('star-rating-hostory/restore/' + this.state.deleteItem.id);
        const entries = Object.values(response.data);
        var message = entries[0][0];
        this._showMessage(message);
        this.getAllStarRating(this.props.company.id);
    }

    _alertRestoreStarRating() {
        return (
            <ModelLayout
                centeredViewStyle={{ marginTop: 220 }}
                visible={this.state.restorePopup}>
                <Text style={{ textAlign: 'center', fontSize: 18, color: Colors.deepPurple }}>Do you want to restore this star rating?</Text>
                <View style={{ width: 150, flexDirection: 'row', marginTop: 15, justifyContent: 'space-evenly' }}>
                    <Button type="textButton" text="Yes" onPress={() => this.restoreStarItem()} />
                    <Button type="textButton" text="No" onPress={() => this.setState({ restorePopup: false })} />
                </View>
            </ModelLayout>
        );
    }

    _alertDeleteStarRating() {
        return (
            <ModelLayout
                centeredViewStyle={{ marginTop: 220 }}
                visible={this.state.deletPopup}>
                <Text style={{ textAlign: 'center', fontSize: 18, }}>Do you want to delete this star rating from your history?</Text>
                <View style={{ width: 150, flexDirection: 'row', marginTop: 15, justifyContent: 'space-evenly' }}>
                    <Button type="textButton" text="Yes" onPress={() => this.deleteItem()} />
                    <Button type="textButton" text="No" onPress={() => this.setState({ deletPopup: false })} />
                </View>
            </ModelLayout>
        );
    }

    _alertCantDeleteStarRating() {
        return (
            <ModelLayout
                centeredViewStyle={{ marginTop: 220 }}
                visible={this.state.cantDelete}>
                <Text style={{ textAlign: 'center', fontSize: 18 }}>The latest star rating cannot be deleted</Text>
                <View style={{ width: 150, flexDirection: 'row', marginTop: 15, justifyContent: 'space-evenly' }}>
                    <Button type="textButton" text="Ok" onPress={() => this.setState({ cantDelete: false })} />
                </View>
            </ModelLayout>
        );
    }

    _RenderScreen() {
        return (
            <ScrollView
                style={{ backgroundColor: Colors.white, paddingLeft: 10, paddingRight: 10 }}
                ref={(ref) => this.myScroll = ref}
                keyboardShouldPersistTaps={'handled'}>
                {
                    this.state.starRateOpacity == 0 ?
                        <View onLayout={event => {
                            this._nodes.set(0, event.nativeEvent.layout.height)
                        }} >

                        </View>
                        :
                        <View style={{ minHeight: 500 }} onLayout={event => {
                            this._nodes.set(0, event.nativeEvent.layout.height)
                        }} >
                            <View style={{ height: 40 }}>
                                {
                                    this._renderDeleteButton()
                                }
                            </View>
                            <Pressable style={({ pressed }) => [{ opacity: pressed ? 0 : 1 }]}
                                onPress={() => this.props.close()}
                                onPressIn={() => VibrateStart()}
                                onPressOut={() => VibrateStop()}>
                                <Icon name="chevron-down" size={25}
                                    style={{ alignSelf: 'center' }} color={Colors.deepPurple} />
                            </Pressable>
                            <Text style={style.StarLabel}> Customer Service</Text>
                            <StarRate
                                disabled={false}
                                containerStyle={{ justifyContent: 'center', padding: 10 }}
                                buttonStyle={{ marginLeft: 15 }}
                                rating={this.state.starCount1}
                                order={1}
                                selectedStar={this.manageRating.bind(this)}
                            />
                            <Text style={style.StarLabel}> Quality Of Product / Service</Text>
                            <StarRate
                                disabled={false}
                                containerStyle={{ justifyContent: 'center', padding: 10 }}
                                buttonStyle={{ marginLeft: 15 }}
                                rating={this.state.starCount2}
                                order={2}
                                selectedStar={this.manageRating.bind(this)}
                            />
                            <Text style={style.StarLabel}>Value For Money</Text>
                            <StarRate
                                disabled={false}
                                containerStyle={{ justifyContent: 'center', padding: 10 }}
                                buttonStyle={{ marginLeft: 15 }}
                                rating={this.state.starCount3}
                                order={3}
                                selectedStar={this.manageRating.bind(this)}
                            />
                            {
                                this._renderButton()
                            }
                        </View>
                }
                {
                    this._alertDeleteStarRating()
                }
                {
                    this._alertCantDeleteStarRating()
                }
                {
                    this._alertRestoreStarRating()
                }

            </ScrollView>
        );
    }

    render() {
        return (
            this._RenderScreen()
        );
    }
}


const style = StyleSheet.create({

    StarLabel: {
        alignSelf: 'center', marginTop: 35,
        fontSize: FontSize.heading,
        color: Colors.darkGrey
    },

    buttonText: {
        color: Colors.deepPurple,
        paddingRight: 10,
        paddingLeft: 10,
        fontSize: FontSize.medium,
        textAlign: 'center',
        textTransform: 'capitalize'
    },
    button: {
        borderColor: Colors.primary,
        borderWidth: 2,
        justifyContent: 'center',
        borderRadius: 5,
        paddingLeft: 1,
        paddingRight: 1,
        paddingTop: 3,
        paddingBottom: 3,
        elevation: 2
    },
    transparentTextStyle: {
        alignSelf: 'center', fontSize: FontSize.heading
    }, fullButtonContainer: {
        flex: 1,
        backgroundColor: '#F5F5F5', elevation: 2, height: 30, justifyContent: 'center'
    }, fullButtonContainerActive: {
        flex: 1,
        backgroundColor: Colors.primary, elevation: 2, height: 30, justifyContent: 'center',
        opacity: 0,
    }, ratingContainer: {
        marginTop: 15, paddingBottom: 20,
        backgroundColor: Colors.white,
    }, ratingContainerActive: {
        backgroundColor: Colors.primary,
        marginTop: 15, paddingBottom: 20, opacity: 0
    },
    starLabel: {
        color: Colors.lessDarkGrey, textAlign: 'right', fontSize: FontSize.medium
    },
    starLabelActive: {
        color: Colors.white, textAlign: 'right', fontSize: FontSize.medium
    },
    starRatingContainer: {
        flex: 1, flexDirection: 'row'
    },
    starContainer: {
        flex: 1, marginLeft: 10,
        alignSelf: 'center'
    }, footerButtton: {
        marginTop: 5,
        marginBottom: 5,
        borderWidth: 0.3,
        borderColor: "#CDCDCD",
        height: 50,
        justifyContent: 'center',
        backgroundColor: Colors.white,
        borderRadius: 10, shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
    },
    footerButttonActivate: {
        borderWidth: 1,
        justifyContent: 'center',
        borderColor: "#CDCDCD",
        backgroundColor: Colors.primary,
        height: 50,
        elevation: 2,
        borderRadius: 10,
        opacity: 0
    },
});


// Exports
export default StarRatingScreen;
