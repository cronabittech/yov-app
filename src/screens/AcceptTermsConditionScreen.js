import {Container, Content, Button,Text} from 'native-base';
import React from 'react';
import {View, StyleSheet, Image } from 'react-native';
import {TermCondition} from '../component';

function AcceptTermsConditionScreen(props) {
  return (
    <Container>
      <Content padder>
        <Image
          style={[style.logo]}
          source={require('../assets/img/logo.png')}
        />
        <TermCondition></TermCondition>
        <View style={{flex: 1, alignSelf: 'stretch'}}>
          <Button rounded success block onPress={()=> props.navigation.navigate("Register")}>
            <Text>I accpet the Terms &amp; Conditions</Text>
          </Button>
        </View>
      </Content>
    </Container>
  );
}
const style = StyleSheet.create({
  logo: {
    resizeMode: 'contain',
    alignSelf: 'center',
    width: 200,
    height: 100,
    marginTop: 20,
    marginBottom: 10,
  },
});
export default AcceptTermsConditionScreen;
