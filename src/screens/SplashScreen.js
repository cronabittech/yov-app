import React, { PureComponent } from "react";
import { View, Text, StyleSheet } from 'react-native'
import { Image, StatusBar } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { Spinner } from "native-base";
import Action from '../config/action';
import { setToken } from '../config/global';

class SplashScreen extends PureComponent {

    constructor() {
        super();
        AsyncStorage.getItem('username').then((value) => {
            var username = value;
            if (value) {
                AsyncStorage.getItem('password').then((val) => {
                    var password = val;
                    if(username == ""){
                    this.props.navigation.navigate('Login');
                        
                    }else{
                        this.props.login({ username, password });
                    }
                });
            } else {
                setTimeout(() => {
                    this.props.navigation.navigate('Login');
                }, 1000);
            }
        });

    }
    componentDidMount() {
        // this.props.navigation.reset({ routes: [{ name: 'Home' }] });

    }


    componentDidUpdate(prevProps) {


        if (prevProps.message !== this.props.message) {
            switch (this.props.type) {
                case Action.LOGIN_FAIL:
                    setTimeout(() => {
                        this.props.navigation.navigate('Login');
                    }, 1000);
                    break;
                case Action.LOGIN_SUCCESS:
                    console.log("token");
                    AsyncStorage.setItem('token', this.props.message);
                    setToken(this.props.message);
                    this.props.navigation.reset({ routes: [{ name: 'Home' }] });
                    break;
            }
        }

    }
    _renderSpinner() {
        if (this.props.isLoading) {
            return (<Spinner color='green' />);
        }
    }
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
                <StatusBar backgroundColor="white" barStyle="dark-content" />
                <Image style={[style.logo]}
                    source={require('../assets/img/logo.png')} />
                {
                    this._renderSpinner()
                }

            </View>
        );
    }
}
const style = StyleSheet.create({
    logo: {
        width: 300,
        padding: 20,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
});

const mapStateToProps = (state) => {
    console.log('Login State:');
    console.log(state.login);

    return {
        // counter: state.counterReducer.counter,
        error: state.login.error,
        isLoading: state.login.isLoading,
        message: state.login.message,
        type: state.login.type,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (data) => {
            dispatch({
                type: Action.LOGIN,
                value: data
            })
        }
    };
};

// Exports
export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);