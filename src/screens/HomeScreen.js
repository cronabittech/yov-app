import { Card, CardItem, Container, Text } from 'native-base';
import React, { PureComponent } from "react";
import { ScrollView, StatusBar, StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


class HomeScreen extends PureComponent {

    render() {
        return (
            <ScrollView>
                <Container style={{ flex: 1, flexDirection: 'column', height: '100%' }}>
                    <StatusBar backgroundColor="#5eb44d" barStyle="light-content" />

                    <View style={style.cardHolder}>
                        <Card style={style.card} onTouchStart={() => this.props.navigation.navigate('Search')}>
                            <CardItem style={{ justifyContent: 'center', marginTop: 10 }}>
                                <View style={[style.iconHolder, { backgroundColor: '#a7e054' }]}>
                                    <Icon active name='search' size={40} color="#fff" style={{ marginTop: 10 }}></Icon>
                                </View>
                            </CardItem>
                            <CardItem header style={{ justifyContent: 'center' }}>
                                <Text style={[style.cardHeader, { color: '#91d343' }]}>Search</Text>
                            </CardItem>
                        </Card>
                        <Card style={[style.card, { marginLeft: 10, marginRight: 0 }]}  onTouchStart={() => this.props.navigation.navigate('History')}>
                            <CardItem style={{ justifyContent: 'center', marginTop: 10 }}>
                                <View style={[style.iconHolder, { backgroundColor: '#37ccf5' }]}>
                                    <Icon active name='history' size={40} color="#fff" style={{ marginTop: 10 }}></Icon>
                                </View>
                            </CardItem>
                            <CardItem header style={{ justifyContent: 'center' }}>
                                <Text style={[style.cardHeader, { color: '#29bcf7' }]}>TimeLine</Text>
                            </CardItem>
                        </Card>
                    </View>

                    <View style={style.cardHolder}>
                        <Card style={style.card} onTouchStart={() => this.props.navigation.navigate('Rated')}>
                            <CardItem style={{ justifyContent: 'center', marginTop: 10 }}>
                                <View style={[style.iconHolder, { backgroundColor: '#f7a172' }]}>
                                    <Icon active name='star' size={40} color="#fff" style={{ marginTop: 10 }}></Icon>
                                </View>
                            </CardItem>
                            <CardItem header style={{ justifyContent: 'center' }}>
                                <Text style={[style.cardHeader, { color: '#f78b5e' }]}>Rated</Text>
                            </CardItem>
                        </Card>
                        <Card style={[style.card, { marginLeft: 10, marginRight: 0 }]} onTouchStart={() => this.props.navigation.navigate('Recommended')}>
                            <CardItem style={{ justifyContent: 'center', marginTop: 10 }}>
                                <View style={[style.iconHolder, { backgroundColor: '#6f76f4' }]}>
                                    <Icon active name='thumbs-up' size={40} color="#fff" style={{ marginTop: 10 }}></Icon>
                                </View>
                            </CardItem>
                            <CardItem header style={{ justifyContent: 'center' }}>
                                <Text style={[style.cardHeader, { color: '#5661f7' }]}>Recomended</Text>
                            </CardItem>
                        </Card>
                    </View>


                    <View style={style.cardHolder}>
                        <Card style={style.card} onTouchStart={() => this.props.navigation.navigate('Favourites')}>
                            <CardItem style={{ justifyContent: 'center', marginTop: 10 }}>
                                <View style={[style.iconHolder, { backgroundColor: '#f67c7f' }]}>
                                    <Icon active name='gratipay' size={40} color="#fff" style={{ marginTop: 10 }}></Icon>
                                </View>
                            </CardItem>
                            <CardItem header style={{ justifyContent: 'center' }}>
                                <Text style={[style.cardHeader, { color: '#f76665' }]}>Favourites</Text>
                            </CardItem>
                        </Card>
                        <Card style={[style.card, { marginLeft: 10, marginRight: 0 }]} onTouchStart={() => this.props.navigation.navigate('Created')}>
                            <CardItem style={{ justifyContent: 'center', marginTop: 10 }}>
                                <View style={[style.iconHolder, { backgroundColor: '#6f76f4' }]}>
                                    <Icon active name='plus-circle' size={40} color="#fff" style={{ marginTop: 10 }}></Icon>
                                </View>
                            </CardItem>
                            <CardItem header style={{ justifyContent: 'center' }}>
                                <Text style={[style.cardHeader, { color: '#5661f7' }]}>Projects</Text>
                            </CardItem>
                        </Card>
                    </View>
                </Container>
            </ScrollView>

        );
    }
}
export default HomeScreen;

const style = StyleSheet.create({

    cardHolder: {
        flexDirection: 'row', paddingLeft: 15, paddingRight: 15, marginTop: 10
    },
    card: {
        height: 160, flex: 1, marginRight: 10, flexDirection: 'column'
    },
    cardHeader: {
        fontWeight: '700', fontSize: 18,
    },
    iconHolder: {
        height: 65, width: 65, alignItems: 'center', borderRadius: 100
    }
});


