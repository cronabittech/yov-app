import { Container, Content, Item, Text } from 'native-base';
import React, { PureComponent } from "react";
import { Pressable, StyleSheet, TextInput, View } from 'react-native';
import { postWithLogin } from '../api/api';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';



const VibrateStart = () => {
}
const VibrateStop = () => {
}

class FavouriteScreen extends PureComponent {

    state = {
        mr: true,
        ms: false,
        dr: false,
        sir: false,
        name_title:'Mr',
        first_name:'',
        surname:'',
        job_title:'',
        landline:'',
        mobile:'',
        email:'',
        notes:'',
    }
    
    defaultSelect() {
        this.setState({
            mr: false,
            ms: false,
            dr: false,
            sir: false,
        });
    }

    componentDidMount(){
        this.addFav();
        if(this.props.favData){
            this.setState({
                name_title : this.props.favData.name_title,
                first_name : this.props.favData.first_name,
                surname : this.props.favData.surname,
                job_title : this.props.favData.job_title,
                landline : this.props.favData.landline,
                mobile : this.props.favData.mobile,
                email : this.props.favData.email,
                notes : this.props.favData.notes,
            },()=>{
                this.defaultSelect();
                switch (this.state.name_title) {
                    case 'Mr':
                        this.setState({ mr: true,name_title:'Mr' });
                        break;
                    case 'Dr':
                        this.setState({ dr: true,name_title:'Dr' });
                        break;
                    case 'Ms':
                        this.setState({ ms: true,name_title:'Ms' });
                        break;
                    case 'Sir':
                        this.setState({ sir: true,name_title:'Sir' });
                        break;
                }
            });
        }
    }

    async addFav(){
        var respose = await postWithLogin('add/fav',{com_id:this.props.company.id});
        console.log(respose.data);
        this.props.updateFavData(respose.data);
    }

    async addData(){
        var data={
            name_title : this.state.name_title,
            first_name : this.state.first_name,
            surname : this.state.surname,
            job_title : this.state.job_title,
            landline : this.state.landline,
            mobile : this.state.mobile,
            email : this.state.email,
            notes : this.state.notes,
        }
        var response  = await postWithLogin('update/fav/'+this.props.company.id,data);
        console.log(response.data);
        this.props.updateFavData(response.data);
        this.props.close();
    }

    selectOption(option) {
        this.defaultSelect();
        switch (option) {
            case 'mr':
                this.setState({ mr: true,name_title:'Mr' });
                break;
            case 'dr':
                this.setState({ dr: true,name_title:'Dr' });
                break;
            case 'ms':
                this.setState({ ms: true,name_title:'Ms' });
                break;
            case 'sir':
                this.setState({ sir: true,name_title:'Sir' });
                break;
        }
    }

    render() {
        return (
            <Container >
                <Content  style={{backgroundColor: '#F5F5F5'}}>
                {/* <Content  style={{backgroundColor: '#F5F5F5'}}> */}

                    <View full iconRight style={{ paddingLeft: 10, paddingRight: 10, elevation: 2, marginBottom: 0, backgroundColor: '#F5F5F5', height: 30, justifyContent: 'center' }} >
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Text style={{ color: Colors.primary, fontSize: FontSize.large }}>Add contact details</Text>
                        </View>
                    </View>

                    <View style={{ justifyContent: 'center',marginTop:10 }}>
                        <Text style={{ textAlign: 'center', color: '#585858', fontSize: FontSize.heading, fontWeight: 'bold', }}>{this.props.company.name}</Text>
                        <Text style={{ textAlign: 'center', color: Colors.lightGrey, fontSize: FontSize.medium, fontWeight: 'bold', }}>{this.props.company.formatted_address}</Text>
                    </View>
                    <View style={{ paddingLeft: 5, paddingRight: 5, marginTop: 10, flex: 1, marginBottom: 1, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: Colors.lightGrey2 }}>
                        <Pressable
                            style={this.state.mr ? styles.optionActive : styles.optionInActive}
                            onPress={() => this.selectOption('mr')}>
                            <View>
                                <Text style={this.state.mr ? styles.optionTextActive : styles.optionTextInActive}>Mr</Text>
                            </View>
                        </Pressable>
                        <Pressable
                            style={this.state.ms ? styles.optionActive : styles.optionInActive}
                            onPress={() => this.selectOption('ms')}>
                            <View>
                                <Text style={this.state.ms ? styles.optionTextActive : styles.optionTextInActive}>Ms</Text>
                            </View>
                        </Pressable>
                        <Pressable
                            style={this.state.sir ? styles.optionActive : styles.optionInActive}
                            onPress={() => this.selectOption('sir')}>
                            <View>
                                <Text style={this.state.sir ? styles.optionTextActive : styles.optionTextInActive}>Sir</Text>
                            </View>
                        </Pressable>
                        <Pressable
                            style={this.state.dr ? styles.optionActive : styles.optionInActive}
                            onPress={() => this.selectOption('dr')}>
                            <View>
                                <Text style={this.state.dr ? styles.optionTextActive : styles.optionTextInActive}>Dr</Text>
                            </View>
                        </Pressable>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 5, paddingRight: 5, backgroundColor: Colors.lightGrey2 }}>
                        <Item regular style={{ flex: 1, height: 40, marginRight: 5, borderRadius: 5, marginTop: 5, backgroundColor: Colors.white }} >
                            <TextInput placeholder='Contact first name' returnKeyType="done"
                                blurOnSubmit={false}
                                value={this.state.first_name}
                                onChangeText={(text)=> this.setState({first_name:text})}
                                style={styles.textInput}
                            />
                        </Item>
                        <Item regular style={{ flex: 1, height: 40, marginLeft: 5, borderRadius: 5, marginTop: 5, backgroundColor: Colors.white }} >
                            <TextInput placeholder='Surname' returnKeyType="done"
                                blurOnSubmit={false}
                                value={this.state.surname}
                                onChangeText={(text)=> this.setState({surname:text})}
                                style={styles.textInput}
                            />
                        </Item>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 5, paddingRight: 5, backgroundColor: Colors.lightGrey2 }}>
                        <Item regular style={{ flex: 1, height: 40, marginRight: 5, borderRadius: 5, marginTop: 5, backgroundColor: Colors.white }} >
                            <TextInput placeholder='Mobile number' returnKeyType="done"
                                blurOnSubmit={false}
                                value={this.state.mobile}
                                onChangeText={(text)=> this.setState({mobile:text})}
                                style={styles.textInput}
                            />
                        </Item>
                        <Item regular style={{ flex: 1, height: 40, marginLeft: 5, borderRadius: 5, marginTop: 5, backgroundColor: Colors.white }} >
                            <TextInput placeholder='Landline phone number' returnKeyType="done"
                                blurOnSubmit={false}
                                value={this.state.landline}
                                onChangeText={(text)=> this.setState({landline:text})}
                                style={styles.textInput}
                            />
                        </Item>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 5, paddingRight: 5, backgroundColor: Colors.lightGrey2 }}>
                        <Item regular style={{ flex: 1, height: 40, borderRadius: 5, marginTop: 5, backgroundColor: Colors.white }} >
                            <TextInput placeholder='Job Title' returnKeyType="done"
                                blurOnSubmit={false}
                                value={this.state.job_title}
                                onChangeText={(text)=> this.setState({job_title:text})}
                                style={styles.textInput}
                            />
                        </Item>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 5, paddingRight: 5, backgroundColor: Colors.lightGrey2 }}>
                        <Item regular style={{ flex: 1, height: 40, borderRadius: 5, marginTop: 5, backgroundColor: Colors.white }} >
                            <TextInput placeholder='Email address' returnKeyType="done"
                                blurOnSubmit={false}
                                value={this.state.email}
                                onChangeText={(text)=> this.setState({email:text})}
                                style={styles.textInput}
                            />
                        </Item>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 5, paddingRight: 5, backgroundColor: Colors.lightGrey2 }}>
                        <Pressable style={({ pressed }) => [
                            pressed ? styles.buttonActive : styles.button, { backgroundColor: Colors.white, marginRight: 5 }
                        ]} onPress={()=>this.props.close()}
                        onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                            <Text style={{
                                fontWeight: 'bold', color: Colors.primary, textAlign: 'center',
                                textTransform: 'capitalize'
                            }}>Skip</Text>
                        </Pressable>
                        <Pressable style={({ pressed }) => [
                            pressed ? styles.buttonActive : styles.button, { marginLeft: 5 }
                        ]} onPress={()=>this.addData()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                            <Text style={{
                                fontWeight: 'bold', color: Colors.white, textAlign: 'center',
                                textTransform: 'capitalize'
                            }}>Done</Text>
                        </Pressable>
                    </View>
                </Content>
            </Container>
        );
    }
    
}
export default FavouriteScreen;


const styles = StyleSheet.create({

    itemStyle: {

    },
    textInput: {
        flex: 1,
        backgroundColor: Colors.white,
        color: Colors.deepPurple,
        fontSize: FontSize.large,
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 0,
        paddingBottom: 0
    },
    searchContainer: {
        paddingLeft: 8,
        paddingRight: 8,
        marginTop: 10
    },
    listItem: {
        backgroundColor: Colors.white,
        padding: 8,
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    listItemActive: {
        backgroundColor: Colors.primary,
        padding: 8, flex: 1,
        opacity: 0,
    },
    itemHeader: {
        flex: 1,
        fontSize: FontSize.heading,
        color: Colors.deepPurple,
        textAlign: 'left'
    },
    itemHeaderActive: {
        fontSize: FontSize.heading,
        color: Colors.white,
        textAlign: 'left'
    },
    itemHeaderActive2: {
        fontSize: FontSize.heading,
        color: '#585858',
        textAlign: 'left'
    },
    itemText: {
        fontSize: FontSize.medium,
        color: Colors.lessDarkGrey,
        textAlign: 'left',
        flex: 1,
    },
    itemTextActive: {
        fontSize: FontSize.medium,
        color: Colors.white,
        textAlign: 'left'
    },
    optionInActive: {
        height: 30,
        width: 70,
        justifyContent: 'center',
        backgroundColor: Colors.lightGrey2,
        elevation: 2,
        borderRadius: 5,
        borderWidth: 0.5,
        borderColor: "#CDCDCD",
    },
    optionActive: {
        height: 30,
        width: 70,
        justifyContent: 'center',
        backgroundColor: Colors.white,
        elevation: 2,
        borderRadius: 5,
        borderWidth: 0.5,
        borderColor: "#CDCDCD",
    },
    optionTextInActive: {
        textAlign: 'center',
        color: Colors.darkGrey
    },
    optionTextActive: {
        textAlign: 'center',
        color: Colors.primary

    },
    buttonActive: {
        opacity: 0,
        elevation: 1,
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 35,
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
    },
    button: {
        elevation: 1,
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10,
        borderWidth:0.3,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 35,
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
    }
})



