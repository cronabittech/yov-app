import moment from 'moment';
import {Container, Content, Spinner, Text} from 'native-base';
import React, {PureComponent} from 'react';
import {StyleSheet, View} from 'react-native';
import {getWithLogin} from '../api/api';
import {Button, SubHeader, CompanyShortProfile} from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';

export default class PromotionDetailsScreen extends PureComponent {
  state = {
    prom_id: 0,
    isLoading: true,
    promotion: undefined,
    isFinished: false,
  };

  momentFormateDate(date) {
    if (date) {
      return moment(date + '', 'YYYY-MM-DD').format('DD/MM/YYYY');
    } else {
      return moment().format('DD/MM/YYYY');
    }
  }

  isDateBiggerThanToday(firstdate) {
    var momentStartDate = moment(firstdate, 'YYYY-MM-DD');
    var momentEndDate = moment();
    return momentEndDate.diff(momentStartDate, 'days') >= 1 ? true : false;
  }

  checkDate(data) {
    return true;
  }

  componentDidMount() {
    this.setState({prom_id: this.props.route.params.id}, () => {
      if (this.state.prom_id != 0) {
        this.getPromotion();
      }
    });
    this.props.navigation.addListener('focus', () => {
      if (this.state.prom_id != 0) {
        this.getPromotion();
      }
    });
  }

  _renderContent() {
    if (this.state.isFinished) {
      return (
        <Content padder>
          <Text style={styles.error}>
            You have already finished this promotion
          </Text>
        </Content>
      );
    } else if (
      this.state.promotion.is_active === 0 ||
      this.state.promotion.is_delete === 1
    ) {
      return (
        <Content padder>
          <Text style={styles.error}>
            Promotion is not active at the moment
          </Text>
        </Content>
      );
    } else if (this.isDateBiggerThanToday(this.state.promotion.end_date)) {
      return (
        <Content padder>
          <Text style={styles.error}>Promotion is expired</Text>
        </Content>
      );
    } else {
      return (
        <Content padder>
          <Button
            type="border2"
            text="Start a survey"
            onPress={() => {
              this.props.navigation.navigate('PromotionSurvey', {
                promotion: this.state.promotion,
              });
            }}></Button>
        </Content>
      );
    }
  }

  async getPromotion() {
    this.setState({isLoading: true});
    try {
      const response = await getWithLogin(
        'promption/detail/' + this.state.prom_id,
      );
      console.log(response.data);
      this.setState({
        promotion: response.data,
        isFinished: response.data.isFinished,
        isLoading: false,
      });
    } catch (e) {
      console.log(e.response);
      this.setState({isLoading: false});
    }
  }

  getLastRecommendDate() {
    const company = this.state.promotion.company;
    if (company.totalRecommendation == 0) {
      return 'Recommend us';
    } else {
      if (company.lastRecommendationDate) {
        return (
          'Last recommend: ' +
          this.momentFormateDate(company.lastRecommendationDate.created_at)
        );
      } else {
        return 'Recommend us';
      }
    }
  }
  getStarRating() {
    const starRating = this.state.promotion.company.starRating;
    if (starRating && starRating.is_delete === 0) {
      return (
        (starRating.customer_service +
          starRating.quality_of_product +
          starRating.value_for_money) /
        3
      );
    }
    return 0;
  }

  render() {
    if (this.state.isLoading) {
      return (
        <Container>
          <Spinner color="green" style={{alignSelf: 'center'}} />
        </Container>
      );
    } else {
      const company = this.state.promotion.company;
      return (
        <Container>
          <SubHeader text="Promotion details"></SubHeader>
          <View style={{padding: 10, height: 90}}>
            <CompanyShortProfile
              icon={company.icon}
              name={company.name}
              totalReco={company.totalRecommendation}
              formatted_address={company.formatted_address}
              rating={this.getStarRating()}
              lastRecomended={this.getLastRecommendDate()}
            />
          </View>
          <View
            style={{
              paddingTop: 6,
              paddingBottom: 6,
              paddingLeft: 10,
              paddingRight: 10,
              marginTop: 45,
              backgroundColor: Colors.lightGrey2,
            }}>
            <Text style={{textAlign: 'center', color: Colors.primary}}>
              Voucher details
            </Text>
          </View>
          <View style={{padding: 10, fontSize: 20}}>
            <Text style={[styles.text, {marginTop: 5}]}>
              Claim a{' '}
              <Text style={{color: Colors.primary}}>
                {this.state.promotion.benifits}
              </Text>
            </Text>
            <Text style={styles.text}>
              Complete our 3 minutes survey about our{' '}
              <Text style={{color: Colors.primary}}>
                {this.state.promotion.question}
              </Text>{' '}
              {this.state.promotion.p_condition !== null ? 'and ' : null}
              {this.state.promotion.p_condition !== null ? (
                <Text style={{color: Colors.primary}}>
                  {this.state.promotion.p_condition}
                </Text>
              ) : null}{' '}
              to validate your claim.
            </Text>
            <Text style={styles.text}>
              This offer is available from{' '}
              <Text style={{color: Colors.primary}}>
                {this.momentFormateDate(this.state.promotion.start_date)}
              </Text>{' '}
              to{' '}
              <Text style={{color: Colors.primary}}>
                {this.momentFormateDate(this.state.promotion.end_date)}
              </Text>
            </Text>
          </View>
          {this._renderContent()}
        </Container>
      );
    }
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: FontSize.extraLarge,
    fontWeight: 'bold',
    color: Colors.lightGrey,
    marginTop: 20,
    textAlign: 'center',
  },
  error: {
    fontSize: FontSize.extraLarge,
    fontWeight: 'bold',
    color: Colors.red,
    marginTop: 20,
    textAlign: 'center',
  },
});
