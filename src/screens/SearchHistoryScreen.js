import moment from "moment";
import { Container, Content, Spinner } from 'native-base';
import React, { PureComponent } from "react";
import { FlatList, TouchableOpacity, View } from 'react-native';
import { getWithLogin } from "../api/api";
import { Button, SearchHistoryItem, SubHeader } from '../component';
import Colors from '../config/colors';
import {callNumber, doMail,getType} from '../config/util';

const VibrateStart = () => {
}

const VibrateStop = () => {
}

class SearchHistoryScreen extends PureComponent {

    state = {
        isLoading: false,
        searchList: undefined
    }
    componentDidMount() {
        this.props.navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity style={{ marginRight: 5 }}>
                    <Button type="header" text="Search" onPress={() => this.props.navigation.navigate('Search')} />
                </TouchableOpacity>
            ),
        });
        this.getSearchHistory();

    }

    momentFormateDate(date) {
        if (date) {
            return moment(date + "", "YYYY-MM-DD HH:mm:ss").format('DD/MM/YYYY hh:mm A');
        } else {
            return moment().format('DD/MM/YYYY hh:mm A');
        }
    }

    async getSearchHistory() {
        this.setState({ isLoading: true });
        try {
            var data = await getWithLogin("searchHistory");
            console.log(data.data);
            this.setState({ searchList: data.data })
            this.setState({ isLoading: false });
        } catch (error) {
            console.log(error.response);
        }
    }
    gotoCompany(item) {
        this.props.navigation.navigate('Recommend', { place_id: item.place_id, });
    }
    gotoRecommend(item) {
        this.props.navigation.navigate('Recommend', { place_id: item.place_id, defaultOpen: 'recommend_business' });
    }

    gotoStarRating(item) {
        this.props.navigation.navigate('Recommend', { place_id: item.place_id, defaultOpen: 'starRate' });
    }

    gotoCompanyMessageScreen(item) {
        this.props.navigation.navigate('Recommend', {
            place_id: item.place_id, defaultOpen: 'messages_search'
        });
    }
    _renderItem = ({ item, index }) => {
        return (
            <SearchHistoryItem
                onHeaderClick={() => this.gotoCompany(item)}
                onStarClick={() => this.gotoCompany(item)}
                // onRecoClick={()=> item.totalRecommendation == 0 ? this.gotoRecommend(item) : this.gotoCompany(item)}
                onRecoClick={() => this.gotoCompany(item)}
                icon={item.company.icon}
                rating={item.totalrating}
                name={item.company.name}
                // onStarClick={item.totalrating > 0 ? ()=>{} : ()=> this.gotoStarRating(item) }
                onStarClick={() => this.gotoCompany(item)}
                emptyStarColor={item.totalrating > 0 ? Colors.lightGrey : Colors.deepPurple}
                totalreco={item.totalRecommendation}
                lastreco={item.totalRecommendation > 0 ? "" + this.momentFormateDate(item.lastRecommendationDate.created_at) : "Recommend us"}
                formatted_address={getType(item.company.category)}
                phone={() => item.company.formatted_phone_number ? callNumber(item.company.formatted_phone_number) : null}
                // mail={() => item.company.email ? doMail(item.company.email) : null }
                mail={ () => this.gotoCompanyMessageScreen(item)}
            />
        );
    }

    _renderScreen() {
        if (this.state.isLoading) {
            return (<Container>
                <SubHeader text="Businesses searched" />
                <Spinner color='green' style={{ alignSelf: 'center', marginTop: 20 }} />
            </Container>);
        } else {
            return (
                <Container >
                    <SubHeader text="Searched businesses" />
                    <Content>
                        <FlatList
                            keyboardShouldPersistTaps={'handled'}
                            data={this.state.searchList}
                            renderItem={this._renderItem}
                            keyExtractor={item => item.id}
                            ItemSeparatorComponent={(props) => {
                                return (<View style={{ height: 1, backgroundColor: "#CDCDCD", marginLeft: 100, marginRight: 10 }} />);
                            }}
                        />
                    </Content>
                </Container >
            );
        }
    }

    render() {
        return (
            this._renderScreen()
        );
    }

}

export default SearchHistoryScreen;