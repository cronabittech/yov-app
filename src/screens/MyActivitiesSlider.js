import moment from "moment";
import { Container, Content, Text } from 'native-base';
import React, { PureComponent } from "react";
import { FlatList, Pressable, StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { deleteWithLogin, getWithLogin } from "../api/api";
import { Button, ModelLayout } from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';


const VibrateStart = () => {
}

const VibrateStop = () => {
}

export default class MyActivitiesSlider extends PureComponent {
    state = {
        logList: undefined,
        deleteLog: undefined,
        deletLogPopup: false,
        lastLog: undefined
    }

    setDeleteLog(item) {
        this.setState({
            deleteLog: item
        }, () => {
            this.setState({
                deletLogPopup: true
            })
        })
    }
    componentDidMount() {
        this.getLog();
    }

    async getLog() {
        const response = await getWithLogin('user/company/log/' + this.props.company.id);
        console.log(response.data);
        if (response.data.length > 0) {
            this.setState({ logList: response.data, lastLog: response.data[0], deletLogPopup: false });
        } else {
            // this.props.updateLastLog(undefined);
            this.setState({ logList: response.data, lastLog: undefined, deletLogPopup: false });
        }
    }

    momentFormateDate(date) {
        if (date) {
            return moment(date + "", "YYYY-MM-DD HH:mm:ss").format('DD/MM/YYYY HH:mm');
        } else {
            return moment().format('DD/MM/YYYY HH:mm');
        }
    }

    getlogIconName(logtype) {
        var iconName = "";
        switch (logtype) {
            case "qr":
                iconName = "qrcode"
                break;
            case "search":
                iconName = "search"
                break;
            case "star":
                iconName = "star"
                break;
            case "recomended":
                iconName = "check"
                break;
            case "note":
                iconName = "sticky-note"
                break;
            case "favorite":
                iconName = "heart"
                break;
            default:
                break;
        }
        return iconName
    }

    async deleteLog() {
        const response = await deleteWithLogin('delete/company/log/' + this.state.deleteLog.id);
        this.getLog();

    }

    _alertDeleteLog() {
        return (
            <ModelLayout
                visible={this.state.deletLogPopup}>
                <Text style={{ textAlign: 'center', fontSize: 18, }}>Do you want to delete this activity?</Text>
                <View style={{ width: 150, flexDirection: 'row', marginTop: 15, justifyContent: 'space-evenly' }}>
                    <Button type="textButton" text="Yes" onPress={() => this.deleteLog()} />
                    <Button type="textButton" text="No" onPress={() => this.setState({ deletLogPopup: false })} />
                </View>
            </ModelLayout>
        );
    }

    _renderLogItem = ({ item, index }) => {
        return (
            <Pressable style={({ pressed }) => [
                pressed ? style.footerButttonActivate : style.footerButtton
            ]} onPress={() => this.setDeleteLog(item)} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Icon style={{ marginLeft: 10 }} name={this.getlogIconName(item.log_type)} size={25} color={Colors.primary} />
                    <Text style={{ alignItems: 'center', fontSize: 14, }}>{item.log}{this.momentFormateDate(item.created_at)}</Text>
                    <Icon style={{ marginRight: 10 }} name="chevron-right" color={Colors.deepPurple} />
                </View>
            </Pressable>
        );
    }

    render() {
        return (
            <Container>
                <Content style={{ paddingLeft: 10, paddingRight: 10 }}>
                    <View full iconRight style={{ elevation: 2, marginBottom: 0, backgroundColor: '#F5F5F5', height: 30, justifyContent: 'center' }} >
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Text style={{ color: Colors.primary, fontSize: FontSize.large }}>My Actvities </Text>
                        </View>
                    </View>
                    {
                        this.state.logList && this.state.logList.length > 0 ?
                            <FlatList
                                keyboardShouldPersistTaps={'handled'}
                                data={this.state.logList}
                                renderItem={this._renderLogItem}
                                keyExtractor={item => item.id}
                            />
                            :
                            <Text >We are getting your activities</Text>
                    }
                    <View style={{ height: 300 }}>

                    </View>
                </Content>
                {
                    this._alertDeleteLog()
                }
            </Container>
        );
    }
}
const style = StyleSheet.create({
    footerButtton: {
        marginTop: 5,
        marginBottom: 5,
        borderWidth: 0.3,
        borderColor: "#CDCDCD",
        height: 50,
        justifyContent: 'center',
        backgroundColor: Colors.white,
        borderRadius: 10, shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
    },
    footerButttonActivate: {
        borderWidth: 1,
        justifyContent: 'center',
        borderColor: "#CDCDCD",
        backgroundColor: Colors.primary,
        height: 50,
        elevation: 2,
        borderRadius: 10,
        opacity: 0
    },
});