import { Container, Item, Spinner, Text } from 'native-base';
import React, { PureComponent } from 'react';
import {
  FlatList,
  Keyboard,
  LogBox,
  Pressable,
  StatusBar,
  StyleSheet,
  TextInput,
  View
} from 'react-native';
import { connect } from 'react-redux';
import { getWithLogin, postWithOutLogin } from '../api/api';
import { Button, SubHeader } from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import { getSerachType } from '../config/global';

class SearchScreen extends PureComponent {
  state = {
    query: '',
    list: undefined,
    hideItem: undefined,
    allCompany: true,
    blinkItem: undefined,
    subtitle: '',
  };
  constructor() {
    super();
  }

  _searchQuery() {
    this.searchCompany(this.state.query);
  }

  async searchCompany(com) {
    try {
      var response = await postWithOutLogin('all-search-com', {company: com});
      console.log(response);
      this.searchdone();
      let data = response.data;
      if (data.hasOwnProperty('isYov')) {
        this.setState({query: com, allCompany: true, list: data.company});
      } else {
        this.setState({query: com, allCompany: false, list: data.predictions});
      }
    } catch (e) {
      console.log(e.response);
    }
  }

  componentDidMount() {
    LogBox.ignoreLogs(['Warning: ...']);
    LogBox.ignoreAllLogs();

    this.props.navigation.addListener('focus', () => {
      if (getSerachType() == 'profile_note_search') {
        this.setState({subtitle: 'Add a note about a business'});
      } else if (getSerachType() == 'phone_search') {
        this.setState({subtitle: 'Call a business'});
      } else if (getSerachType() == 'recommend_search') {
        this.setState({subtitle: 'Recommend a business'});
      } else if (getSerachType() == 'star_rating_search') {
        this.setState({subtitle: 'Star rate a business'});
      } else if (getSerachType() == 'feedback_search') {
        this.setState({subtitle: 'Submit feedback to a business'});
      } else if (getSerachType() == 'favorite_search') {
        this.setState({subtitle: 'Add businesss to favourites'});
      } else if (getSerachType() == 'messages_search') {
        this.setState({subtitle: 'Message a business'});
      } else {
        this.setState({subtitle: 'Search and communicate'});
      }
      // setTimeout(()=>{this.ref.focus()},1000);
    });

    this.props.navigation.setOptions({
      headerRight: () => (
        <Button
          type="header"
          text=""
          onPress={() => this._searchQuery()}
        />
      ),
    });
  }
  async searchdone() {
    var response = await getWithLogin('user/state/' + 'search');
    var response = await getWithLogin('user/state/' + 'activities');
  }

  componentDidUpdate() {}

  gotoDetail(placeId) {
    Keyboard.dismiss();
    this.setState({blinkItem: placeId});
    setTimeout(() => {
      this.setState({blinkItem: null});
    }, 3000);
    setTimeout(() => {
      var type = getSerachType();
      this.props.navigation.navigate('Recommend', {
        place_id: placeId,
        defaultOpen: type,
      });
    }, 500);
  }

  renderItem = ({item}) => {
    if (this.state.allCompany) {
      return (
        <Pressable
          style={({pressed}) => [
            pressed ? styles.listItemActive : styles.listItem,
          ]}
          onPress={() => this.gotoDetail(item.place_id)}>
          {({pressed}) => (
            <View>
              <Text
                style={
                  pressed
                    ? styles.itemHeaderActive
                    : this.state.blinkItem == item.place_id
                    ? styles.itemHeaderActive2
                    : styles.itemHeader
                }>
                {item.name}
              </Text>
              <Text
                note
                numberOfLines={2}
                style={pressed ? styles.itemTextActive : styles.itemText}>
                {item.formatted_address}
              </Text>
            </View>
          )}
        </Pressable>
      );
    } else {
      return (
        <Pressable
          style={({pressed}) => [
            pressed ? styles.listItemActive : styles.listItem,
          ]}
          onPress={() => this.gotoDetail(item.place_id)}>
          {({pressed}) => (
            <View>
              <Text
                style={
                  pressed
                    ? styles.itemHeaderActive
                    : this.state.blinkItem == item.place_id
                    ? styles.itemHeaderActive2
                    : styles.itemHeader
                }>
                {item.structured_formatting.main_text}
              </Text>
              <Text
                note
                numberOfLines={2}
                style={pressed ? styles.itemTextActive : styles.itemText}>
                {item.structured_formatting.secondary_text}
              </Text>
            </View>
          )}
        </Pressable>
      );
    }
  };

  getSubTitle() {}

  renderSuggestionList = () => {
    if (this.state.list != undefined) {
      return (
        <FlatList
          keyboardShouldPersistTaps={'always'}
          data={this.state.list.slice(0, 3)}
          renderItem={this.renderItem}
          keyExtractor={(item) => item.place_id}
          ItemSeparatorComponent={(props) => {
            return <View style={{height: 1, backgroundColor: '#CDCDCD'}} />;
          }}
        />
      );
    } else {
      return null;
    }
  };

  _renderSpinner() {
    if (this.props.isLoading) {
      return <Spinner color="green" />;
    } else {
      return this.renderSuggestionList();
    }
  }
  _back() {
    setTimeout(() => {
      this.props.navigation.goBack();
    }, 1000);
  }
  _goHome() {
    setTimeout(() => {
      this.props.navigation.navigate('Home');
    }, 1000);
  }

  render() {
    return (
      <Container>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <SubHeader text={this.state.subtitle} />
        <View style={styles.searchContainer}>
          <Item regular style={{height: 35, borderRadius: 5}}>
            <TextInput
              placeholder="Name of business"
              returnKeyType="done"
              autoFocus={true}
              blurOnSubmit={false}
              // ref={ref => this.ref = ref}
              style={styles.textInput}
              onChangeText={(text) => this.searchCompany(text)}
            />
          </Item>
        </View>
        <View style={{marginLeft: 8, marginRight: 8}}>
          {this._renderSpinner()}
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    flex: 1,
    color: Colors.deepPurple,
    textAlign: 'center',
    fontSize: FontSize.large,
    alignSelf: 'center',
    padding: 0,
  },
  searchContainer: {
    paddingLeft: 8,
    paddingRight: 8,
    marginTop: 10,
  },
  listItem: {
    backgroundColor: Colors.white,
    padding: 8,
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  listItemActive: {
    backgroundColor: Colors.primary,
    padding: 8,
    flex: 1,
    opacity: 0,
  },
  itemHeader: {
    flex: 1,
    fontSize: FontSize.heading,
    color: Colors.deepPurple,
    textAlign: 'left',
  },
  itemHeaderActive: {
    fontSize: FontSize.heading,
    color: Colors.white,
    textAlign: 'left',
  },
  itemHeaderActive2: {
    fontSize: FontSize.heading,
    color: '#585858',
    textAlign: 'left',
  },
  itemText: {
    fontSize: FontSize.medium,
    color: Colors.lessDarkGrey,
    textAlign: 'left',
    flex: 1,
  },
  itemTextActive: {
    fontSize: FontSize.medium,
    color: Colors.white,
    textAlign: 'left',
  },
});

const mapStateToProps = (state) => {
  console.log('State:');
  console.log(state);

  return {
    error: state.search.error,
    isLoading: state.search.isLoading,
    result: state.search.result,
    type: state.search.type,
    companyList: state.search.companyList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    search: (data) => {
      dispatch({
        type: Action.SEARCH,
        value: data,
      });
    },
    getCompany: () => {
      dispatch({
        type: Action.GET_COMP_LIST,
      });
    },
  };
};

// Exports
export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);
