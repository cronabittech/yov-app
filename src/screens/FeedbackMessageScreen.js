import moment from 'moment';
import {Container, Header, Text} from 'native-base';
import React, {PureComponent} from 'react';
import {
  FlatList,
  Pressable,
  StatusBar,
  StyleSheet,
  Alert,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {getWithLogin, postWithLogin} from '../api/api';
import {SubHeader, Button} from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';

export default class FeedbackMessageScreen extends PureComponent {
  flatList = null;

  state = {
    chat: [],
    company: undefined,
    message: '',
    uid: '',
    height: 45,
    focus: false,
    feedback: undefined,
    index: 0,
  };

  async doneThings(type) {
    await getWithLogin('user/state/' + 'message');
  }

  momentFormateDate(date) {
    if (date) {
      return moment(date + '', 'YYYY-MM-DD HH:mm:ss').format(
        'DD/MM/YYYY HH:mm',
      );
    } else {
      return moment().format('DD/MM/YYYY HH:mm');
    }
  }

  _endThisConverison() {
    Alert.alert('Alert', 'Do you want to end this conversion ?', [
      {
        text: 'No',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'Yes', onPress: () => this.endConverison(this.state.feedback.uid)},
    ]);
  }

  componentDidMount() {
    this.setState(
      {
        company: this.props.route.params.company,
        feedback: this.props.route.params.feedbacks,
        index: this.props.route.params.index,
      },
      () => {
        this.getMessage();

        if (this.state.feedback && this.state.feedback.is_complete == 0) {
          this.props.navigation.setOptions({
            headerRight: () => (
              <TouchableOpacity style={{marginRight: 5}}>
                <Button
                  type="header"
                  text="End"
                  onPress={() => this._endThisConverison()}
                />
              </TouchableOpacity>
            ),
          });
        }
      },
    );

    this.doneThings();
  }

  async endConverison(uid) {
    try {
      const response = await getWithLogin('fed/end/' + uid);
      this.setState({feedback: response.data});
    } catch (error) {}
  }

  async getMessage() {
    try {
      const response = await getWithLogin(
        'fed/msg/con/' + this.state.feedback.uid,
      );

      let data = {
        id: 2,
        is_sent: 1,
        created_at: this.state.feedback.created_at,
        msg: this.state.feedback.full_answer,
      };
      response.data.unshift(data);
      this.setState({chat: response.data});
      this.readMessage();
    } catch (e) {}
  }

  async sendMessage() {
    try {
      const response = await postWithLogin('fed/msg/usr/send', {
        com_id: this.state.company.id,
        msg: this.state.message,
        uid: this.state.feedback.uid,
      });
    } catch (e) {
      console.log(e.response);
    }
    this.setState({message: ''});
    this.getMessage();
    this.readMessage();
  }

  async readMessage() {
    try {
      const response = await getWithLogin(
        'fed/msg/usr/con/' + this.state.feedback.uid,
      );
    } catch (e) {
      console.log(e.response);
    }
  }

  renderItem = ({item}) => {
    if (item.is_sent == 1) {
      return (
        <View style={{marginBottom: 10}}>
          <View style={styles.sendContainer}>
            <Text style={styles.sendText}>{item.msg}</Text>
          </View>
          <Text
            style={{fontSize: 12, color: Colors.lightGrey, textAlign: 'right'}}>
            {this.momentFormateDate(item.created_at)}
          </Text>
        </View>
      );
    } else {
      return (
        <View style={{marginBottom: 10}}>
          <View style={styles.receivedContainer}>
            <Text style={styles.receivedText}>{item.msg}</Text>
          </View>
          <Text style={{fontSize: 12, color: Colors.lightGrey}}>
            {this.momentFormateDate(item.created_at)}
          </Text>
        </View>
      );
    }
  };

  render() {
    return (
      <Container>
        <SubHeader text="Feedback chat" />
        <Header
          style={{
            height: 77,
            backgroundColor: Colors.white,
            flexDirection: 'row',
            elevation: 0,
          }}>
          <View>
            <Text
              style={{
                marginTop: 10,
                textAlign: 'center',
                color: '#585858',
                fontSize: FontSize.heading,
                fontWeight: 'bold',
              }}>
              {this.state.company ? this.state.company.name : null}
            </Text>
            <Text
              style={{
                textAlign: 'center',
                color: Colors.lightGrey,
                fontSize: FontSize.medium,
                fontWeight: 'bold',
              }}>
              {this.state.company ? this.state.company.formatted_address : null}
            </Text>
            <View
              style={{
                marginTop: 5,
              }}>
              <Text style={{textAlign: 'center', color: Colors.dark}}>
                {this.state.feedback
                  ? this.state.feedback.feed_short == 'comment'
                    ? 'Review submitted'
                    : this.state.feedback.question
                  : null}
              </Text>
            </View>
          </View>
        </Header>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <View
          style={{
            flex: 1,
            padding: 10,
            paddingBottom: this.state.index == 0 ? 70 : 0,
          }}>
          <FlatList
            contentContainerStyle={{marginBottom: 100}}
            keyboardShouldPersistTaps="always"
            data={this.state.chat}
            ref={(ref) => (this.flatList = ref)}
            keyExtractor={(item) => item.id}
            maxToRenderPerBatch={1000000000}
            onContentSizeChange={() =>
              this.flatList.scrollToEnd({animated: true})
            }
            onLayout={() => this.flatList.scrollToEnd({animated: true})}
            renderItem={this.renderItem}
          />
        </View>
        {this.state.feedback && this.state.feedback.is_complete == 0 ? (
          <View
            style={{
              backgroundColor: Colors.white,
              flex: 1,
              position: 'absolute',
              alignSelf: 'baseline',
              flexDirection: 'row',
              bottom: 0,
              padding: 10,
              justifyContent: 'center',
            }}>
            <TextInput
              placeholder="Start your reply here"
              multiline={true}
              returnKeyType="done"
              blurOnSubmit={false}
              onFocus={() => this.setState({focus: true})}
              onBlur={() => this.setState({focus: false})}
              value={this.state.message}
              onChangeText={(text) => this.setState({message: text})}
              style={[
                styles.textInput,
                {
                  borderColor: this.state.focus
                    ? Colors.darkBlue
                    : Colors.lightGrey,
                },
              ]}
            />
            <Pressable
              style={styles.fullButtonContainer}
              onPress={() => this.sendMessage()}>
              <Icon
                style={{alignSelf: 'center'}}
                name="arrow-up"
                size={25}
                color={Colors.primary}></Icon>
            </Pressable>
          </View>
        ) : (
          <Text
            style={{textAlign: 'center', marginBottom: 10, color: Colors.red}}>
            End Of Conversion
          </Text>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    flex: 1,
    color: Colors.deepPurple,
    textAlign: 'left',
    fontSize: FontSize.large,
    alignSelf: 'flex-end',
    padding: 0,
    paddingLeft: 5,
    minHeight: 45,
    borderRadius: 5,
    flex: 4,
    marginRight: 10,
    borderWidth: 1,
    borderColor: Colors.lightGrey,
  },
  receivedContainer: {
    borderRadius: 20,
    backgroundColor: Colors.primary,
    borderTopLeftRadius: 0,
    padding: 10,
    marginBottom: 5,
    minWidth: 10,
    maxWidth: 250,
    alignSelf: 'flex-start',
    elevation: 2,
  },
  receivedText: {
    textAlign: 'left',
    color: Colors.white,
  },
  sendContainer: {
    borderRadius: 20,
    backgroundColor: Colors.lightGrey2,
    borderTopRightRadius: 0,
    padding: 10,
    marginBottom: 5,
    minWidth: 10,
    maxWidth: 250,
    alignSelf: 'flex-end',
    elevation: 2,
  },
  sendText: {
    textAlign: 'left',
    color: Colors.dark,
  },
  fullButtonContainer: {
    flex: 1,
    borderRadius: 5,
    backgroundColor: '#F5F5F5',
    elevation: 2,
    height: 45,
    justifyContent: 'center',
    maxHeight: 45,
    alignSelf: 'flex-end',
  },
  fullButtonContainerActive: {
    flex: 1,
    borderRadius: 5,
    backgroundColor: Colors.primary,
    elevation: 2,
    height: 45,
    justifyContent: 'center',
    opacity: 0,
    maxHeight: 45,
  },
});
