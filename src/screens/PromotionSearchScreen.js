import {Container, Spinner, Content, Card, Badge} from 'native-base';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  View,
  Text,
  Pressable,
  StyleSheet,
  TextInput,
} from 'react-native';
import {postWithLogin} from '../api/api';
import {SubHeader} from '../component';
import Colors from '../config/colors';
import moment from 'moment';

function PromotionSearchScreen(props) {
  const [name, setName] = useState('');
  const [promotions, setPromotions] = useState('');
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    getPromotion();
  }, [name]);

  const getPromotion = async () => {
    try {
      setLoading(true);
      const response = await postWithLogin('promption/search', {name});
      console.log(response);
      setPromotions(response.data);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  const renderItem = ({item}) => {
    return (
      <Card style={{margin: 10, marginTop: 5, marginBottom: 5}}>
        <Pressable onPress={() =>    props.navigation.navigate('PromotionDetails',{id:item.prom_id})}>
          <View style={{padding: 10}}>
            <Text numberOfLines={1} style={{fontWeight: 'bold'}}>
              {item.name}
            </Text>
            <Text numberOfLines={2} style={{color: Colors.lightGrey}}>
              {item.formatted_address}
            </Text>
            <Text
              numberOfLines={2}
              style={{marginTop: 5, color: Colors.primary}}>
              {item.benifits}
            </Text>
          </View>
        </Pressable>
      </Card>
    );
  };

  return (
    <Container>
      <SubHeader text="Search promotion" />
      <View style={{padding: 10}}>
        <TextInput
          placeholder="Search company"
          returnKeyType="done"
          style={[style.textInput]}
          autoFocus={true}
          autoCapitalize="none"
          onChangeText={(text) => setName(text)}
        />
        <View style={{marginTop: 10}}>
          {isLoading ? (
            <Spinner color="green" style={{alignSelf: 'center'}} />
          ) : (
            <FlatList
              contentContainerStyle={{marginBottom: 100}}
              keyboardShouldPersistTaps="always"
              data={promotions}
              keyExtractor={(item) => item.prom_id}
              renderItem={renderItem}
            />
          )}
        </View>
      </View>
    </Container>
  );
}

export default PromotionSearchScreen;

const style = StyleSheet.create({
  textInput: {
    fontSize: 17,
    borderWidth: 1.5,
    textAlign: 'center',
    borderRadius: 5,
    color: Colors.primary,
    borderColor: Colors.lightGrey,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
  },
});
