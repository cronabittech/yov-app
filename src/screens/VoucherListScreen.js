import {Container, Spinner, Content, Card, Badge} from 'native-base';
import React, {useEffect, useState} from 'react';
import {FlatList, View, Text, Pressable} from 'react-native';
import {getWithLogin} from '../api/api';
import {SubHeader} from '../component';
import Colors from '../config/colors';
import moment from 'moment';

function VoucherListScreen(props) {
  const [vocuhers, setVouchers] = useState('');
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    props.navigation.addListener('focus', () => {
      getVocuher();
    });
  }, []);

  const getVocuher = async () => {
    try {
      setLoading(true);
      const response = await getWithLogin('user/vocuhers');
      console.log(response);
      setVouchers(response.data);
      setLoading(false);
    } catch (e) {
      console.log('eoor', e.response);
    }
  };

  const isDateBiggerThanToday = (firstdate) => {
    var momentStartDate = moment(firstdate, 'YYYY-MM-DD');
    var momentEndDate = moment();
    return momentEndDate.diff(momentStartDate, 'days') >= 1 ? true : false;
  };

  const momentFormateDate = (date) => {
    if (date) {
      return moment(date + '', 'YYYY-MM-DD').format('DD/MM/YYYY');
    } else {
      return moment().format('DD/MM/YYYY');
    }
  };

  const itemClick = (item) => {
    if (!isDateBiggerThanToday(item.voucher_expire_date) && item.is_used == 0) {
      props.navigation.navigate('RedeemVoucher', {item :item});
    }
  };

  const renderItem = ({item}) => {
    return (
      <Card style={{margin: 10, marginTop: 5, marginBottom: 5}}>
        <Pressable onPress={() => itemClick(item)}>
          <View style={{padding: 10}}>
            <Text numberOfLines={1} style={{fontWeight: 'bold'}}>
              {item.name}
            </Text>
            <Text numberOfLines={2} style={{color: Colors.lightGrey}}>
              {item.formatted_address}
            </Text>
            <Text style={{marginTop: 5, color: Colors.primary, fontSize: 16}}>
              {item.benifits}
            </Text>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{marginTop: 5, color: Colors.red, fontSize: 12}}>
                Expiring at: {momentFormateDate(item.voucher_expire_date)}
              </Text>
              {item.is_used == 1 && (
                <Badge success>
                  <Text style={{fontWeight: 'bold', color: Colors.white}}>
                    Used
                  </Text>
                </Badge>
              )}
              {item.is_used == 0 &&
                isDateBiggerThanToday(item.voucher_expire_date) && (
                  <Badge danger>
                    <Text style={{fontWeight: 'bold', color: Colors.white}}>
                      Expired
                    </Text>
                  </Badge>
                )}
            </View>
          </View>
        </Pressable>
      </Card>
    );
  };

  return (
    <Container>
      <SubHeader text="Vocuher list" />
      {isLoading ? (
        <Spinner color="green" style={{alignSelf: 'center'}} />
      ) : (
        <FlatList
          contentContainerStyle={{marginBottom: 100}}
          keyboardShouldPersistTaps="always"
          data={vocuhers}
          keyExtractor={(item) => item.id}
          renderItem={renderItem}
        />
      )}
    </Container>
  );
}

export default VoucherListScreen;
