import moment from 'moment';
import {Container, Content} from 'native-base';
import React, {PureComponent} from 'react';
import {FlatList, StyleSheet, View, Text} from 'react-native';
import {getWithLogin} from '../../api/api';
import Colors from '../../config/colors';
import {Button, LogHistoryItem, SubHeader} from '../../component';

export default class Recommendtion extends PureComponent {
  componentDidMount() {}

  async postRecommend() {
    try {
      const response = await getWithLogin('do-recomend/' + this.props.comp_id);
      this.props.done();
    } catch (e) {
      console.log(e.response);
    }
  }

  render() {
    return (
      <View style={{marginTop: 10}}>
        <Text style={styles.header}> Recommend Business</Text>
        <Content padder>
          <Text style={{textAlign: 'center', fontSize: 15, marginTop: 25}}>
            Would you like to recommend
          </Text>
          <Text style={{textAlign: 'center',fontSize: 15,  color: Colors.primary}}>
            {this.props.name}
          </Text>
          <Text
            numberOfLines={2}
            style={{textAlign: 'center',fontSize: 15,  color: Colors.darkGrey}}>
            {this.props.address}
          </Text>
          <Text style={{textAlign: 'center', fontSize: 15}}>
            to other people using this app?
          </Text>
          <View
            style={{
              marginTop: 20,
              justifyContent: 'space-around',
              flexDirection: 'row',
            }}>
            <Button
              type="border2"
              text="Yes"
              onPress={() => this.postRecommend()}></Button>
            <Button
              type="border2"
              text="No"
              onPress={() => this.props.done()}></Button>
          </View>
        </Content>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.lightGrey2,
    padding: 5,
    textAlign: 'center',
    color: Colors.primary,
    fontSize: 16,
  },
});
