import { Content } from 'native-base';
import React, { PureComponent } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { postWithLogin } from '../../api/api';
import { Button } from '../../component';
import Colors from '../../config/colors';

export default class Favorite extends PureComponent {
  componentDidMount() {}

  async postFavorite() {
    try {
      var respose = await postWithLogin('add/fav', {
        com_id: this.props.comp_id,
      });
      this.props.done();
    } catch (e) {
      console.log(e.response);
    }
  }

  render() {
    return (
      <View style={{marginTop: 10}}>
        <Text style={styles.header}>Favourite Business</Text>
        <Content padder>
          <Text style={{textAlign: 'center', fontSize: 17, marginTop: 25}}>
            Will you add
          </Text>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 15,
              marginTop: 5,
              color: Colors.primary,
            }}>
            {this.props.name}
          </Text>
          <Text
            numberOfLines={2}
            style={{textAlign: 'center', fontSize: 15, color: Colors.darkGrey}}>
            {this.props.address}
          </Text>
          <Text style={{textAlign: 'center', fontSize: 17, marginTop: 5}}>
            to your favourites?
          </Text>
          <View
            style={{
              marginTop: 20,
              justifyContent: 'space-around',
              flexDirection: 'row',
            }}>
            <Button
              type="border2"
              text="Yes"
              onPress={() => this.postFavorite()}></Button>
            <Button
              type="border2"
              text="No"
              onPress={() => this.props.done()}></Button>
          </View>
        </Content>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.lightGrey2,
    padding: 5,
    textAlign: 'center',
    color: Colors.primary,
    fontSize: 16,
  },
});
