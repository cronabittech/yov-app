import { Content,  } from 'native-base';
import React, { PureComponent } from 'react';
import { StyleSheet, Text, View ,TextInput} from 'react-native';
import { postWithLogin } from '../../api/api';
import { Button } from '../../component';
import  Colors from '../../config/colors';
import FontSize from '../../config/fontSize';

export default class Feedback extends PureComponent {
  componentDidMount() {}
  state = {
    step: 0,
    com1: '',
    com2: '',
    com3: '',
    com4: '',
    optionAns: '',
    optionAns2: '',
    optionAns3: '',
    defaultFeedback: 'I thought your products / service was',
    comment: '',
    time: 0,
    commentFocus: false,
    fullanswer: undefined,
  };

  async postRecommend() {
    try {
      const response = await getWithLogin('do-recomend/' + this.props.comp_id);
      this.props.done();
    } catch (e) {
      console.log(e.response);
    }
  }

  
  step1Ans(ans) {
    const question = this.props.promotion.question;
    this.setState({optionAns: ans});
    console.log(question);
    this.setState(
        {
          fullanswer: {
            question: 'What do you think about our '+question+'?',
            feed_short: this.state.optionAns,
            full_answer: ans,
            com_id: this.props.comp_id,
            prom_id: this.props.prom_id,
          },
        },
        () => this.sendFeedback(),
      );
  }

  doStep2() {
    this.setState({step: 2});
    const question = this.props.promotion.question;

    this.setState(
      {
        fullanswer: {
          question:
            'What was ' +
            this.state.optionAns +
            ' about the '+question+'?',
          feed_short: this.state.optionAns,
          full_answer: this.state.optionAns2,
          com_id: this.props.comp_id,
          prom_id: this.props.prom_id,
        },
      },
      () => this.sendFeedback(),
    );
  }


  doComment() {
    const question = this.props.promotion.question;
    this.setState(
      {
        fullanswer: {
          question: 'What do you think about our '+question+'?',
          feed_short: 'comment',
          full_answer: this.state.comment,
          com_id: this.props.comp_id,
          prom_id: this.props.prom_id,
        },
      },
      () => this.sendFeedback(),
    );
  }

  async sendFeedback() {
    try{
      const response = await postWithLogin('promotion/fed/usr/send', this.state.fullanswer);
      if (this.state.step == 0) {
        this.setState({step: 1});
      } else {
          this.props.done();
      }
    }catch(e){
      console.log(e.response);
    }
    
  }

  
  _renderQuestion() {
    if (this.state.step == 0) {
      return (
        <View style={{justifyContent: 'center', marginTop: 10}}>
          <Text
            style={{
              textAlign: 'center',
              marginTop: 10,
              fontSize: FontSize.large,
            }}>
            What do you think about our <Text style={{color:Colors.primary}}>{this.props.promotion.question}</Text>?
          </Text>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 10,
              justifyContent: 'space-between',
            }}>
            <Button
              type="border"
              text="Fantastic"
              onPress={() =>
                setTimeout(() => {
                  this.step1Ans('fantastic');
                }, 50)
              }
            />
            <Button
              type="border"
              text="Good"
              onPress={() =>
                setTimeout(() => {
                  this.step1Ans('good');
                }, 50)
              }
            />
            <Button
              type="border"
              text="Poor"
              onPress={() =>
                setTimeout(() => {
                  this.step1Ans('poor');
                }, 50)
              }
            />
            <Button
              type="border"
              text="Not to my liking"
              onPress={() =>
                setTimeout(() => {
                  this.step1Ans('not to my liking');
                }, 50)
              }
            />
          </View>
          <View style={{marginTop: 10}}>
            <Button
              type="border2"
              text="Enter your comments here"
              onPress={() =>
                setTimeout(() => {
                  this.setState({step: 3});
                }, 50)
              }
            />
          </View>
        </View>
      );
    } else if (this.state.step == 1) {
      return (
        <View style={{justifyContent: 'center', flex: 1, marginTop: 10}}>
          <Text
            style={{
              textAlign: 'center',
              marginTop: 20,
              fontSize: FontSize.large,
            }}>
            <Text>What was </Text>
            <Text style={{color: Colors.primary}}>{this.state.optionAns}</Text>
            <Text> about the <Text style={{color:Colors.primary}}>{this.props.promotion.question}</Text>?</Text>
          </Text>
          <View style={{marginTop: 10, justifyContent: 'center'}}>
            <TextInput
              placeholder="Comments"
              returnKeyType="done"
              multiline={true}
              autoFocus={true}
              autoCapitalize="none"
              style={[
                styles.textInput,
                {
                  borderColor: this.state.commentFocus
                    ? Colors.deepPurple
                    : Colors.lightGrey,
                },
              ]}
              onFocus={() => this.setState({commentFocus: true})}
              onBlur={() => this.setState({commentFocus: false})}
              defaultValue={this.state.optionAns2}
              onChangeText={(text) => this.setState({optionAns2: text})}
            />

            {this.state.optionAns2.length > 0 ? (
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 10,
                  justifyContent: 'space-around',
                }}>
                <Button
                  type="border2"
                  text="Delete"
                  onPress={() => this.setState({optionAns2: ''})}
                />
                <Button
                  type="border2"
                  text="Send"
                  onPress={() =>
                    setTimeout(() => {
                      this.doStep2();
                    }, 50)
                  }
                />
              </View>
            ) : (
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 10,
                  justifyContent: 'space-around',
                }}>
                <Button
                  type="border2"
                  text="No further comment"
                  onPress={() =>
                    setTimeout(() => {
                      this.props.done();
                    }, 50)
                  }
                />
              </View>
            )}
          </View>
        </View>
      );
    } else if (this.state.step == 3) {
      return (
        <View style={{justifyContent: 'center', flex: 1, marginTop: 10}}>
          <View style={{marginTop: 10, justifyContent: 'center'}}>
            <TextInput
              placeholder="Enter your comments here"
              returnKeyType="done"
              style={[
                styles.textInput,
                {
                  borderColor: this.state.commentFocus
                    ? Colors.deepPurple
                    : Colors.lightGrey,
                },
              ]}
              multiline={true}
              autoFocus={true}
              autoCapitalize="none"
              onFocus={() => this.setState({commentFocus: true})}
              onBlur={() => this.setState({commentFocus: false})}
              defaultValue={this.state.comment}
              onChangeText={(text) => this.setState({comment: text})}
            />
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
                justifyContent: 'space-evenly',
              }}>
              {this.state.comment.length > 0 ? (
                <Button
                  type="border2"
                  text="Delete"
                  onPress={() => this.setState({comment: ''})}
                />
              ) : null}
              {this.state.comment.length > 0 ? (
                <Button
                  type="border2"
                  text="Send"
                  onPress={() =>
                    setTimeout(() => {
                      this.doComment();
                    }, 50)
                  }
                />
              ) : null}
              <Button
                type="border2"
                text="Close"
                onPress={() =>setTimeout(() => {
                  this.setState({step: 0});
                }, 50)}
              />
            </View>
          </View>
        </View>
      );
    }
  }

  render() {
    return (
      <View style={{marginTop: 10}}>
        <Text style={styles.header}>Submit a Feedback</Text>
        <Content padder>{this._renderQuestion()}</Content>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.lightGrey2,
    padding:5,
    textAlign: 'center',
    color:Colors.primary,
    fontSize:16
  },
  textInput: {
    fontSize: 17,
    borderWidth: 1.5,
    borderRadius: 5,
    color: Colors.primary,
    borderColor: Colors.lightGrey2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 12,
    paddingBottom: 12,
    maxHeight: 110,
  },
});
