import moment from 'moment';
import {Container, Content} from 'native-base';
import React, {PureComponent} from 'react';
import {FlatList, StyleSheet, View, Text} from 'react-native';
import {postWithLogin} from '../../api/api';
import Colors from '../../config/colors';
import {Button, LogHistoryItem, SubHeader} from '../../component';

export default class ExistingUser extends PureComponent {
  componentDidMount() {}

  async postExistinUser(result) {
    try {
      let data = {
        comp_id: this.props.comp_id,
        is_exsiting: result,
      };
      const response = await postWithLogin('user/exisiting', data);
      this.props.done();
    } catch (e) {
        console.log(e.response);
    }
  }

  render() {
    return (
      <View style={{marginTop: 10}}>
        <Text style={styles.header}> Existing Customer</Text>
        <Content padder>
          <Text style={{textAlign: 'center', fontSize: 19, marginTop: 25}}>
            Are you exsiting customer?
          </Text>
          <View
            style={{
              marginTop: 20,
              justifyContent: 'space-around',
              flexDirection: 'row',
            }}>
            <Button
              type="border2"
              text="Yes, I'm"
              onPress={() => this.postExistinUser(1)}></Button>
            <Button
              type="border2"
              text="No"
              onPress={() => this.postExistinUser(0)}></Button>
          </View>
        </Content>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.lightGrey2,
    padding:5,
    textAlign: 'center',
    color:Colors.primary,
    fontSize:16
  },
});
