import {Content} from 'native-base';
import React, {PureComponent} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {postWithLogin} from '../../api/api';
import {Blinkview, StarRate} from '../../component';
import Colors from '../../config/colors';
import FontSize from '../../config/fontSize';

export default class StarRating extends PureComponent {
  state = {
    starCount1: 0,
    starCount2: 0,
    starCount3: 0,
  };
  componentDidMount() {}

  async addStarRating() {
    if (
      this.state.starCount1 > 0 ||
      this.state.starCount2 > 0 ||
      this.state.starCount3 > 0
    ) {
      var data = {
        com_id: this.props.comp_id,
        customer_service: this.state.starCount1,
        quality_of_product: this.state.starCount2,
        value_for_money: this.state.starCount3,
      };
      var response = await postWithLogin('star-rating/add', data);
      this.props.done();
    }
  }

  manageRating(order, rating) {
    switch (order) {
      case 1:
        {
          if (rating > this.state.starCount1) {
            this.setState({starCount1: rating});
          } else {
            var newRating = rating - 1;
            if (newRating <= 0) {
              this.setState({starCount1: 0});
            } else {
              this.setState({starCount1: newRating});
            }
          }
        }
        break;
      case 2:
        {
          if (rating > this.state.starCount2) {
            this.setState({starCount2: rating});
          } else {
            var newRating = rating - 1;
            if (newRating <= 0) {
              this.setState({starCount2: 0});
            } else {
              this.setState({starCount2: newRating});
            }
          }
        }
        break;
      case 3:
        {
          if (rating > this.state.starCount3) {
            this.setState({starCount3: rating});
          } else {
            var newRating = rating - 1;
            if (newRating <= 0) {
              this.setState({starCount3: 0});
            } else {
              this.setState({starCount3: newRating});
            }
          }
        }
        break;
    }
  }

  render() {
    return (
      <View style={{marginTop: 10}}>
        <Text style={styles.header}> Star rate a Business</Text>
        <Content padder>
          <View>
            <Text style={styles.starLabel}> Customer Service</Text>
            <StarRate
              disabled={false}
              containerStyle={{justifyContent: 'center', padding: 10}}
              buttonStyle={{marginLeft: 15}}
              rating={this.state.starCount1}
              order={1}
              selectedStar={this.manageRating.bind(this)}
            />
            <Text style={styles.starLabel}> Quality Of Product / Service</Text>
            <StarRate
              disabled={false}
              containerStyle={{justifyContent: 'center', padding: 10}}
              buttonStyle={{marginLeft: 15}}
              rating={this.state.starCount2}
              order={2}
              selectedStar={this.manageRating.bind(this)}
            />
            <Text style={styles.starLabel}>Value For Money</Text>
            <StarRate
              disabled={false}
              containerStyle={{justifyContent: 'center', padding: 10}}
              buttonStyle={{marginLeft: 15}}
              rating={this.state.starCount3}
              order={3}
              selectedStar={this.manageRating.bind(this)}
            />
          </View>
          {this.state.starCount1 > 0 ||
          this.state.starCount2 > 0 ||
          this.state.starCount3 > 0 ? (
            <View style={{marginTop: 20, justifyContent: 'center'}}>
              <Blinkview
                twoArrow={true}
                text="Done"
                onPress={() => this.addStarRating()}
              />
            </View>
          ) : null}
        </Content>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.lightGrey2,
    padding: 5,
    textAlign: 'center',
    color: Colors.primary,
    fontSize: 16,
  },
  starRatingContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  starLabel: {
    textAlign: 'center',
    fontSize: FontSize.medium,
    fontSize: 17,
    marginTop: 20,
    fontWeight: 'bold',
  },
  starLabelActive: {
    color: Colors.white,
    textAlign: 'right',
    fontSize: FontSize.medium,
  },
});
