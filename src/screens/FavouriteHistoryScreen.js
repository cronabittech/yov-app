import {Container, Content} from 'native-base';
import React, {PureComponent} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {getWithLogin} from '../api/api';
import {Button, FavouriteItem, SubHeader} from '../component';
import {callNumber, doMail,getType} from '../config/util';

const VibrateStart = () => {};

const VibrateStop = () => {};

export default class FavouriteHistoryScreen extends PureComponent {
  state = {
    isLoading: false,
    favList: undefined,
  };
  componentDidMount() {
    this.props.navigation.setOptions({
      headerRight: () => (
        <Button
          type="header"
          text="Search"
          onPress={() => this.props.navigation.navigate('SearchFromFavorite')}
        />
      ),
    });
    this.getFav();
    this.props.navigation.addListener('focus', () => {
      this.getFav();
    });
  }
  gotoCompany(item) {
    this.props.navigation.navigate('Recommend', {
      place_id: item.company.place_id,
      defaultOpen: 'view_favourite',
    });
  }
  checkName(item) {
    var name = '';
    if (item.name_title != '' && item.name_title != null) {
      name = name + ' ' + item.name_title;
    }
    if (item.first_name != '' && item.first_name != null) {
      name = name + ' ' + item.first_name;
    }
    if (item.surname != '' && item.surname != null) {
      name = name + ' ' + item.surname;
    }
    if (name != '') {
      return name;
    } else {
      return 'Contact name';
    }
  }
  _renderItem = ({item, index}) => {
    return (
      <FavouriteItem
        onHeaderClick={() => this.gotoCompany(item)}
        onStarClick={() => this.gotoCompany(item)}
        onRecoClick={() => this.gotoCompany(item)}
        icon={item.company.icon}
        cname={item.company.name}
        name={this.checkName(item)}
        title={item.job_title ? item.job_title : 'Job title'}
        post={getType(item.company.category)}
        phone={() => callNumber(item.company.formatted_phone_number)}
        mail={() => doMail(item.company.email)}
      />
    );
  };

  async getFav() {
    var response = await getWithLogin('user/favorite');
    console.log(response.data);
    this.setState({favList: response.data});
  }
  render() {
    return (
      <Container>
        <SubHeader text="My favourites" />
        <Content>
          <FlatList
            keyboardShouldPersistTaps={'handled'}
            data={this.state.favList}
            renderItem={this._renderItem}
            keyExtractor={(item) => item.id}
            ItemSeparatorComponent={(props) => {
              return (
                <View
                  style={{
                    height: 1,
                    backgroundColor: '#CDCDCD',
                    marginLeft: 100,
                    marginRight: 10,
                  }}
                />
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({});
