import {
  Button,
  Container,
  Content,
  Input,
  Item,
  Spinner,
  Text,
} from 'native-base';
import {postWithLogin} from '../api/api';
import {SubHeader} from '../component';
import Colors from '../config/colors';
import moment from 'moment';
import React, {useEffect, useState} from 'react';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';

function ChangePasswordScreen(props) {
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmNewPassword, setConfirmNewPassword] = useState('');
  const [isLoading, setLoading] = useState(false);

  const _renderSubmitButton = () => {
    if (isLoading) {
      return <Spinner color="green" />;
    } else {
      return (
        <Button
          block
          rounded
          success
          style={{marginTop: 15}}
          onPress={() => submit()}>
          <Text style={{fontWeight: 'bold', textTransform: 'capitalize'}}>
            submit
          </Text>
        </Button>
      );
    }
  };
  const submit = () => {
    if (oldPassword == '') {
      _showMessage('Enter old password');
      return;
    }
    if (newPassword == '') {
      _showMessage('Enter new password');
      return;
    }
    if (confirmNewPassword == '') {
      _showMessage('Enter confirm new password');
      return;
    }
    if (newPassword !== confirmNewPassword) {
      _showMessage("Confirm new password doesn't match");
      return;
    }

    let data = {
      confirmPassword: confirmNewPassword,
      old_password: oldPassword,
      password: newPassword,
    };
    changePassword(data);
  };

  const _showMessage = (msg) => {
    Snackbar.show({
      text: msg,
      duration: Snackbar.LENGTH_INDEFINITE,
      action: {
        text: 'close',
        textColor: 'red',
        onPress: () => {
          Snackbar.dismiss();
        },
      },
    });
  };

  const changePassword = async (data) => {
    setLoading(true);
    try {
      const response = await postWithLogin('change-password', data);
      setLoading(false);

      _showMessage('Password changed');
      //   _logOut();
    } catch (err) {
      _showMessage(err.response.data.message[0]);
      setLoading(false);
    }
  };

  const _logOut = () => {
    AsyncStorage.setItem('username', '');
    AsyncStorage.setItem('password', '');
    props.navigation.reset({routes: [{name: 'splash'}]});
  };

  return (
    <Container>
      <SubHeader text="Change Password" />
      <Content padder>
        <Item rounded style={{marginTop: 10}}>
          <Input
            placeholder="Old password"
            multiline={false}
            placeholderTextColor="#cdcdcd"
            secureTextEntry={true}
            onChangeText={(text) => setOldPassword(text)}
          />
        </Item>
        <Item rounded style={{marginTop: 10}}>
          <Input
            placeholder="New password"
            multiline={false}
            placeholderTextColor="#cdcdcd"
            secureTextEntry={true}
            onChangeText={(text) => setNewPassword(text)}
          />
        </Item>
        <Item rounded style={{marginTop: 10}}>
          <Input
            placeholder="Confirm new password"
            multiline={false}
            placeholderTextColor="#cdcdcd"
            secureTextEntry={true}
            onChangeText={(text) => setConfirmNewPassword(text)}
          />
        </Item>
        {_renderSubmitButton()}
      </Content>
    </Container>
  );
}

export default ChangePasswordScreen;