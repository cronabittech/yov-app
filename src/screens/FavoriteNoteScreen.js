/*
Bottom Favorite Note screen 
:-By kishan hadiyal
*/
import { Container, Content, Footer, Text, Header } from 'native-base';
import React, { PureComponent } from "react";
import { Pressable, StyleSheet, TextInput, View, StatusBar, Dimensions } from 'react-native';
import { postWithLogin, getWithLogin } from "../api/api";
import {
    Button,
    ModelLayout
} from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
const windowHeight = Dimensions.get('window').height;


const VibrateStart = () => {
}

const VibrateStop = () => {
}

export default class FavoriteNoteScreen extends PureComponent {

    state = {
        title: 'Add favourite notes',
        value: '',
        height: 40,
        isEditMode: 1,
        addFocus: false,
        editFocus: false,
        removeNoteAlert: false,
    }

    updateSize = (height) => {
        this.setState({
            height
        });
    }

    componentDidMount() {
        if (this.props.favData) {
            if (this.props.favData && this.props.favData.notes != "" && this.props.favData.notes != null) {
                this.setState({ value: this.props.favData.notes, isEditMode: 0, title: 'My favourite notes', });
            }
        }
    }

    onAddFocus() {
        this.setState({ addFocus: true });
    }

    onAddBlur() {
        this.setState({ addFocus: false, title: this.state.value ? "Adding favuorite notes" : "Add favuorite notes" });
    }

    onEditFocus() {
        this.setState({ editFocus: true });
    }

    onEditBlur() {
        this.setState({ editFocus: false, title: this.state.value ? "Editing favuorite notes" : "Edit favuorite notes" });
    }

    _removeFavAlert() {
        return (
            <ModelLayout
                centeredViewStyle={{ marginTop: 35 }}
                visible={this.state.removeNoteAlert}>
                <Text style={{ textAlign: 'center' }}>Are you sure you want to delete your notes?</Text>
                <View style={{ width: 150, flexDirection: 'row', marginTop: 15, justifyContent: 'space-between' }}>
                    <Button type="textButton" text="Yes" onPress={() => this.deleteData()} />
                    <Button type="textButton" text="No" onPress={() => this.setState({ removeNoteAlert: false })} />
                </View>
            </ModelLayout>
        );
    }

    async addData() {
        var data = {
            name_title: this.state.name_title,
            first_name: this.state.first_name,
            surname: this.state.surname,
            job_title: this.state.job_title,
            landline: this.state.landline,
            mobile: this.state.mobile,
            email: this.state.email,
            notes: this.state.value,
            type: this.state.isEditMode == 2 ? "edit" : "add"
        }
        try {
            var response = await postWithLogin('update/fav/' + this.props.company.id, data);
            this.setState({ isEditMode: false, title: 'My favourite notes' });
            this.props.updateFavData(response.data);
        } catch (error) {
            console.log(error.response.data);
        }

    }


    async deleteData() {
        var data = {
            name_title: this.state.name_title,
            first_name: this.state.first_name,
            surname: this.state.surname,
            job_title: this.state.job_title,
            landline: this.state.landline,
            mobile: this.state.mobile,
            email: this.state.email,
            notes: "",
            type: "delete"
        }
        try {
            var response = await postWithLogin('update/fav/' + this.props.company.id, data);
            this.setState({ isEditMode: true, title: 'Add favourite notes', value: "" });
            this.props.updateFavData(response.data);
            this.setState({ removeNoteAlert: false });
        } catch (error) {
            console.log(error.response.data);
        }

    }

    _renderContent() {
        if (this.state.isEditMode == 1) {
            return (
                <View>
                    <View>
                        <TextInput
                            multiline={true}
                            autoFocus={true}
                            onFocus={() => this.onAddFocus()}
                            onBlur={() => this.onAddBlur()}
                            style={[styles.inputStyle, { borderColor: this.state.addFocus ? Colors.deepPurple : Colors.lightGrey }]}
                            placeholder="Enter your notes here"
                            onChangeText={(value) => this.setState({ value, title: 'Adding favuorite notes?' })}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, justifyContent: this.state.value.length > 0 ? 'space-between' : 'space-around' }}>
                        {
                            this.state.value.length > 0 ?
                                <Pressable style={({ pressed }) => [
                                    pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 100 }
                                ]} onPress={() => this.addData()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                                    <Text style={{
                                        color: Colors.deepPurple, textAlign: 'center',
                                        textTransform: 'capitalize', fontSize: FontSize.heading
                                    }}>Done</Text>
                                </Pressable>
                                :
                                null
                        }
                        <Pressable style={({ pressed }) => [
                            pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 100 }
                        ]} onPress={() => this.props.close()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                            <Text style={{
                                color: Colors.deepPurple, textAlign: 'center',
                                textTransform: 'capitalize', fontSize: FontSize.heading
                            }}>Close</Text>
                        </Pressable>
                    </View>
                </View>
            );
        } else if (this.state.isEditMode == 2) {
            return (
                <View>
                    <View>
                        <TextInput
                            multiline={true}
                            value={this.state.value}
                            autoFocus={true}
                            onFocus={() => this.onEditFocus()}
                            onBlur={() => this.onEditBlur()}
                            style={[styles.inputStyle, { borderColor: this.state.editFocus ? Colors.deepPurple : Colors.lightGrey }]}
                            placeholder="Enter your notes here"
                            onChangeText={(value) => this.setState({ value, title: 'Editing favuorite notes?' })}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, justifyContent: this.state.value.length > 0 ? 'space-between' : 'space-around' }}>
                        {
                            this.state.value.length > 0 ?
                                <Pressable style={({ pressed }) => [
                                    pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 100 }
                                ]} onPress={() => this.addData()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                                    <Text style={{
                                        color: Colors.deepPurple, textAlign: 'center',
                                        textTransform: 'capitalize', fontSize: FontSize.heading
                                    }}>Done</Text>
                                </Pressable>
                                :
                                null
                        }
                        <Pressable style={({ pressed }) => [
                            pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 100 }
                        ]} onPress={() => this.props.close()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                            <Text style={{
                                color: Colors.deepPurple, textAlign: 'center',
                                textTransform: 'capitalize', fontSize: FontSize.heading
                            }}>Close</Text>
                        </Pressable>
                    </View>
                </View>
            );
        } else {
            return (
                <View  >
                    <Text onPress={() => this.setState({ isEditMode: 2, title: 'Edit favuorite notes' })} style={{ height: windowHeight, color: Colors.dark, marginTop: 10, fontSize: 16 }}>
                        {this.state.value}
                    </Text>
                </View>
            );
        }
    }

    render() {
        return (<Container>
            <View style={{ elevation: 2, marginBottom: 0, backgroundColor: '#F5F5F5', height: 30, justifyContent: 'center' }} >
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ color: Colors.primary, fontSize: FontSize.large }}>{this.state.title}</Text>
                </View>
            </View>
            <Header style={styles.headerStyle}>
                <View style={{ justifyContent: 'center', }}>
                    <Text style={{ textAlign: 'center', color: '#585858', fontSize: FontSize.heading, fontWeight: 'bold', }}>{this.props.company.name}</Text>
                    <Text numberOfLines={2} style={{ textAlign: 'center', color: Colors.lightGrey, fontSize: FontSize.medium, fontWeight: 'bold', }}>{this.props.company.formatted_address}</Text>
                </View>
            </Header>
            <StatusBar
                backgroundColor="#fff" barStyle="dark-content" />
            <Content padder>
                {
                    this._renderContent()
                }
            </Content>
            {
                !this.state.isEditMode ?
                    <Footer style={styles.footerStyle}>
                        <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
                            <Pressable style={({ pressed }) => [
                                pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 110 }
                            ]} onPress={() => this.setState({ removeNoteAlert: true })} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                                <Text style={{
                                    color: Colors.deepPurple, textAlign: 'center',
                                    textTransform: 'capitalize', fontSize: FontSize.heading
                                }}>Delete</Text>
                            </Pressable>
                            <Pressable style={({ pressed }) => [
                                pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 110 }
                            ]} onPress={() => this.setState({ isEditMode: 2, title: 'Edit favuorite notes' })} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                                <Text style={{
                                    color: Colors.deepPurple, textAlign: 'center',
                                    textTransform: 'capitalize', fontSize: FontSize.heading
                                }}>Edit</Text>
                            </Pressable>
                            <Pressable style={({ pressed }) => [
                                pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 110 }
                            ]} onPress={() => this.props.close()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                                <Text style={{
                                    color: Colors.deepPurple, textAlign: 'center',
                                    textTransform: 'capitalize', fontSize: FontSize.heading
                                }}>Close</Text>
                            </Pressable>
                        </View>
                    </Footer>
                    :
                    null
            }
            {
                this._removeFavAlert()
            }
        </Container>);
    }

}


const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: Colors.white,
        flexDirection: 'column',
        height: 60,
        elevation: 0,
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 10,
        shadowOpacity: 0,
        borderWidth: 0,
        borderBottomWidth: 0
    },
    footerStyle: {
        backgroundColor: Colors.white, height: 115,
        paddingLeft: 10, paddingRight: 10,
        paddingTop: 10, shadowOpacity: 0,
        borderTopWidth: 0,
        elevation: 0
    },
    inputStyle: {
        fontSize: 16,
        borderWidth: 0,
        borderRadius: 5,
        color: Colors.darkGrey,
        borderColor: Colors.lightGrey,
        minHeight: 45,
        maxHeight: 200,
        padding: 0
    },
    favButtonActive: {
        opacity: 0,
        elevation: 1,
        borderRadius: 5,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 40,
    },
    favButton: {
        elevation: 1,
        borderRadius: 5,
        borderWidth: 0.3,
        borderColor: "#CDCDCD",
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 60,
        backgroundColor: Colors.white,
        borderRadius: 5, shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
    }
});