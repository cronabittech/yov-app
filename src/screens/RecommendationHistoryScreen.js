import moment from "moment";
import { Container, Content, Spinner } from 'native-base';
import React, { PureComponent } from "react";
import { FlatList, StyleSheet, View } from 'react-native';
import { getWithLogin } from "../api/api";
import { Button, RecommendItem, SubHeader } from '../component';
import {callNumber, doMail} from '../config/util';


const VibrateStart = () => {
}

const VibrateStop = () => {
}


export default class RecommendationHistoryScreen extends PureComponent {

    state = {
        isLoading: false,
        starList: undefined
    }

    componentDidMount() {
        this.props.navigation.setOptions({
            headerRight: () => (
                <Button type="header" text="Search" onPress={() => this.props.navigation.navigate('SearchFromMyRecommned')} />
            ),
        });
        this.getRecommendation();
    }
    gotoCompany(item) {
        this.props.navigation.navigate('Recommend', { place_id:item.company.place_id, 
        defaultOpen: "view_recommeded" });
    }

    async getRecommendation() {
        this.setState({ isLoading: true });
        try{
            var data = await getWithLogin("user-recomneded");
            console.log(data);
            this.setState({ starList: data.data });
            this.setState({ isLoading: false });
        }
        catch(error){
            console.log(error);
        }
    }

    momentFormateDate(date) {
        if (date) {
            return moment(date + "", "YYYY-MM-DD HH:mm:ss").format('DD/MM/YYYY');
        } else {
            return moment().format('DD/MM/YYYY hh:mm A');
        }
    }

    _renderItem = ({ item, index }) => {
        return (
            <RecommendItem
                onHeaderClick={() => this.gotoCompany(item)}
                onStarClick={() => this.gotoCompany(item)}
                onRecoClick={() => this.gotoCompany(item)}
                icon={item.company.icon}
                name={item.company.name}
                starDate={this.momentFormateDate(item.created_at)}
                formatted_address={item.company.formatted_address}
                phone={() => callNumber(item.company.formatted_phone_number)}
                mail={() => doMail(item.company.email)}
            />
        );
    }

    _renderScreen() {
        if (this.state.isLoading) {
            return (<Container>
                <SubHeader text="My recommended businesses" />
                <Spinner color='green' style={{ alignSelf: 'center', marginTop: 20 }} />
            </Container>);
        } else {
            return (
                <Container >
                    <SubHeader text="My recommended businesses" />
                    <Content>
                        <FlatList
                            keyboardShouldPersistTaps={'handled'}
                            data={this.state.starList}
                            renderItem={this._renderItem}
                            keyExtractor={item => item.id}
                            ItemSeparatorComponent={(props) => {
                                return (<View style={{ height: 1, backgroundColor: "#CDCDCD", marginLeft: 100, marginRight: 10 }} />);
                            }}
                        />
                    </Content>
                </Container>
            );
        }
    }

    render() {
        return (
            this._renderScreen()
        );
    }

}

const styles = StyleSheet.create({

});