import AsyncStorage from '@react-native-community/async-storage';
import { Container, Content, Input, Item, Spinner, Text } from 'native-base';
import React, { PureComponent } from "react";
import { Image, Pressable, StatusBar, StyleSheet, View } from 'react-native';
import Snackbar from 'react-native-snackbar';
import { connect } from 'react-redux';
import { Button } from '../component';
import Action from '../config/action';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import { setToken } from '../config/global';
import styles from "../config/style";


const VibrateStart = () => {
}
const VibrateStop = () => {
}

class LoginScreen extends PureComponent {

    state = {
        username: '',
        password: '',
    }

    _showMessage(msg) {
        Snackbar.show({
            text: msg,
            duration: Snackbar.LENGTH_INDEFINITE,
            action: {
                text: 'close',
                textColor: 'red',
                onPress: () => { Snackbar.dismiss(); },
            },
        });
    }

    _submitPress() {
        if (this.state.username == "") {
            this._showMessage("Enter email");
        } else if (this.state.password == "") {
            this._showMessage("Enter password");
        } else {
            this.props.login(this.state)
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.message !== this.props.message) {
            switch (this.props.type) {
                case Action.LOGIN_FAIL:
                    this._showMessage(this.props.message);
                    break;
                case Action.LOGIN_SUCCESS:
                    AsyncStorage.setItem('username', this.state.username);
                    AsyncStorage.setItem('password', this.state.password);
                    setToken(this.props.message);
                    AsyncStorage.setItem('token', this.props.message);
                    this.props.navigation.reset({ routes: [{ name: 'Home' }] });
                    break;
            }
        }
    }


    _renderSubmitButton() {
        if (this.props.isLoading) {
            return (<Spinner color='green' />);
        } else {
            return (
                <Pressable onPress={() => this._submitPress()} style={({ pressed }) => [
                    pressed ? style.buttonActive : style.button
                ]} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                    <Text style={{ fontWeight: 'bold', color: Colors.white, textAlign: 'center', textTransform: 'capitalize' }}>Sign In</Text>
                </Pressable>
            );
        }
    }

    goto(route){
        setTimeout(()=>{
            this.props.navigation.navigate(route)
        },50);
    }
    

    render() {
        return (
            <Container>
                <StatusBar backgroundColor="white" barStyle="dark-content" />
                <Content padder>
                    <Image style={[style.logo]}
                        source={require('../assets/img/logo.png')}
                    />
                    <Item regular style={{ height: 35, borderRadius: 5 }}>
                        <Input placeholder='Email Address' 
                         autoCapitalize='none' keyboardType='email-address'
                            placeholderTextColor="#cdcdcd" style={styles.textInput}
                            onChangeText={(text) => this.setState({ username: text })} />
                    </Item>
                    <Item regular style={{ marginTop: 20, height: 35, borderRadius: 5 }}>
                        <Input secureTextEntry={true} placeholder='Password'
                            style={styles.textInput}  autoCapitalize='none' placeholderTextColor="#cdcdcd"
                            onChangeText={(text) => this.setState({ password: text })} />
                    </Item>
                    {this._renderSubmitButton()}
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
                        <Button type="trasnparent" text="Reset Password" onPress={() => this.goto('reset-password')} />
                        <Button type="trasnparent" text="Create new account" onPress={() => this.goto('Register')} />
                    </View>
                </Content>
            </Container>
        );
    }
}

const style = StyleSheet.create({
    logo: {
        width: 200,
        resizeMode: 'contain',
        alignSelf: 'center',
        height: 100,
        marginTop: 40,
        marginBottom: 35
    }, textInput: {
        flex: 1,
        color: Colors.deepPurple,
        textAlign: 'center',
        fontSize: FontSize.large,
        alignSelf: 'center',
        padding: 0,
    }, buttonActive: {
        opacity: 0,
        elevation: 1,
        borderRadius: 3,
        marginTop: 20,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 35
    },
    button: {
        elevation: 1,
        borderRadius: 3,
        marginTop: 20,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 35
    }
});


const mapStateToProps = (state) => {
    console.log('State:');
    console.log(state);

    return {
        // counter: state.counterReducer.counter,
        error: state.login.error,
        isLoading: state.login.isLoading,
        message: state.login.message,
        type: state.login.type,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (data) => {
            dispatch({
                type: Action.LOGIN,
                value: data
            })
        }
    };
};

// Exports
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);