import moment from 'moment';
import {Container, Content, Text, Header, Card} from 'native-base';
import React, {PureComponent} from 'react';
import {Modal, StatusBar, StyleSheet, View, FlatList} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import {Button} from '../component';
import {getWithLogin} from '../api/api';

class FeedbackReviewScreen extends PureComponent {
  state = {
    step: 0,
    company: undefined,
    feedback: undefined,
    showThankYou: false,
    feedbacks: [],
  };

  constructor() {
    super();
  }

  _back() {
    this.props.navigation.goBack();
  }

  async getFeedbacks() {
    const response = await getWithLogin('fed/usr/' + this.state.company.id);
    console.log(response);
    this.setState({feedbacks: response.data});
  }

  _thankUsAlert() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.showThankYou}>
        <View style={style.centeredView}>
          <View style={style.modalView}>
            <Text style={{textAlign: 'center'}}>
              Thank you for taking time to let us know your opinion.
            </Text>
          </View>
        </View>
      </Modal>
    );
  }

  momentFormateDate(date) {
    if (date) {
      return moment(date + '', 'YYYY-MM-DD HH:mm:ss').format(
        'DD/MM/YYYY HH:mm',
      );
    } else {
      return moment().format('DD/MM/YYYY HH:mm');
    }
  }

  componentDidMount() {
    this.setState(
      {
        company: this.props.route.params.company,
        feedback: this.props.route.params.feedback,
      },
      () => {
        this.getFeedbacks();
      },
    );
    this.props.navigation.setOptions({
      headerRight: () => (
        <View style={{marginRight: 5}}>
          <Button type="header" text="Close" onPress={() => this._back()} />
        </View>
      ),
    });
    this.props.navigation.addListener('focus', () => {
      this.getFeedbacks();
    });
  }
  openReplies(item, index) {
    this.props.navigation.navigate('FeedbackMessage', {
      feedbacks: item,
      company: this.state.company,
      index:index
    });
  }

  renderItem = ({item, index}) => {
    if (item.is_read === 1) {
      return (
        <Card style={{padding: 10}} onTouchEnd={() => this.openReplies(item,index)}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              width: '100%',
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                alignSelf: 'center',
                color: Colors.lessDarkGrey,
              }}>
              Sent: {this.momentFormateDate(item.created_at)}
            </Text>
            {item.count <= 0 ? (
              <Text style={{color: Colors.darkBlue}}>
                {item.totalReply} Replies
              </Text>
            ) : (
              <View
                style={{
                  backgroundColor: Colors.primary,
                  padding: 5,
                  paddingTop: 3,
                  paddingBottom: 3,
                  borderRadius: 5,
                }}>
                <Text style={{color: Colors.white}}>
                  {item.totalReply} Replies
                </Text>
              </View>
            )}
          </View>
          {item.feed_short == 'comment' ? (
            <Text style={{marginTop: 5}}>Review submitted</Text>
          ) : (
            <Text style={{marginTop: 5}}>Q. {item.question}</Text>
          )}
          <Text style={{color: Colors.primary, marginTop: 3}}>
            A. {item.full_answer}
          </Text>
          {
             item.is_read == 0 ? 
             <Text style={{color: Colors.red, marginTop: 3}}>
             Not seen by bussiness owner.
           </Text>
             :
             null
          }
        </Card>
      );
    } else {
      return (
        <Card
          style={{padding: 10, backgroundColor: Colors.lightGrey}}
          onTouchEnd={() => this.openReplies(item, index)}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              width: '100%',
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                alignSelf: 'center',
                color: Colors.white,
              }}>
              Sent: {this.momentFormateDate(item.created_at)}
              <Text
                style={{fontSize: 16, fontWeight: 'bold', color: Colors.white}}>
                {' '}
                View Only
              </Text>
            </Text>
            {item.count <= 0 ? (
              <Text style={{color: Colors.darkBlue}}>
                {item.totalReply} Replies
              </Text>
            ) : (
              <View
                style={{
                  backgroundColor: Colors.primary,
                  padding: 5,
                  paddingTop: 3,
                  paddingBottom: 3,
                  borderRadius: 5,
                }}>
                <Text style={{color: Colors.white}}>
                  {item.totalReply} Replies
                </Text>
              </View>
            )}
          </View>
          {item.feed_short == 'comment' ? (
            <Text style={{marginTop: 5}}>Review submitted</Text>
          ) : (
            <Text style={{marginTop: 5}}>Q. {item.question}</Text>
          )}
          <Text style={{color: Colors.primary, marginTop: 3}}>
            A. {item.full_answer}
          </Text>
          {
             item.is_read == 0 ? 
             <Text style={{color: Colors.red, marginTop: 3}}>
             Not seen by bussiness owner.
           </Text>
             :
             null
          }
        </Card>
      );
    }
  };

  render() {
    return (
      <Container>
        <Text
          style={{
            backgroundColor: Colors.primary,
            color: Colors.white,
            textAlign: 'center',
            padding: 5,
            fontSize: FontSize.heading,
          }}>
          My feedback
        </Text>
        {this.state.company ? (
          <Header
            style={{
              height: 60,
              backgroundColor: Colors.white,
              flexDirection: 'row',
              elevation: 0,
            }}>
            <View style={{justifyContent: 'center', marginTop: 10}}>
              <Text
                style={{
                  textAlign: 'center',
                  color: '#585858',
                  fontSize: FontSize.heading,
                  fontWeight: 'bold',
                }}>
                {this.state.company.name}
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  color: Colors.lightGrey,
                  fontSize: FontSize.medium,
                  fontWeight: 'bold',
                }}>
                {this.state.company.formatted_address}
              </Text>
            </View>
          </Header>
        ) : null}
        {this.state.company ? (
          <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        ) : null}
        <Content padder>
          {this.state.feedbacks.length > 0 ? (
            <FlatList
              contentContainerStyle={{marginBottom: 100}}
              keyboardShouldPersistTaps="always"
              data={this.state.feedbacks}
              keyExtractor={(item) => item.id}
              renderItem={this.renderItem}
            />
          ) : null}
          {this._thankUsAlert()}
        </Content>
      </Container>
    );
  }
}
export default FeedbackReviewScreen;

const style = StyleSheet.create({
  feedbackLayout: {
    borderRadius: 15,
    elevation: 5,
    padding: 15,
    margin: 5,
    backgroundColor: Colors.white,
    maxWidth: 325,
  },

  buttonText: {
    color: '#3962a9',
    paddingRight: 10,
    paddingLeft: 10,
    textTransform: 'capitalize',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 35,
    elevation: 5,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
  },
});
