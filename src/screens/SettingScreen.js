import AsyncStorage from '@react-native-community/async-storage';
import {Body, Container, Content, ListItem, Right, Text} from 'native-base';
import React, {PureComponent} from 'react';
import {StatusBar} from 'react-native';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';

class SettingScreen extends PureComponent {
  static navigationOptions = {
    headerShown: false,
  };

  constructor() {
    super();
  }

  _showMessage(msg) {
    Snackbar.show({
      text: msg,
      duration: Snackbar.LENGTH_INDEFINITE,
      action: {
        text: 'close',
        textColor: 'red',
        onPress: () => {
          Snackbar.dismiss();
        },
      },
    });
  }

  _logOut() {
    AsyncStorage.setItem('username', '');
    AsyncStorage.setItem('password', '');
    this.props.navigation.reset({routes: [{name: 'splash'}]});
  }
  componentDidMount() {
    this.props.navigation.setOptions({title: 'Setting'});
  }

  render() {
    return (
      <Container>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <Text
          style={{
            backgroundColor: '#008000',
            color: '#fff',
            textAlign: 'center',
            padding: 5,
          }}>
          Settings
        </Text>

        <Content>
          <ListItem
            style={{marginTop: 10}}
            icon
            onPress={() => this._showMessage('Coming Soon')}>
            <Body>
              <Text note>Edit Profile</Text>
            </Body>
            <Right>
              <Icon active name="chevron-right" />
            </Right>
          </ListItem>

          <ListItem
            style={{marginTop: 10}}
            icon
            onPress={() => this._showMessage('Coming Soon')}>
            <Body>
              <Text note>About Us</Text>
            </Body>
            <Right>
              <Icon active name="chevron-right" />
            </Right>
          </ListItem>

          <ListItem
            style={{marginTop: 10}}
            icon
            onPress={() => this._showMessage('Coming Soon')}>
            <Body>
              <Text note>Rate The App</Text>
            </Body>
            <Right>
              <Icon active name="chevron-right" />
            </Right>
          </ListItem>

          <ListItem
            style={{marginTop: 10}}
            icon
            onPress={() => this._showMessage('Coming Soon')}>
            <Body>
              <Text note>Share The App</Text>
            </Body>
            <Right>
              <Icon active name="chevron-right" />
            </Right>
          </ListItem>

          <ListItem
            style={{marginTop: 10}}
            icon
            onPress={() =>
              this.props.navigation.navigate('TermsConditionView')
            }>
            <Body>
              <Text note>Terms &amp; condition</Text>
            </Body>
            <Right>
              <Icon active name="chevron-right" />
            </Right>
          </ListItem>
          <ListItem
            style={{marginTop: 10}}
            icon
            onPress={() => this.props.navigation.navigate('ChangePassword')}>
            <Body>
              <Text note>Change Password</Text>
            </Body>
            <Right>
              <Icon active name="chevron-right" />
            </Right>
          </ListItem>
          <ListItem style={{marginTop: 10}} icon onPress={() => this._logOut()}>
            <Body>
              <Text note>Sign Out</Text>
            </Body>
            <Right>
              <Icon active name="chevron-right" />
            </Right>
          </ListItem>
        </Content>
      </Container>
    );
  }
}
export default SettingScreen;
