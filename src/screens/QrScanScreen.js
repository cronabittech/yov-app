import React, {PureComponent} from 'react';
import {View} from 'react-native';
import BarcodeMask from 'react-native-barcode-mask';
import QRCodeScanner from 'react-native-qrcode-scanner';

class QrScanScreen extends PureComponent {
  onSuccess(e) {
    this.props.navigation.navigate('PromotionDetails',{id:e.data});
  }
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#000'}}>
        <QRCodeScanner
          showMarker
          reactivate={true}
          onRead={this.onSuccess.bind(this)}
          customMarker={
            <BarcodeMask
              width={250}
              height={250}
              edgeColor={'#fff'}
              edgeRadius={25}
              outerMaskOpacity={0.0}
              animatedLineColor={'#fff'}
              lineAnimationDuration={2000}></BarcodeMask>
          }
        />
      </View>
    );
  }
}
export default QrScanScreen;
