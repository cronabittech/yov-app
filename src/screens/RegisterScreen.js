import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {
  Button,
  Container,
  Content,
  Input,
  Item,
  Picker,
  Spinner,
  Text,
} from 'native-base';
import React, {PureComponent} from 'react';
import {Image, Pressable, StatusBar, StyleSheet, ScrollView,
  View} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {connect} from 'react-redux';
import Action from '../config/action';
import Colors from '../config/colors';
import styles from '../config/style';
import {
  ModelLayout,
  SubHeader,
  Button as YovButton,
  TermCondition
} from '../component';

const VibrateStart = () => {};
const VibrateStop = () => {};
var strongRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{10,}$/;
let emailRegx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;

class RegisterScreen extends PureComponent {
  content=null;
  state = {
    first_name: '',
    last_name: '',
    email: '',
    contact_no: '',
    password: '',
    cpassword: '',
    gender: '',
    otp: '',
    dob: 'Select Birthdate',
    showDatePicker: false,
    step: 0,
    showSentAuthMSG: false,
    showOtpField: false,
    correctOtp: '',
    userId: '',
    referal: ''
  };

  onDateChange(value) {
    var date = moment(value).format('DD/MM/YYYY');
    this.setState({dob: date});
  }

  onValueChange(value) {
    this.setState({
      gender: value,
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.message !== this.props.message) {
      switch (this.props.type) {
        case Action.REGISTER_FAIL:
          this._showMessage(this.props.message);
          break;
        case Action.REGISTER_SUCCESS:
          this._showMessage(this.props.message);
          setTimeout(() => {
            // show OTP field
            this.setState({step: 1, showSentAuthMSG: true});
            console.log('register success show OTP');
            // this.props.navigation.navigate('Login');
          }, 1000);
          break;
      }
    }
  }

  _submitPress() {
    // this.props.register('data test')
    // if (this.state.first_name == '') {
    //   this._showMessage('Enter first name');
    // } else if (this.state.last_name == '') {
    //   this._showMessage('Enter last name');
    // } else if (this.state.email == '') {
    //   this._showMessage('Enter email');
    // } else if (this.state.dob == '') {
    //   this._showMessage('Select date of birth');
    // } else if (this.state.password == '') {
    //   this._showMessage('Enter password');
    // } else if (!strongRegex.test(this.state.password)) {
    //   this._showMessage('Password must be 10 char long and must have atleast 1 number and 1 capital letter.');     
    //   return;
    // } else if (this.state.cpassword == '') {
    //   this._showMessage('Enter confirm password');
    // } else if (this.state.password != this.state.cpassword) {
    //   this._showMessage("Confirm password didn't match");
    // } else {
      if(this.state.step == 0){
          this.props.register(this.state);
      }else if(this.state.step == 1){
        this.setState({
          step:2
        });
      }
    // }
  }

  _showMessage(msg) {
    Snackbar.show({
      text: msg,
      duration: Snackbar.LENGTH_INDEFINITE,
      action: {
        text: 'close',
        textColor: 'red',
        onPress: () => {
          Snackbar.dismiss();
        },
      },
    });
  }
  _ShowSentAuthCodeMSG() {
    return (
      <ModelLayout visible={this.state.showSentAuthMSG} centeredViewStyle={{ margin: 0, marginTop: 0 }}>
        
        <Text style={{textAlign: 'center'}}>
        You have been sent an email, which includes an authorisation code. Please enter the authorisation code.
        </Text>
        <View
          style={{
            // width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <YovButton
            type="textButton"
            text="Ok"
            onPress={() =>
              this.setState({
                showOtpField: true,
                showSentAuthMSG: false
              })
            }
          />
        </View>
      </ModelLayout>
    );
  }

  isValid(){
    if(this.state.step == 0){
      if(this.state.first_name != '' 
        && this.state.last_name != '' 
        && this.state.email != '' 
        && emailRegx.test(this.state.email)
        && this.state.contact_no != ''){
        return true;
      }
    } else if(this.state.step == 1){
      if(this.state.first_name != '' 
        && this.state.last_name != '' 
        && this.state.email != '' 
        && emailRegx.test(this.state.email)
        && this.state.contact_no != ''
        && this.state.otp == this.state.correctOtp){
        return true;
      }
    }else if(this.state.step == 3){
      if(this.state.gender != '' 
        && this.state.dob != '' 
        && this.state.password != '' 
        && this.state.cpassword != '' 
        && this.state.password == this.state.cpassword
        && strongRegex.test(this.state.password)){
        return true;
      }
    }
    return false;
  }

  _renderSubmitButton() {
    if (this.props.isLoading) {
      return <Spinner color="green" />;
    } else {
      if (this.isValid()) {
        return (
          <Pressable
            onPress={() => this._submitPress()}
            style={({pressed}) => [pressed ? styles.buttonActive : styles.button]}
            onPressIn={() => VibrateStart()}
            onPressOut={() => VibrateStop()}>
            <Text
              style={{
                fontWeight: 'bold',
                color: Colors.white,
                textAlign: 'center',
                textTransform: 'capitalize',
              }}>
              Done
            </Text>
          </Pressable>
        );
      }
    }
  }

  setSelectedDate(event, selectedDate) {
    this.setState({
      showDatePicker: false,
    });
    if (event.type == 'set') {
      this.onDateChange(selectedDate);
    }
  }

  _renderStep() {
    if (this.state.step == 0) {
      return (
        <Content padder >         
          <Item regular style={styles.mt20}>
            <Input
              placeholder="First Name"
              placeholderTextColor="#cdcdcd"
              autoCapitalize="none"
              onChangeText={(text) => this.setState({first_name: text})}
            />
          </Item>
          <Item regular style={styles.mt20}>
            <Input
              placeholder="Last Name"
              placeholderTextColor="#cdcdcd"
              autoCapitalize="none"
              onChangeText={(text) => this.setState({last_name: text})}
            />
          </Item>
          <Item regular style={styles.mt20}>
            <Input
              placeholder="Email Address"
              keyboardType="email-address"
              autoCapitalize="none"
              placeholderTextColor="#cdcdcd"
              onChangeText={(text) => this.setState({email: text})}
            />
          </Item>
          <Item regular style={styles.mt20}>
            <Input
              placeholder="Mobile number"
              keyboardType="phone-pad"
              autoCapitalize="none"
              placeholderTextColor="#cdcdcd"
              onChangeText={(text) => this.setState({contact_no: text})}
            />
          </Item>
          
          {this._renderSubmitButton()}


          <Button block transparent style={styles.mt20}
            onPress={() => this.props.navigation.navigate('Login')}>
            <Text style={{textTransform: 'capitalize'}}>Have an account?</Text>
            <Text
              style={{fontWeight: 'bold', marginLeft: -28, textTransform: 'capitalize'}}>
              Sign In
            </Text>
          </Button>
        </Content>);
      }else if (this.state.step == 1){
        return (
          <Content padder >  
          <Text style={{textAlign: 'center'}}>
            Your verification code is: X5VJL3A 
          </Text>

          <Item regular style={styles.mt20}>
            <Input
              placeholder="First Name"
              placeholderTextColor="#cdcdcd"
              autoCapitalize="none"
              onChangeText={(text) => this.setState({first_name: text})}
              disabled={true}
            />
          </Item>
          <Item regular style={styles.mt20}>
            <Input
              placeholder="Last Name"
              placeholderTextColor="#cdcdcd"
              autoCapitalize="none"
              onChangeText={(text) => this.setState({last_name: text})}
              disabled={true}
            />
          </Item>
          <Item regular style={styles.mt20}>
            <Input
              placeholder="Email Address"
              keyboardType="email-address"
              autoCapitalize="none"
              placeholderTextColor="#cdcdcd"
              onChangeText={(text) => this.setState({email: text})}
              disabled={true}
            />
          </Item>
          <Item regular style={styles.mt20}>
            <Input
              placeholder="Mobile number"
              keyboardType="phone-pad"
              autoCapitalize="none"
              placeholderTextColor="#cdcdcd"
              onChangeText={(text) => this.setState({contact_no: text})}
              disabled={true}
            />
          </Item>
          <Item regular style={styles.mt20}>
            <Input
              placeholder="Authorisation code"
              placeholderTextColor="#cdcdcd"
              autoCapitalize="none"
              onChangeText={(text) => this.setState({otp: text})}
            />
          </Item>
          
          {this._renderSubmitButton()}

          
          <Button block transparent style={styles.mt20}
            onPress={() => this.props.navigation.navigate('Login')}>
            <Text style={{textTransform: 'capitalize'}}>Have an account?</Text>
            <Text
              style={{fontWeight: 'bold', marginLeft: -28, textTransform: 'capitalize'}}>
              Sign In
            </Text>
          </Button>
          
          </Content>);
      }else if (this.state.step == 2){
        return (
          <Content padder >
            <Text style={style.userDetailText}>{this.state.first_name} {this.state.last_name}</Text>
            <Text style={style.userDetailText}>{this.state.email}</Text>
            <Text style={style.userDetailText}>{this.state.contact_no}</Text>
            <ScrollView style={style.tAndConditionArea}>
              <TermCondition></TermCondition>
            </ScrollView>
            <Pressable
              onPress={() => this.setState({step: 3})}
              style={({pressed}) => [pressed ? styles.buttonActive : styles.button]}
              onPressIn={() => VibrateStart()}
              onPressOut={() => VibrateStop()}>
              <Text
                style={{
                  fontWeight: 'bold',
                  color: Colors.white,
                  textAlign: 'center',
                }}> I have read and agree to terms & conditions</Text>
            </Pressable>
          </Content>);
      }else if(this.state.step == 3){
        return (
          <Content padder>
            <Text style={style.userDetailText}>{this.state.first_name} {this.state.last_name}</Text>
            <Text style={style.userDetailText}>{this.state.email}</Text>
            <Text style={style.userDetailText}>{this.state.contact_no}</Text>

            <Item regular style={styles.mt20}>
              <Picker
                mode="dropdown"
                // iosIcon={<Icon name="arrow-down" />}
                placeholder="Select Gender"
                placeholderStyle={{color: '#000000'}}
                style={{width: undefined}}
                selectedValue={this.state.gender}
                onValueChange={this.onValueChange.bind(this)}>
                <Picker.Item label="Select Gender" value="" />
                <Picker.Item label="Male" value="male" />
                <Picker.Item label="Female" value="female" />
                <Picker.Item label="Other" value="other" />
              </Picker>
            </Item>           
          
            <Item regular style={styles.mt20}>
              <Pressable
                style={{flex: 1}}
                onPress={() => this.setState({showDatePicker: true})}>
                <Text style={{marginLeft: 10}}>{this.state.dob}</Text>
              </Pressable>
            </Item>

            <Item regular style={styles.mt20}>
              <Input
                placeholder="Activation code (invitation code) "
                placeholderTextColor="#cdcdcd"
                autoCapitalize="none"
                onChangeText={(text) => this.setState({referal: text})}
              />
            </Item>
          
            <Item regular style={styles.mt20}>
              <Input
                placeholder=" Create a passcode"
                autoCapitalize="none"
                secureTextEntry={true}
                placeholderTextColor="#cdcdcd"
                onChangeText={(text) => this.setState({password: text})}
                onBlur={(e) => {
                  if(!strongRegex.test(this.state.password)){
                    this._showMessage('Password must be 10 char long and must have atleast 1 number and 1 capital letter.');     
                  }
                }}
              />
            </Item>
            <Item regular style={styles.mt20}>
              <Input
                placeholder="Re enter passcode"
                autoCapitalize="none"
                secureTextEntry={true}
                placeholderTextColor="#cdcdcd"
                onChangeText={(text) => this.setState({cpassword: text})}
              />
            </Item>
          {this._renderSubmitButton()}
          {this.state.showDatePicker && (
            <DateTimePicker
              value={new Date()}
              maximumDate={new Date()}
              testID="dateTimePicker"
              mode="date"
              display="spinner"
              onChange={(event, selectedDate) =>
                this.setSelectedDate(event, selectedDate)
              }
            />
          )}
          </Content>);
      }
  }
  _renderScreen() {
    return (
      <Container>
        <SubHeader text="Registration" />
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        {this._renderStep()}
      </Container>

    );
  }

  render() {
    return(
    <View style={{flex: 1}}>
    {this._ShowSentAuthCodeMSG()}
    {this._renderScreen()}
   </View>
    );
  }
}

const style = StyleSheet.create({
  logo: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  userDetailText: {
    textAlign: 'center',
    borderWidth: 1.5,
    borderColor: '#b1cbe4',
    padding: 5,
    marginTop:5,
  },
  tAndConditionArea: {
    borderWidth: 1.5,
    borderColor: '#b1cbe4',
    padding: 5,
    marginTop:5,
    height: 450,
  }
});

const mapStateToProps = (state) => {
  console.log('State:');
  console.log(state);

  return {
    // counter: state.counterReducer.counter,
    error: state.register.error,
    isLoading: state.register.isLoading,
    message: state.register.message,
    type: state.register.type,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    register: (data) => {
      dispatch({
        type: Action.REGISTER,
        value: data,
      });
    },
  };
};

// Exports
export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);
