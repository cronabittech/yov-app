import { Button, Container, Content, Input, Item, Spinner, Text } from 'native-base';
import React from "react";
import { Image, StatusBar, StyleSheet } from 'react-native';
import Snackbar from 'react-native-snackbar';
import { connect } from 'react-redux';
import { Component } from "react/cjs/react.production.min";
import Action from '../config/action';
import styles from "../config/style";

class ForgetPasswordScreen extends Component {
    state = {
        email: '',

    }

    _showMessage(msg) {
        Snackbar.show({
            text: msg,
            duration: Snackbar.LENGTH_INDEFINITE,
            action: {
                text: 'close',
                textColor: 'red',
                onPress: () => { Snackbar.dismiss(); },
            },
        });
    }

    _submitPress() {
        if (this.state.email == "") {
            this._showMessage("Enter email");
        } else {
            this.props.login(this.state)
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.message !== this.props.message) {
            switch (this.props.type) {
                case Action.RESETP_FAIL:
                    this._showMessage(this.props.message);
                    break;
                case Action.RESETP_SUCCESS:
                    this._showMessage(this.props.message);
                  
                    this.props.navigation.reset({ routes: [{ name: 'Login' }] });
                    break;
            }
        }
    }


    _renderSubmitButton() {
        if (this.props.isLoading) {
            return (<Spinner color='green' />);
        } else {
            return (<Button block rounded success style={styles.mt20} onPress={() => this._submitPress()}>
                <Text style={{ fontWeight: 'bold', textTransform: 'capitalize' }}>Recover</Text>
            </Button>);
        }
    }
    render() {
        return (
            <Container>
                <StatusBar backgroundColor="#5eb44d" barStyle="light-content" />
                <Content padder>
                    <Image style={[style.logo]}
                        source={require('../assets/img/logo.png')}
                    />

                    <Item rounded style={styles.mt20}>
                        <Input placeholder='Email Address' keyboardType='email-address'  placeholderTextColor="#cdcdcd"
                            onChangeText={(text) => this.setState({ email: text })} />
                    </Item>

                    {this._renderSubmitButton()}
                    <Button block transparent style={styles.mt20} onPress={() => this.props.navigation.goBack()}>
                        <Text style={{ textTransform: 'capitalize' }}>Have an account?</Text><Text style={{ fontWeight: 'bold', marginLeft: -28, textTransform: 'capitalize' }}>Sign In</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}
const style = StyleSheet.create({
    logo: {
        marginTop: 50,
        width: 250,
        height: 120,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
});

const mapStateToProps = (state) => {
    console.log('State:');
    console.log(state);

    return {
        // counter: state.counterReducer.counter,
        error: state.resetPassword.error,
        isLoading: state.resetPassword.isLoading,
        message: state.resetPassword.message,
        type: state.resetPassword.type,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (data) => {
            dispatch({
                type: Action.RESETP,
                value: data
            })
        }
    };
};

// Exports
export default connect(mapStateToProps, mapDispatchToProps)(ForgetPasswordScreen);