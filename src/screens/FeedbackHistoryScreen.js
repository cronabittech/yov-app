import {Container, Content} from 'native-base';
import React, {PureComponent} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {Button, FeedbackItem, SubHeader} from '../component';
import {getWithLogin} from '../api/api';
import moment from 'moment';

const VibrateStart = () => {};

const VibrateStop = () => {};

export default class FeedbackHistoryScreen extends PureComponent {
  state = {
    feedbacks: [],
  };

  componentDidMount() {
    this.props.navigation.setOptions({
      headerRight: () => (
        <Button
          type="header"
          text="Search"
          onPress={() => this.props.navigation.navigate('Search')}
        />
      ),
    });
    this.getFeedBack();
  }

  async getFeedBack() {
    try {
      const response = await getWithLogin('fed/usr');
      console.log(response);
      this.setState({feedbacks: response.data});
    } catch (e) {
      console.log(e.response);
    }
  }
  momentFormateDate(date) {
    if (date) {
      return moment(date + '', 'YYYY-MM-DD HH:mm:ss').format(
        'DD/MM/YYYY HH:mm',
      );
    } else {
      return moment().format('DD/MM/YYYY HH:mm');
    }
  }

  gotoCompany(item) {
    this.props.navigation.navigate('FeedbackReview', {
      company: item.company,
      feedback: this.state.feedback,
    });
  }

  _renderItem = ({item, index}) => {
    return (
      <FeedbackItem
        count={item.count}
        created_at={this.momentFormateDate(item.created_at)}
        icon={item.company.icon}
        onHeaderClick={() => this.gotoCompany(item)}
        onStarClick={() => this.gotoCompany(item)}
        onRecoClick={() => this.gotoCompany(item)}
        cname={item.company.name}
        post={
          item.company.category === null
            ? 'To be confirmed'
            : item.company.category
        }
        feedback={item.full_answer}
      />
    );
  };
  render() {
    return (
      <Container>
        <SubHeader text="Sent feedback / comments" />
        <Content>
          <FlatList
            keyboardShouldPersistTaps={'handled'}
            data={this.state.feedbacks}
            renderItem={this._renderItem}
            keyExtractor={(item) => item.id}
            ItemSeparatorComponent={(props) => {
              return (
                <View
                  style={{
                    height: 1,
                    backgroundColor: '#CDCDCD',
                    marginRight: 10,
                    marginLeft: 100,
                  }}
                />
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({});
