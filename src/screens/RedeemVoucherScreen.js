import {Button, Container, Content, Spinner, Text, View} from 'native-base';
import React, {useEffect, useState} from 'react';
import {StyleSheet, TextInput} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {getWithLogin} from '../api/api';
import {SubHeader} from '../component';
import Colors from '../config/colors';

function RedeemVoucherScreen(props) {
  const [item, setItem] = useState(null);
  const [loading, setLoding] = useState(true);
  const [code, setCode] = useState('');

  useEffect(() => {
    setItem(props.route.params.item);
    setLoding(false);
  }, []);

  const applyCode = async () => {
    if (code.length > 0) {
      try {
        const response = await getWithLogin(
          'redeem/vocuher/' + item.voucher_id + '/' + code,
        );
        Snackbar.show({
          text: 'Congralatuion!! Voucher redeemed',
          duration: 500,
          textColor: Colors.white,
        });
        props.navigation.pop();
      } catch (e) {
        const response = e.response.data;
        for (var row in response) {
          Snackbar.show({
            text: response[row][0],
            duration: 1000,
            textColor: Colors.red,
          });
        }
      }
    }
  };

  return (
    <Container>
      <SubHeader text="Redeem voucher" />
      {loading ? (
        <Content>
          <Spinner color="green" style={{alignSelf: 'center'}} />
        </Content>
      ) : item ? (
        <Content padder>
          <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
            {item.name}
          </Text>
          <Text
            style={{
              textAlign: 'center',
              fontWeight: 'bold',
              color: Colors.lessDarkGrey,
            }}>
            {item.formatted_address}
          </Text>
          <View style={{marginTop: 10}}>
            <Text style={{color: Colors.primary, textAlign: 'center'}}>
              {item.benifits}
            </Text>
            <TextInput
              placeholder="Voucher code"
              returnKeyType="done"
              style={[style.textInput]}
              autoFocus={true}
              autoCapitalize="none"
              onChangeText={(text) => setCode(text)}
            />
          </View>
          <Button
            success
            block
            style={{marginTop: 15}}
            onPress={() => applyCode()}>
            <Text style={{textTransform: 'capitalize'}}>Redeem voucher</Text>
          </Button>
        </Content>
      ) : null}
    </Container>
  );
}

export default RedeemVoucherScreen;

const style = StyleSheet.create({
  textInput: {
    fontSize: 17,
    borderWidth: 1.5,
    textAlign: 'center',
    marginTop: 20,
    borderRadius: 5,
    color: Colors.primary,
    borderColor: Colors.lightGrey,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
  },
});
