import {Container, Content, Footer, Text} from 'native-base';
import React, {PureComponent} from 'react';
import {Pressable, StatusBar, StyleSheet, TextInput, View} from 'react-native';
import {postWithLogin} from '../api/api';
import {Button} from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';

class SubmitFeedbackScreen extends PureComponent {
  state = {
    step: 0,
    com1: '',
    com2: '',
    com3: '',
    com4: '',
    optionAns: '',
    optionAns2: '',
    optionAns3: '',
    defaultFeedback: 'I thought your products / service was',
    comment: '',
    time: 0,
    commentFocus: false,
    fullanswer: undefined,
  };
  constructor(props) {
    super();
  }

  _back() {
    this.props.close();
  }
  gotoReview() {
    this.props.close();
    this.props.navigation.navigate('FeedbackReview', {
      company: this.props.company,
    });
  }

  getCuurentTime() {
    const currentDate = new Date();
    const timestamp = currentDate.getTime();
    return '' + timestamp;
  }

  step1Ans(ans) {
    this.setState({optionAns: ans});
    this.setState({time: this.getCuurentTime()}, () => {
      this.setState(
        {
          fullanswer: {
            question: 'What do you think about our product(s) / service?',
            feed_short: this.state.optionAns,
            full_answer: ans,
            com_id: this.props.company.id,
            uid: this.state.time,
          },
        },
        () => this.sendFeedback(),
      );
    });
  }

  doStep2() {
    this.setState({step: 2});

    this.setState(
      {
        fullanswer: {
          question:
            'What was ' +
            this.state.optionAns +
            ' about the product(s) / service?',
          feed_short: this.state.optionAns,
          full_answer: this.state.optionAns2,
          com_id: this.props.company.id,
          uid: this.state.time,
        },
      },
      () => this.sendFeedback(),
    );
  }

  doComment() {
    this.props.updateFeedback(this.state.comment);
    this.setState(
      {
        fullanswer: {
          question: 'What do you think about our product(s) / service?',
          feed_short: 'comment',
          full_answer: this.state.comment,
          com_id: this.props.company.id,
          uid: this.getCuurentTime(),
        },
      },
      () => this.sendFeedback(),
    );
  }

  async sendFeedback() {
    const response = await postWithLogin('fed/usr/send', this.state.fullanswer);
    if (this.state.step == 0) {
      this.setState({step: 1});
    } else {
      this.gotoReview();
    }
  }

  forceClose() {
    this.props.updateFeedback('');
    this.props.close();
  }

  _renderQuestion() {
    if (this.state.step == 0) {
      return (
        <View style={{justifyContent: 'center', marginTop: 10}}>
          <Text
            style={{
              textAlign: 'center',
              marginTop: 10,
              fontSize: FontSize.large,
            }}>
            What do you think about our product(s) / service?
          </Text>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 10,
              justifyContent: 'space-between',
            }}>
            <Button
              type="border"
              text="Fantastic"
              onPress={() =>
                setTimeout(() => {
                  this.step1Ans('fantastic');
                }, 50)
              }
            />
            <Button
              type="border"
              text="Good"
              onPress={() =>
                setTimeout(() => {
                  this.step1Ans('good');
                }, 50)
              }
            />
            <Button
              type="border"
              text="Poor"
              onPress={() =>
                setTimeout(() => {
                  this.step1Ans('poor');
                }, 50)
              }
            />
            <Button
              type="border"
              text="Not to my liking"
              onPress={() =>
                setTimeout(() => {
                  this.step1Ans('not to my liking');
                }, 50)
              }
            />
          </View>
          <View style={{marginTop: 10}}>
            <Button
              type="border2"
              text="Enter your comments here"
              onPress={() =>
                setTimeout(() => {
                  this.setState({step: 3});
                }, 50)
              }
            />
          </View>
        </View>
      );
    } else if (this.state.step == 1) {
      return (
        <View style={{justifyContent: 'center', flex: 1, marginTop: 10}}>
          <Text
            style={{
              textAlign: 'center',
              marginTop: 20,
              fontSize: FontSize.large,
            }}>
            <Text>What was </Text>
            <Text style={{color: Colors.primary}}>{this.state.optionAns}</Text>
            <Text> about the product(s) / service?</Text>
          </Text>
          <View style={{marginTop: 10, justifyContent: 'center'}}>
            <TextInput
              placeholder="Comments"
              returnKeyType="done"
              multiline={true}
              autoFocus={true}
              autoCapitalize="none"
              style={[
                style.textInput,
                {
                  borderColor: this.state.commentFocus
                    ? Colors.deepPurple
                    : Colors.lightGrey,
                },
              ]}
              onFocus={() => this.setState({commentFocus: true})}
              onBlur={() => this.setState({commentFocus: false})}
              defaultValue={this.state.optionAns2}
              onChangeText={(text) => this.setState({optionAns2: text})}
            />

            {this.state.optionAns2.length > 0 ? (
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 10,
                  justifyContent: 'space-around',
                }}>
                <Button
                  type="border2"
                  text="Delete"
                  onPress={() => this.setState({optionAns2: ''})}
                />
                <Button
                  type="border2"
                  text="Send"
                  onPress={() =>
                    setTimeout(() => {
                      this.doStep2();
                    }, 50)
                  }
                />
                <Button
                  type="border2"
                  text="Close"
                  onPress={() => this.forceClose()}
                />
              </View>
            ) : (
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 10,
                  justifyContent: 'space-around',
                }}>
                <Button
                  type="border2"
                  text="No further comment"
                  onPress={() =>
                    setTimeout(() => {
                      this.gotoReview();
                    }, 50)
                  }
                />
              </View>
            )}
          </View>
        </View>
      );
    } else if (this.state.step == 3) {
      return (
        <View style={{justifyContent: 'center', flex: 1, marginTop: 10}}>
          <View style={{marginTop: 10, justifyContent: 'center'}}>
            <TextInput
              placeholder="Enter your comments here"
              returnKeyType="done"
              style={[
                style.textInput,
                {
                  borderColor: this.state.commentFocus
                    ? Colors.deepPurple
                    : Colors.lightGrey,
                },
              ]}
              multiline={true}
              autoFocus={true}
              autoCapitalize="none"
              onFocus={() => this.setState({commentFocus: true})}
              onBlur={() => this.setState({commentFocus: false})}
              defaultValue={this.state.comment}
              onChangeText={(text) => this.setState({comment: text})}
            />
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
                justifyContent: 'space-evenly',
              }}>
              {this.state.comment.length > 0 ? (
                <Button
                  type="border2"
                  text="Delete"
                  onPress={() => this.setState({comment: ''})}
                />
              ) : null}
              {this.state.comment.length > 0 ? (
                <Button
                  type="border2"
                  text="Send"
                  onPress={() =>
                    setTimeout(() => {
                      this.doComment();
                    }, 50)
                  }
                />
              ) : null}
              <Button
                type="border2"
                text="Close"
                onPress={() => this.props.close()}
              />
            </View>
          </View>
        </View>
      );
    }
  }

  render() {
    return (
      <Container>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <Text
          style={{
            marginTop: 7,
            backgroundColor: '#f5f5f5',
            color: '#008000',
            textAlign: 'center',
            padding: 5,
            fontSize: FontSize.heading,
          }}>
          Submit your feedback
        </Text>
        <Content padder>
          <View style={{justifyContent: 'center'}}>
            <Text
              style={{
                textAlign: 'center',
                color: '#585858',
                fontSize: FontSize.heading,
                fontWeight: 'bold',
              }}>
              {this.props.company.name}
            </Text>
            <Text
              style={{
                textAlign: 'center',
                color: Colors.lightGrey,
                fontSize: FontSize.medium,
                fontWeight: 'bold',
              }}>
              {this.props.company.formatted_address}
            </Text>
          </View>
          {this._renderQuestion()}
        </Content>
        {this.state.step == 0 ? (
          <Footer style={style.footerStyle}>
            <Pressable
              style={({pressed}) => [
                pressed ? style.favButtonActive : style.favButton,
                {backgroundColor: Colors.white, width: 120},
              ]}
              onPress={() => this.props.close()}>
              <Text
                style={{
                  color: Colors.deepPurple,
                  textAlign: 'center',
                  textTransform: 'capitalize',
                  fontSize: FontSize.heading,
                }}>
                Close
              </Text>
            </Pressable>
          </Footer>
        ) : null}
      </Container>
    );
  }
}
export default SubmitFeedbackScreen;

const style = StyleSheet.create({
  footerStyle: {
    backgroundColor: Colors.white,
    height: 100,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    shadowOpacity: 0,
    borderTopWidth: 0,
    elevation: 0,
  },
  textInput: {
    fontSize: 17,
    borderWidth: 1.5,
    borderRadius: 5,
    color: Colors.primary,
    borderColor: Colors.lightGrey2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 12,
    paddingBottom: 12,
    maxHeight: 250,
  },
  buttonText: {
    color: Colors.deepPurple,
    paddingRight: 10,
    paddingLeft: 10,
    fontSize: FontSize.medium,
    textAlign: 'center',
    textTransform: 'capitalize',
  },
  button: {
    borderColor: Colors.primary,
    borderWidth: 2,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    borderRadius: 5,
    paddingLeft: 1,
    paddingRight: 1,
    paddingTop: 3,
    paddingBottom: 3,
    elevation: 2,
  },
  favButtonActive: {
    opacity: 0,
    elevation: 1,
    borderRadius: 5,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    height: 40,
  },
  favButton: {
    elevation: 1,
    borderRadius: 5,
    borderWidth: 0.3,
    borderColor: '#CDCDCD',
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    height: 50,
    backgroundColor: Colors.white,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 2,
  },
});
