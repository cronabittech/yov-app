import moment from "moment";
import { Container, Content, Spinner } from 'native-base';
import React, { PureComponent } from "react";
import { FlatList, View } from 'react-native';
import { getWithLogin } from "../api/api";
import { Button, ProfileNoteItem, SubHeader } from "../component";


const VibrateStart = () => {
}

const VibrateStop = () => {
}

export default class ProfileNoteHistoryScreen extends PureComponent {

    state = {
        isLoading: false,
        noteList:undefined
    }

    componentDidMount(){
        this.getNoteHistory();
        this.props.navigation.setOptions({
            headerRight: () => (
                <Button type="header" text="Search" onPress={() => this.props.navigation.navigate('SearchFromMyNotes')} />
            ),
        });
    }

    momentFormateDate(date) {
        if (date) {
            return moment(date + "", "YYYY-MM-DD HH:mm:ss").format('DD/MM/YYYY');
        } else {
            return moment().format('DD/MM/YYYY');
        }
    }

    async getNoteHistory() {
        this.setState({ isLoading: true });
        try {
            var data = await getWithLogin("user/notes");
            console.log( data.data);
            this.setState({ noteList: data.data,isLoading: false })
        } catch (error) {
            console.log(error.response.data);
            this.setState({ isLoading: false });
        }
    }

    gotoCompany(item) {
        this.props.navigation.navigate('Recommend', { place_id:item.place_id, 
        defaultOpen: "view_profile_note" });
    }

    _renderItem = ({ item, index }) => {
        return (
            <ProfileNoteItem
                onHeaderClick={() => this.gotoCompany(item)}
                onStarClick={() => this.gotoCompany(item)}
                onRecoClick={() => this.gotoCompany(item)}
                icon={item.com_icon}
                name={item.com_name}
                onStarClick={() => this.gotoCompany(item)}
                formatted_address={item.com_address}
                starDate={this.momentFormateDate(item.created_at)}
                note={item.note}
            />
        );
    }

    _renderScreen() {
        if (this.state.isLoading) {
            return (<Container>
                <SubHeader text="My private notes" />
                <Spinner color='green' style={{ alignSelf: 'center', marginTop: 20 }} />
            </Container>);
        } else {
            return (
                <Container >
                    <SubHeader text="My private notes" />
                    <Content>
                        <FlatList
                            keyboardShouldPersistTaps={'handled'}
                            data={this.state.noteList}
                            renderItem={this._renderItem}
                            keyExtractor={item => item.id}
                            ItemSeparatorComponent={(props) => {
                                return (<View style={{ height: 1, backgroundColor: "#EEEEEE", marginLeft: 90, marginRight: 10 }} />);
                            }}
                        />
                        <View style={{ height: 1, backgroundColor: "#EEEEEE", marginLeft: 90, marginRight: 10 }} />
                    </Content>
                </Container >
            );
        }
    }
    render() {
        return this._renderScreen();
    }
}