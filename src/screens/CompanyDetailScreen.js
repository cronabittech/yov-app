/*
Company profile screen
:-By kishan hadiyal
*/
import moment from 'moment';
import {Container, Content, Header, Spinner, Text} from 'native-base';
import React, {PureComponent} from 'react';
import {
  Dimensions,
  Image,
  Platform,
  Pressable,
  ScrollView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {deleteWithLogin, getWithLogin, postWithLogin} from '../api/api';
import {
  BottomPopUp,
  Button,
  CompanyShortProfile,
  ModelLayout,
  OptionItem,
  StarRate,
  SubHeader,
} from '../component';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import {callNumber, doMail} from '../config/util';
import {changeProfileTitle} from '../config/ProfileTitle';
import FavoriteNoteScreen from './FavoriteNoteScreen';
import FavouriteScreen from './FavouriteScreen';
import FeedbackReviewScreen from './FeedbackReviewScreen';
import MiniReportSlider from './MiniReportSlider';
import MyActivitiesSlider from './MyActivitiesSlider';
import MyFavouriteScreen from './MyFavouriteScreen';
import MyStarRatingHistory from './MyStarRatingHistory';
import ProfileNoteScreen from './ProfileNoteScreen';
import RecommendationHistory from './RecommendationHistory';
import StarRatingScreen from './StarRatingScreen';
import SubmitFeedbackScreen from './SubmitFeedbackScreen';

const windowHeight =
  Dimensions.get('window').height - (Platform.OS == 'ios' ? 43 : 0);

const VibrateStart = () => {};
const VibrateStop = () => {};

var activityOpen = false;
var feedbackOpen = false;
var starHistoryOpen = false;
var reportOpen = false;
var optionOpen = false;
var starRatingOpen = false;
var feedbackReviewOpen = false;
var recommendHistory = false;
var favouriteFormSheet = false;
var myFavouriteFormSheet = false;
var favNoteSheet = false;
var userNoteSheet = false;

class CompanyDetailScreen extends PureComponent {
  state = {
    isLoading: false,
    place_id: undefined,
    company: undefined,
    showExperiance: false,
    showThankYou: false,
    yov: false,
    starCount1: 0,
    starCount2: 0,
    starCount3: 0,
    estarCount1: 0,
    estarCount2: 0,
    estarCount3: 0,
    starTick: false,
    sheetOpen: false,
    feedOpen: false,
    starPerform: false,
    showRating: false,
    changesMade: false,
    totalRecommendation: 0,
    lastRecommendationDate: undefined,
    autoScrolltype: 'no',
    defaultOpen: '',
    currentStarRating: undefined,
    lastLog: undefined,
    feedback: undefined,
    subtitle: 'Profile',
    lastRecomended: undefined,
    askRecomendAlert: false,
    askRemoveFav: false,
    confirmRemoveFav: false,
    removeRecomendAlert: false,
    confirmRecomendAlert: false,
    changeRecomendAlert: false,
    confirmChangeRecomendAlert: false,
    changeFavAlert: false,
    confirmChangeFavAlert: false,
    showFavRemovedAlert: false,
    changeStarAlert: false,
    confirmChangeStarAlert: false,
    recommendSentAlert: false,
    showRecoTick: false,
    is_fav: 0,
    isFavourite: false,
    favData: undefined,
    bottomSheetHeight: 0,
    userNote: undefined,
    aboveHeight: 0,
    isMessage: false,
    isExsiting: null,
    promotion: null,
  };

  constructor() {
    super();
  }

  slop = {
    top: 30,
    right: 30,
    left: 30,
    bottom: 30,
  };

  momentFormateDate(date) {
    if (date) {
      return moment(date + '', 'YYYY-MM-DD HH:mm:ss').format(
        'DD/MM/YYYY HH:mm',
      );
    } else {
      return moment().format('DD/MM/YYYY HH:mm');
    }
  }

  componentDidMount() {
    this.setState(
      {
        place_id: this.props.route.params.place_id,
        defaultOpen: this.props.route.params.defaultOpen,
      },
      () => {
        if (this.state.place_id && this.state.place_id != '') {
          this.getCompany(false);
        }
      },
    );

    this.barHeaderButton();
    this.props.navigation.addListener('focus', () => {
      if (this.state.place_id && this.state.place_id != '') {
        this.getCompany(true);
      }
    });
  }

  _showMessage(msg) {
    Snackbar.show({
      text: msg,
      duration: Snackbar.LENGTH_INDEFINITE,
      action: {
        text: 'close',
        textColor: 'red',
        onPress: () => {
          Snackbar.dismiss();
        },
      },
    });
  }

  async postExistinUser(result) {
    try {
      let data = {
        comp_id: this.state.company.id,
        is_exsiting: result,
      };
      const response = await postWithLogin('user/exisiting', data);
    } catch (e) {
      console.log(e.response);
    }
  }

  _experienceResult(result) {
    this.postExistinUser(result);
    this.setState({
      showExperiance: false,
      showThankYou: false,
    });
  }

  /* #region  All models */

  _experienceAlert() {
    return (
      <ModelLayout visible={this.state.showExperiance}>
        <Text style={{textAlign: 'center'}}>Are you exsiting customer?</Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() => this._experienceResult(1)}
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => this._experienceResult(0)}
          />
        </View>
      </ModelLayout>
    );
  }

  _thankUsAlert() {
    return (
      <ModelLayout visible={this.state.showThankYou}>
        <Text style={{textAlign: 'center'}}>
          Thanks you for telling us know
        </Text>
      </ModelLayout>
    );
  }

  _askRecomendAlert() {
    return (
      <ModelLayout visible={this.state.askRecomendAlert}>
        <Text style={{textAlign: 'center'}}>Would you like to recommend </Text>
        <Text style={{textAlign: 'center', color: Colors.primary}}>
          {this.state.company ? this.state.company.name : ''}{' '}
        </Text>
        <Text
          numberOfLines={2}
          style={{textAlign: 'center', color: Colors.darkGrey}}>
          {this.state.company ? this.state.company.formatted_address : ''}{' '}
        </Text>
        <Text>to other people using this app?</Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() => this.recomendBussiness()}
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => this.closeRecommendus()}
          />
        </View>
      </ModelLayout>
    );
  }

  _removeRecomendAlert() {
    return (
      <ModelLayout visible={this.state.removeRecomendAlert}>
        <Text style={{textAlign: 'center'}}>
          Would you like to remove your recommendation?
        </Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() =>
              this.setState({
                removeRecomendAlert: false,
                confirmRecomendAlert: true,
              })
            }
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => this.setState({removeRecomendAlert: false})}
          />
        </View>
      </ModelLayout>
    );
  }

  _confirmRemoveRecomendAlert() {
    return (
      <ModelLayout visible={this.state.confirmRecomendAlert}>
        <Text style={{textAlign: 'center'}}>
          Are you sure you wish to remove your recommendation?
        </Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() => this.removeRecomendBussiness()}
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => this.setState({confirmRecomendAlert: false})}
          />
        </View>
      </ModelLayout>
    );
  }

  _removeFavAlert() {
    return (
      <ModelLayout visible={this.state.askRemoveFav}>
        <Text style={{textAlign: 'center'}}>
          Would you like to remove us from your favourites?
        </Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() =>
              this.setState({askRemoveFav: false, confirmRemoveFav: true})
            }
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => this.setState({askRemoveFav: false})}
          />
        </View>
      </ModelLayout>
    );
  }

  _confirmRemoveFavAlert() {
    return (
      <ModelLayout visible={this.state.confirmRemoveFav}>
        <Text style={{textAlign: 'center'}}>
          Are you sure you wish to remove us you’re your favourites?
        </Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() => this.deleteFav()}
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => this.setState({confirmRemoveFav: false})}
          />
        </View>
      </ModelLayout>
    );
  }

  _ChangeRecomendAlert() {
    return (
      <ModelLayout visible={this.state.changeRecomendAlert}>
        <Text style={{textAlign: 'center'}}>
          You recommended us on the{' '}
          {this.state.lastRecomended
            ? this.momentFormateDate(this.state.lastRecomended.created_at)
            : ''}
        </Text>
        <Text style={{textAlign: 'center'}}>Change your recommendation?</Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() => {
              this.setState({
                changeRecomendAlert: false,
                confirmChangeRecomendAlert: true,
              });
              this.changeTitle('Recommended ?');
            }}
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => {
              this.setState({changeRecomendAlert: false});
              this.changeTitle('no');
            }}
          />
        </View>
      </ModelLayout>
    );
  }

  _ConfirmchangeRecomendAlert() {
    return (
      <ModelLayout visible={this.state.confirmChangeRecomendAlert}>
        <Text style={{textAlign: 'center'}}>
          Are you sure you want to change your recommendation?
        </Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() => this.removeRecomendBussiness()}
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => {
              this.setState({confirmChangeRecomendAlert: false});
              this.changeTitle('no');
            }}
          />
        </View>
      </ModelLayout>
    );
  }

  _ChangeFavAlert() {
    return (
      <ModelLayout visible={this.state.changeFavAlert}>
        <Text style={{textAlign: 'center'}}>
          You’ve already added us to your favorites’ on:{' '}
          {this.state.favData
            ? this.momentFormateDate(this.state.favData.created_at)
            : ''}
        </Text>
        <Text style={{textAlign: 'center'}}>
          Would you like to remove us from your favourites ?
        </Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() =>
              this.setState({
                changeFavAlert: false,
                confirmChangeFavAlert: true,
              })
            }
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => {
              this.setState({changeFavAlert: false});
              this.changeTitle('no');
            }}
          />
        </View>
      </ModelLayout>
    );
  }

  _ConfirmchangeFavAlert() {
    return (
      <ModelLayout visible={this.state.confirmChangeFavAlert}>
        <Text style={{textAlign: 'center'}}>
          Are you sure you would like to remove us from your favourites?
        </Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() => this.deleteFav()}
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => {
              this.setState({confirmChangeFavAlert: false});
              this.changeTitle('no');
            }}
          />
        </View>
      </ModelLayout>
    );
  }

  _ChangeStarAlert() {
    return (
      <ModelLayout visible={this.state.changeStarAlert}>
        <Text style={{textAlign: 'center'}}>
          You’ve star rated us on:{' '}
          {this.state.currentStarRating
            ? this.momentFormateDate(this.state.currentStarRating.created_at)
            : ''}
        </Text>
        <Text style={{textAlign: 'center'}}>
          Would you like to change your star ratings?
        </Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() =>
              this.setState({
                changeStarAlert: false,
                confirmChangeStarAlert: true,
              })
            }
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => {
              this.setState({changeStarAlert: false});
              this.changeTitle('no');
            }}
          />
        </View>
      </ModelLayout>
    );
  }

  _ConfirmchangeStarAlert() {
    return (
      <ModelLayout visible={this.state.confirmChangeStarAlert}>
        <Text style={{textAlign: 'center'}}>
          Are you sure you would like to change your star ratings?
        </Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Button
            type="textButton"
            text="Yes"
            onPress={() => {
              this.setState({confirmChangeStarAlert: false});
              this._OpenStarRating();
            }}
          />
          <Button
            type="textButton"
            text="No"
            onPress={() => {
              this.setState({confirmChangeStarAlert: false});
              this.changeTitle('no');
            }}
          />
        </View>
      </ModelLayout>
    );
  }

  _ShowFavRemovedAlert() {
    return (
      <ModelLayout visible={this.state.showFavRemovedAlert}>
        <Text style={{textAlign: 'center'}}>Removed from favourites</Text>
        <View
          style={{
            width: 150,
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'center',
          }}>
          <Button
            type="textButton"
            text="OK"
            onPress={() => {
              this.setState({showFavRemovedAlert: false});
              this._OpenStarRating();
            }}
          />
        </View>
      </ModelLayout>
    );
  }

  _recommendSentAlert() {
    return (
      <ModelLayout visible={this.state.recommendSentAlert}>
        <View>
          <Icon
            style={{alignSelf: 'center'}}
            name="thumbs-up"
            size={40}
            color={Colors.primary}
          />
        </View>
        <Text style={{textAlign: 'center', marginTop: 15}}>
          Recommendation sent
        </Text>
      </ModelLayout>
    );
  }
  /* #endregion */

  /* #region  All async function */
  async recomendBussiness() {
    this.closeRecommendus();
    var result = await getWithLogin('do-recomend/' + this.state.company.id);
    console.log(result);
    this.setState({
      lastRecomended: result.data,
      recommendSentAlert: true,
      lastRecommendationDate: result.data,
      showRecoTick: true,
    });
    this.setState({totalRecommendation: this.state.totalRecommendation + 1});

    setTimeout(() => {
      this.setState({recommendSentAlert: false});
      this.openMiniReport();
    }, 3000);
    this.doneThings('recommend');
  }

  async getLog() {
    const response = await getWithLogin(
      'user/company/log/' + this.state.company.id,
    );
    console.log(response.data);
    if (response.data.length > 0) {
      this.setState({lastLog: response.data[0]});
    }
  }

  async doneThings(type) {
    var response = await getWithLogin('user/state/' + type);
  }

  async removeRecomendBussiness() {
    this.changeTitle('no');
    this.setState({
      confirmRecomendAlert: false,
      confirmChangeRecomendAlert: false,
    });
    try {
      var result = await deleteWithLogin(
        'delete/recomneded/' + this.state.lastRecomended.id,
      );
      console.log(result);
      this.setState({lastRecomended: result.data, showRecoTick: true});
      this.setState({totalRecommendation: this.state.totalRecommendation - 1});
      this.openMiniReport();
    } catch (error) {
      console.log(error);
      console.log(error.response.data);
    }
  }

  async getCompany(comeback) {
    this.setState({
      isLoading: true,
      defaultOpen: this.props.route.params.defaultOpen,
    });
    var data = await getWithLogin('company/add/' + this.state.place_id);
    console.log("company response",data);
    this.setState(
      {
        company: data.data,
        lastLog: data.data.lastLog,
        lastRecomended: data.data.lastRecomendation,
        totalRecommendation: data.data.totalRecommendation,
        lastRecommendationDate: data.data.lastRecommendationDate,
        favData: data.data.fav,
        userNote: data.data.note,
        isMessage: data.data.isMessage,
        isExsiting: data.data.isExisiting,
        promotion: data.data.promotion,
        isLoading: false,
        feedback: data.data.feedback ? 'Done' : undefined,
      },
      () => {
        if (
          this.state.isExsiting == null ||
          this.state.isExsiting == undefined
        ) {
          this.setState({showExperiance: true});
        }
        if (this.state.company.starRating) {
          this.setState(
            {
              starCount1: this.state.company.starRating.customer_service,
              starCount2: this.state.company.starRating.quality_of_product,
              starCount3: this.state.company.starRating.value_for_money,
              estarCount1: this.state.company.starRating.customer_service,
              estarCount2: this.state.company.starRating.quality_of_product,
              estarCount3: this.state.company.starRating.value_for_money,
              currentStarRating: this.state.company.starRating,
            },
            () => {
              if (
                this.state.starCount1 > 0 ||
                this.state.starCount2 > 0 ||
                this.state.starCount3 > 0
              ) {
                this.setState({showRating: true});
              }
            },
          );
        } else {
          this.setState({
            starCount1: 0,
            starCount2: 0,
            starCount3: 0,
            showRating: false,
          });
        }
        if (
          this.state.isExsiting == null ||
          this.state.isExsiting == undefined
        ) {
          if (!comeback) {
            setTimeout(() => {
              this.handleDefaultOpen();
            }, 3000);
          }
        } else {
          if (!comeback) {
            setTimeout(() => {
              this.handleDefaultOpen();
            }, 1000);
          }
        }
      },
    );
  }

  async deleteFav() {
    this.changeTitle('no');
    this.setState({is_fav: 0, confirmChangeFavAlert: false});
    var response = await deleteWithLogin('delete/fav/' + this.state.favData.id);
    this.setState({favData: response.data, confirmRemoveFav: false});
    if (this.state.defaultOpen === 'favorite_search') {
      this.setState({showFavRemovedAlert: true});
      setTimeout(() => {
        this.setState({showFavRemovedAlert: false});
      }, 3000);
    }
  }

  async addFav() {
    // this.setState({ is_fav: 1 });
    this.changeTitle('Added to your favourites');
    var respose = await postWithLogin('add/fav', {
      com_id: this.state.company.id,
    });
    console.log(respose.data);
    this.setState({favData: respose.data});
    var response = await getWithLogin('user/state/' + 'favorite');
  }
  /* #endregion */

  /* #region  open sheet funtion */
  openMiniReport() {
    reportOpen = true;
    this.closeHeaderButton();
    this.changeTitle('Mini report');
    this.MiniReportSheet.open();
  }

  openUserNote() {
    userNoteSheet = true;
    this.closeHeaderButton();
    if (this.state.userNote) {
      this.changeTitle('Add Note');
    } else {
      this.changeTitle('Add profile notes');
    }
    this.UserNoteSheet.open();
  }

  openStarHistory() {
    starHistoryOpen = true;
    this.changeTitle('My star ratings');
    this.StarHistorySheet.open();
    this.closeHeaderButton();
  }

  openActivitySheet() {
    activityOpen = true;
    this.closeHeaderButton();
    this.ActivitySheet.open();
    this.changeTitle('My activities');
  }

  openOptionSheet() {
    optionOpen = true;
    this.closeHeaderButton();
    this.OptionSheet.open();
  }

  _OpenStarRating() {
    starRatingOpen = true;
    this.changeTitle('Star rate us');
    this.StarSheet.open();
    this.setState({changesMade: false});
    this.closeHeaderButton();
  }

  openFeedSheet() {
    feedbackOpen = true;
    this.FeedSheet.open();
    this.doneThings('feedback');
    this.changeTitle('Send us feedback');
    this.closeHeaderButton();
  }

  openFeedBackReview() {
    this.props.navigation.navigate('FeedbackReview', {
      company: this.state.company,
      feedback: this.state.feedback,
    });
  }

  openRecommendHistory() {
    recommendHistory = true;
    this.RecommendHistory.open();
    this.closeHeaderButton();
    this.changeTitle('Recommendations');
  }

  openFavouriteSheet() {
    favouriteFormSheet = true;
    this.FavouriteSheet.open();
    if (this.state.favData) {
      this.changeTitle('Add contact details');
    } else {
      this.changeTitle('Add us to favourites');
    }
    this.doneThings('favorite');
    this.closeHeaderButton();
  }

  openMyFavouriteSheet() {
    myFavouriteFormSheet = true;
    this.MyFavouriteSheet.open();
    this.closeHeaderButton();
  }
  openRecommendUs() {
    this.changeTitle('Recommend us');
    this.setState({askRecomendAlert: true});
  }

  openFavNote() {
    favNoteSheet = true;
    this.MyFavouriteNoteSheet.open();
    this.closeHeaderButton();
    // if (this.state.favData.notes) {
    //     this.changeTitle("Favourite notes");
    // } else {
    //     this.changeTitle("Add favourite notes");
    // }
  }

  /* #endregion */

  /* #region  Close sheet function */
  closeUserNote() {
    userNoteSheet = false;
    this.barHeaderButton();
    this.changeTitle('no');
  }

  closeMiniReport() {
    reportOpen = false;
    this.barHeaderButton();
    this.setState({showRecoTick: false});
    this.changeTitle('no');
  }

  closeStarHistory() {
    starHistoryOpen = false;
    this.changeTitle('');
    this.barHeaderButton();
  }

  closeActivitySheet() {
    activityOpen = false;
    this.changeTitle('no');
    this.barHeaderButton();
  }

  closeOptionMenuSheet() {
    optionOpen = false;
    this.barHeaderButton();
  }

  _CloseStarRating() {
    starRatingOpen = false;
    if (this.state.showRating == false || !this.state.changesMade) {
      this.changeTitle('');
      console.log('change made false');
      this.setState({
        estarCount1: this.state.starCount1,
        estarCount2: this.state.starCount2,
        estarCount3: this.state.starCount3,
      });
    } else {
      if (this.state.changesMade) {
        console.log('change were made');
        this.setState({
          starCount1: this.state.estarCount1,
          starCount2: this.state.estarCount2,
          starCount3: this.state.estarCount3,
        });
        this.openMiniReport();
      }
    }
    this.barHeaderButton();
  }

  closeFeedSheet() {
    feedbackOpen = false;
    this.changeTitle('');
    this.barHeaderButton();
  }

  closeFeedBackReview() {
    feedbackReviewOpen = false;
    this.barHeaderButton();
    this.changeTitle('');
  }

  closeRecommendHiostory() {
    recommendHistory = false;
    this.barHeaderButton();
    this.changeTitle('');
  }

  closeFavouriteSheet() {
    favouriteFormSheet = false;
    this.barHeaderButton();
    this.changeTitle('');
  }

  closeMyFavouriteSheet() {
    myFavouriteFormSheet = false;
    this.barHeaderButton();
  }

  closeRecommendus() {
    this.changeTitle('profile');
    this.setState({askRecomendAlert: false});
  }

  closeFavNote() {
    favNoteSheet = false;
    // this.changeTitle("profile");
    this.barHeaderButton();
  }

  /* #endregion */

  /* #region  All bottom sheet */

  activitySheet() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.ActivitySheet = ref;
        }}
        isFull={false}
        height={this.state.bottomSheetHeight}
        onClose={() => this.closeActivitySheet()}>
        <MyActivitiesSlider
          company={this.state.company}
          updateFeedback={this.updateFeedback.bind(this)}
          navigation={this.props.navigation}
          close={() => this.ActivitySheet.close()}
        />
      </BottomPopUp>
    );
  }

  feedSheet() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.FeedSheet = ref;
        }}
        isFull={true}
        height={windowHeight}
        onClose={() => this.closeFeedSheet()}>
        <SubmitFeedbackScreen
          company={this.state.company}
          updateFeedback={this.updateFeedback.bind(this)}
          navigation={this.props.navigation}
          close={() => this.FeedSheet.close()}
        />
      </BottomPopUp>
    );
  }

  starHistorySheet() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.StarHistorySheet = ref;
        }}
        isFull={false}
        height={this.state.bottomSheetHeight}
        onClose={() => this.closeStarHistory()}>
        <MyStarRatingHistory company={this.state.company} />
      </BottomPopUp>
    );
  }

  miniReport() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.MiniReportSheet = ref;
        }}
        isFull={false}
        height={this.state.bottomSheetHeight}
        onClose={() => this.closeMiniReport()}>
        <MiniReportSlider
          company={this.state.company}
          star1={this.state.starCount1}
          close={() => this.MiniReportSheet.close()}
          star2={this.state.starCount2}
          star3={this.state.starCount3}
          userNote={this.state.userNote}
          openUserNote={this.openUserNote.bind(this)}
          openFeedSheet={this.openFeedSheet.bind(this)}
          openFeedBackReview={this.openFeedBackReview.bind(this)}
          currentStarRating={this.state.currentStarRating}
          lastLog={this.state.lastLog}
          lastRecommendation={this.state.lastRecomended}
          feedback={this.state.feedback}
          favData={this.state.favData}
          AddFav={this.addFav.bind(this)}
          viewFavourites={this.viewFavourites.bind(this)}
          company={this.state.company}
          openRecommend={() => this.openRecommendUs()}
          showTick={this.state.showRecoTick}
          openUserNote={this.openUserNote.bind(this)}
          openActivitySheet={this.openActivitySheet.bind(this)}
          openStarHistory={this.openStarHistory.bind(this)}
          openStarSheet={this._OpenStarRating.bind(this)}
        />
      </BottomPopUp>
    );
  }

  optionitemCilckEvent(item) {
    switch (item) {
      case 'recommend':
        if (this.checkLastRecomend()) {
          this.setState({removeRecomendAlert: true});
        } else {
          this.openRecommendUs();
        }
        break;
      case 'star':
        setTimeout(() => {
          this._OpenStarRating();
        }, 1000);
        break;
      case 'activities':
        setTimeout(() => {
          this.openActivitySheet();
        }, 1000);
        break;
      case 'feedback':
        if (this.state.feedback) {
          setTimeout(() => {
            this.openFeedBackReview();
          }, 1000);
        } else {
          setTimeout(() => {
            this.openFeedSheet();
          }, 1000);
        }
        break;
      case 'note':
        setTimeout(() => {
          this.openUserNote();
        }, 1000);
        break;
      case 'favourites':
        if (this.state.favData && this.state.favData.is_delete == 0) {
          this.setState({askRemoveFav: true});
        } else {
          this.addFav();
        }
        break;
      default:
        break;
    }
    optionOpen = false;
    this.OptionSheet.close();
  }

  optionSheet() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.OptionSheet = ref;
        }}
        isFull={false}
        height={this.state.bottomSheetHeight}
        onClose={() => this.closeOptionMenuSheet()}>
        <ScrollView
          style={{
            backgroundColor: Colors.white,
            paddingLeft: 10,
            paddingRight: 10,
          }}
          keyboardShouldPersistTaps={'handled'}>
          <OptionItem
            text={
              this.checkLastRecomend()
                ? 'Remove your recommendation'
                : 'Recommend us'
            }
            onPress={() => this.optionitemCilckEvent('recommend')}
          />
          <OptionItem
            text={
              this.state.showRating
                ? 'Edit / delete my star ratings'
                : 'Star rate us'
            }
            onPress={() => this.optionitemCilckEvent('star')}
          />
          <OptionItem
            text={
              this.state.feedback ? 'My feedback' : 'Submit a review / comments'
            }
            onPress={() => this.optionitemCilckEvent('feedback')}
          />
          <OptionItem
            onPress={() => this.optionitemCilckEvent('favourites')}
            text={
              this.state.favData && this.state.favData.is_delete == 0
                ? 'Remove favourites'
                : 'Add to favourites'
            }
          />
          <OptionItem text="Send us a message" />
          <OptionItem text="Give us a call" />
          <OptionItem
            text="View my activities"
            onPress={() => this.optionitemCilckEvent('activities')}
          />
          <OptionItem
            text={this.state.userNote ? 'View notes' : 'Add notes'}
            onPress={() => this.optionitemCilckEvent('note')}
          />
          <OptionItem text="Yov" />
        </ScrollView>
      </BottomPopUp>
    );
  }

  starSheet() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.StarSheet = ref;
        }}
        isFull={false}
        height={this.state.bottomSheetHeight}
        onClose={() => this._CloseStarRating()}>
        <StarRatingScreen
          changeTitle={this.changeTitle.bind(this)}
          company={this.state.company}
          star1={this.state.starCount1}
          star2={this.state.starCount2}
          star3={this.state.starCount3}
          currentStarRating={this.state.currentStarRating}
          openMiniReport={this.openMiniReport.bind(this)}
          updateCurrentStarRating={this.updateCurrentStarRating.bind(this)}
          unDoneStar={this.unDoneStar.bind(this)}
          close={() => this.StarSheet.close()}
          doneStar={this.doneStar.bind(this)}
          changesMade={this.changesMade.bind(this)}
          manageStarRating={this.manageStarRating.bind(this)}
        />
      </BottomPopUp>
    );
  }

  feedbackReviewSheet() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.FeedBackReview = ref;
        }}
        isFull={true}
        height={this.state.bottomSheetHeight}
        onClose={() => this.closeFeedBackReview()}>
        <FeedbackReviewScreen
          company={this.state.company}
          feedback={this.state.feedback}
          navigation={this.props.navigation}
          close={() => this.ActivitySheet.close()}
        />
      </BottomPopUp>
    );
  }

  recommendHistorySheet() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.RecommendHistory = ref;
        }}
        isFull={false}
        height={this.state.bottomSheetHeight}
        onClose={() => this.closeRecommendHiostory()}>
        <RecommendationHistory
          company={this.state.company}
          close={() => this.ActivitySheet.close()}
        />
      </BottomPopUp>
    );
  }

  favouriteSheet() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.FavouriteSheet = ref;
        }}
        isFull={true}
        height={this.state.bottomSheetHeight}
        onClose={() => this.closeFavouriteSheet()}>
        <FavouriteScreen
          favData={this.state.favData}
          updateFavData={this.updateFavData.bind(this)}
          company={this.state.company}
          close={() => this.FavouriteSheet.close()}
        />
      </BottomPopUp>
    );
  }

  updateFavData(data) {
    this.setState({favData: data});
    this.viewFavourites();
  }

  myFavouriteSheet() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.MyFavouriteSheet = ref;
        }}
        isFull={false}
        height={this.state.bottomSheetHeight}
        onClose={() => this.closeMyFavouriteSheet()}>
        <MyFavouriteScreen
          company={this.state.company}
          delete={this.state.isFavourite}
          close={() => this.ActivitySheet.close()}
        />
      </BottomPopUp>
    );
  }

  myFavouriteNoteSheet() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.MyFavouriteNoteSheet = ref;
        }}
        height={windowHeight}
        isFull={true}
        onClose={() => this.closeFavNote()}>
        <FavoriteNoteScreen
          navigation={this.props.navigation}
          changeTitle={this.changeTitle.bind(this)}
          favData={this.state.favData}
          updateFavData={this.updateFavData.bind(this)}
          company={this.state.company}
          close={() => this.MyFavouriteNoteSheet.close()}
        />
      </BottomPopUp>
    );
  }

  userNoteSheet() {
    return (
      <BottomPopUp
        Ref={(ref) => {
          this.UserNoteSheet = ref;
        }}
        height={windowHeight}
        isFull={true}
        onClose={() => this.closeUserNote()}>
        <ProfileNoteScreen
          navigation={this.props.navigation}
          changeTitle={this.changeTitle.bind(this)}
          updateUserNote={this.updateUserNote.bind(this)}
          userNote={this.state.userNote}
          company={this.state.company}
          close={() => this.UserNoteSheet.close()}
        />
      </BottomPopUp>
    );
  }

  /* #endregion */

  updateFeedback(data) {
    this.setState({feedback: data});
  }

  updateUserNote(data) {
    this.setState({userNote: data});
  }

  _onCloseOptionStarRating() {
    this.barHeaderButton();
  }

  updateLastLog(data) {
    this.setState({lastLog: data});
  }

  updateCurrentStarRating(data) {
    this.setState({currentStarRating: data});
  }

  deleteStarRating() {
    this.setState({
      company: {
        ...this.state.company.starRating,
        customer_service: 0,
        quality_of_product: 0,
        value_for_money: 0,
      },
    });
  }

  changesMade() {
    this.setState({
      changesMade: true,
    });
  }

  doneStar() {
    this.setState({
      showRating: true,
      starCount1: this.state.estarCount1,
      starCount2: this.state.estarCount2,
      starCount3: this.state.estarCount3,
    });
  }

  unDoneStar() {
    this.setState({
      showRating: false,
      starCount1: 0,
      starCount2: 0,
      starCount3: 0,
    });
  }

  manageStarRating(order, rating) {
    switch (order) {
      case 1:
        this.setState({estarCount1: rating});
        break;
      case 2:
        this.setState({estarCount2: rating});
        break;
      case 3:
        this.setState({estarCount3: rating});
        break;
    }
  }

  submitReview() {
    this.props.navigation.navigate('FeedbackReview');
  }

  // Handle default opening of bottom sheets
  handleDefaultOpen() {
    console.log(this.state.defaultOpen);
    if (this.state.defaultOpen === 'star_rating_search') {
      if (this.state.showRating) {
        this.changeTitle('Already star rated by you');
        this.setState({changeStarAlert: true});
      } else {
        this._OpenStarRating();
      }
    } else if (this.state.defaultOpen === 'starHistory') {
      this.openStarHistory();
    } else if (this.state.defaultOpen === 'view_favourite') {
      this.viewFavourites();
    } else if (this.state.defaultOpen === 'view_favourite_note') {
      this.changeTitle('Favourite notes');
      this.openFavNote();
    } else if (this.state.defaultOpen === 'view_profile_note') {
      this.openUserNote();
    } else if (this.state.defaultOpen === 'view_feedback') {
      this.openFeedBackReview();
    } else if (this.state.defaultOpen === 'view_activities') {
      this.openActivitySheet();
    } else if (this.state.defaultOpen === 'view_recommeded') {
      this.openRecommendHistory();
    } else if (this.state.defaultOpen === 'recommend_search') {
      if (
        !this.state.lastRecomended ||
        (this.state.lastRecomended && this.state.lastRecomended.is_delete == 1)
      ) {
        this.openRecommendUs();
      } else {
        this.changeTitle('Already recommended by you');
        this.setState({changeRecomendAlert: true});
      }
    } else if (this.state.defaultOpen === 'feedback_search') {
      this.openFeedSheet();
    } else if (this.state.defaultOpen === 'favorite_search') {
      if (this.state.favData && this.state.favData.is_delete == 0) {
        this.changeTitle('Already added to favourites');
        this.setState({changeFavAlert: true});
      } else {
        this.changeTitle('Add us to favourites');
        this.addFav();
      }
    } else if (this.state.defaultOpen === 'profile_note_search') {
      this.openUserNote();
    }
     else if (this.state.defaultOpen === 'messages_search') {
      this.openMessageScreen();
    }
  }

  _gotoHistory() {
    this._OpenStarRating();
  }

  _resetStartRating() {
    this.setState({showRating: false});
  }

  goNone(route) {}

  changeTitle(sheet) {
    this.setState({subtitle: changeProfileTitle(sheet)});
  }

  barHeaderButton() {
    this.getLog();
    if (
      activityOpen ||
      starHistoryOpen ||
      reportOpen ||
      optionOpen ||
      starRatingOpen ||
      feedbackReviewOpen ||
      recommendHistory ||
      favouriteFormSheet ||
      myFavouriteFormSheet ||
      favNoteSheet ||
      userNoteSheet
    ) {
    } else {
      this.setState({sheetOpen: false});
      this.props.navigation.setOptions({
        headerRight: () => (
          <TouchableOpacity
            hitSlop={this.slop}
            style={{marginRight: 10}}
            onPress={() => this.openOptionSheet()}>
            <Image source={require('../assets/img/bar.png')}></Image>
          </TouchableOpacity>
        ),
      });
    }
  }

  closeHeaderButton() {
    this.getLog();
    this.setState({sheetOpen: true});
    this.props.navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity style={{marginRight: 5}}>
          <Button type="header" text="Close" />
        </TouchableOpacity>
      ),
    });
  }

  checkLastRecomend() {
    if (this.state.lastRecomended && this.state.lastRecomended.is_delete == 0) {
      return true;
    } else {
      return false;
    }
  }

  _ratingSection() {
    return (
      <Pressable
        style={{flex: 1}}
        onPress={() => (this.state.showRating ? {} : this._OpenStarRating())}
        onPressIn={() => VibrateStart()}
        onPressOut={() => VibrateStop()}>
        {({pressed}) => (
          <View
            style={[
              pressed ? styles.ratingContainerActive : styles.ratingContainer,
            ]}>
            <Text
              style={[
                styles.transparentTextStyle,
                {
                  color: pressed
                    ? Colors.white
                    : this.state.showRating
                    ? Colors.primary
                    : Colors.deepPurple,
                },
              ]}>
              {this.state.showRating ? 'Star rated' : 'Star rate us'}
            </Text>
            <View
              style={{
                flex: 1,
                height: 100,
                justifyContent: 'space-between',
                marginTop: 5,
              }}>
              <View style={styles.starRatingContainer}>
                <View style={{flex: 1}}>
                  <Text
                    style={pressed ? styles.starLabelActive : styles.starLabel}>
                    Customer services
                  </Text>
                </View>
                <View style={styles.starContainer}>
                  <StarRate
                    disabled={true}
                    starSize={20}
                    buttonStyle={{marginLeft: 3}}
                    rating={this.state.showRating ? this.state.starCount1 : 0}
                  />
                </View>
              </View>
              <View style={styles.starRatingContainer}>
                <View style={{flex: 1}}>
                  <Text
                    style={pressed ? styles.starLabelActive : styles.starLabel}>
                    Product(s)/ service
                  </Text>
                </View>
                <View style={styles.starContainer}>
                  <StarRate
                    disabled={true}
                    starSize={20}
                    buttonStyle={{marginLeft: 3}}
                    rating={this.state.showRating ? this.state.starCount2 : 0}
                  />
                </View>
              </View>
              <View style={styles.starRatingContainer}>
                <View style={{flex: 1}}>
                  <Text
                    style={pressed ? styles.starLabelActive : styles.starLabel}>
                    Value for money
                  </Text>
                </View>
                <View style={styles.starContainer}>
                  <StarRate
                    disabled={true}
                    starSize={20}
                    buttonStyle={{marginLeft: 3}}
                    rating={this.state.showRating ? this.state.starCount3 : 0}
                  />
                </View>
              </View>
            </View>
          </View>
        )}
      </Pressable>
    );
  }

  openMessageScreen() {
    this.props.navigation.navigate('Message', {company: this.state.company});
  }

  getLastRecommendDate() {
    if (this.state.totalRecommendation == 0) {
      return 'Recommend us';
    } else {
      if (this.state.lastRecommendationDate) {
        return (
          'Last recommend: ' +
          this.momentFormateDate(this.state.lastRecommendationDate.created_at)
        );
      } else {
        return 'Recommend us';
      }
    }
  }

  checkName() {
    var name = '';
    if (this.state.favData) {
      if (
        this.state.favData.name_title != '' &&
        this.state.favData.name_title != null
      ) {
        if (
          this.state.favData.first_name != null &&
          this.state.favData.surname != null
        ) {
          name = name + ' ' + this.state.favData.name_title;
        }
      }
      if (
        this.state.favData.first_name != '' &&
        this.state.favData.first_name != null
      ) {
        name = name + ' ' + this.state.favData.first_name;
      }
      if (
        this.state.favData.surname != '' &&
        this.state.favData.surname != null
      ) {
        name = name + ' ' + this.state.favData.surname;
      }
    }
    if (name != '') {
      return name;
    } else {
      return 'Contact name';
    }
  }

  viewFavourites() {
    this.setState({is_fav: 1});
  }

  openFavourit() {
    this.doneThings('favorite');
    this.setState({isFavourite: true});
    this.viewFavourites();
  }

  callFromFavorite() {
    if (this.state.favData && this.state.favData != null) {
      if (this.state.favData.mobile) {
        callNumber(this.state.favData.mobile);
      } else if (this.state.favData.landline) {
        callNumber(this.state.favData.landline);
      } else {
        callNumber(this.state.company.formatted_phone_number);
      }
    } else {
      callNumber(this.state.company.formatted_phone_number);
    }
  }

  doEmail() {
    if (this.state.favData && this.state.favData != null) {
      if (this.state.favData.email) {
        doMail(this.state.favData.email);
      } else {
        doMail(this.state.company.email);
      }
    } else {
      callNumber(this.state.company.email);
    }
  }

  _renderFavouriteScreen() {
    if (this.state.is_fav == 0) {
      return (
        <View
          style={{
            backgroundColor: Colors.lightGrey2,
            paddingLeft: 10,
            paddingRight: 10,
            paddingTop: 2,
            paddingBottom: 2,
          }}>
          <View
            style={{
              marginBottom: 4,
              marginTop: 2,
              flexDirection: 'row',
              flex: 1,
            }}>
            <View style={{flex: 1, paddingRight: 4}}>
              <Button
                type="profileButton"
                isActive={true}
                onPress={() => this.openMiniReport()}
                textColor={Colors.deepPurple}
                text="Mini report"
              />
            </View>
            <View style={{flex: 1, paddingLeft: 4}}>
              <Button
                type="profileButton"
                onPress={() => this.openUserNote()}
                isActive={true}
                textColor={
                  this.state.userNote ? Colors.white : Colors.deepPurple
                }
                backgroundColor={
                  this.state.userNote ? Colors.primary : Colors.white
                }
                text={this.state.userNote ? 'View notes' : 'Add notes'}
              />
            </View>
          </View>
          <View style={{marginBottom: 4, flexDirection: 'row', flex: 1}}>
            <View style={{flex: 1, paddingRight: 4}}>
              <Button
                type="profileButton"
                isActive={true}
                textColor={Colors.deepPurple}
                onPress={() =>
                  this.state.lastRecomended
                    ? this.state.lastRecomended.is_delete == 1
                      ? this.openRecommendUs()
                      : this.openRecommendHistory()
                    : this.openRecommendUs()
                }
                text={
                  this.state.lastRecomended &&
                  this.state.lastRecomended.is_delete == 0
                    ? 'My recommendations'
                    : 'Recommend us'
                }
              />
            </View>
            <View style={{flex: 1, paddingLeft: 4}}>
              <Button
                type="profileButton"
                isActive={true}
                textColor={Colors.deepPurple}
                text="My phone calls"
                onPress={() =>
                  callNumber(this.state.company.formatted_phone_number)
                }
              />
            </View>
          </View>
          <View style={{marginBottom: 4, flexDirection: 'row', flex: 1}}>
            <View style={{flex: 1, paddingRight: 4}}>
              <Button
                type="profileButton"
                isActive={true}
                onPress={() =>
                  this.state.currentStarRating
                    ? this.openStarHistory()
                    : this._OpenStarRating()
                }
                textColor={Colors.deepPurple}
                text={
                  this.state.currentStarRating
                    ? 'My star ratings'
                    : 'Star rate us'
                }
              />
            </View>
            <View style={{flex: 1, paddingLeft: 4}}>
              <Button
                type="profileButton"
                isActive={true}
                textColor={Colors.deepPurple}
                text="Send us a message "
              />
            </View>
          </View>
          <View style={{marginBottom: 4, flexDirection: 'row', flex: 1}}>
            <View style={{flex: 1, paddingRight: 4}}>
              <Button
                onPress={
                  this.state.feedback
                    ? () => this.openFeedBackReview()
                    : () => this.openFeedSheet()
                }
                type="profileButton"
                isActive={true}
                textColor={Colors.deepPurple}
                text={this.state.feedback ? 'My feedback' : 'Send us feedback'}
              />
            </View>
            <View style={{flex: 1, paddingLeft: 4}}>
              <Button
                onPress={() =>
                  this.state.favData && this.state.favData.is_delete == 0
                    ? this.viewFavourites()
                    : this.addFav()
                }
                type="profileButton"
                isActive={true}
                textColor={Colors.deepPurple}
                textColor={
                  this.state.favData && this.state.favData.is_delete == 0
                    ? Colors.white
                    : Colors.deepPurple
                }
                backgroundColor={
                  this.state.favData && this.state.favData.is_delete == 0
                    ? Colors.primary
                    : Colors.white
                }
                text={
                  this.state.favData && this.state.favData.is_delete == 0
                    ? 'View in favourites'
                    : 'Add us to favourites'
                }
              />
            </View>
          </View>
          <View style={{marginBottom: 4, flexDirection: 'row', flex: 1}}>
            <View style={{flex: 1, paddingRight: 4}}>
              <Button
                type="profileButton"
                onPress={() => this.openActivitySheet()}
                isActive={true}
                textColor={Colors.deepPurple}
                text="My activities"
              />
            </View>
            <View style={{flex: 1, paddingLeft: 4}}>
              <Button
                type="profileButton"
                isActive={this.state.promotion != null ? true : false}
                onPress={() =>
                  this.props.navigation.navigate('PromotionDetails', {
                    id: this.state.promotion.id,
                  })
                }
                textColor={Colors.deepPurple}
                text="Promotion"
              />
            </View>
          </View>
        </View>
      );
    } else if (this.state.is_fav == 1) {
      return (
        <View style={{paddingLeft: 10, paddingRight: 10}}>
          <View style={[styles.favButton, {maxHeight: 80}]}>
            <Text
              numberOfLines={1}
              style={{marginTop: 10, textAlign: 'center', fontSize: 21}}>
              {this.state.favData && this.state.favData.is_delete == 0
                ? this.checkName()
                : 'Contact name'}
            </Text>
            <Text
              numberOfLines={1}
              style={{marginTop: 5, marginBottom: 10, textAlign: 'center'}}>
              {this.state.favData && this.state.favData.is_delete == 0
                ? this.state.favData.job_title != '' &&
                  this.state.favData.job_title != null
                  ? this.state.favData.job_title
                  : 'Job title'
                : 'Job title'}
            </Text>
          </View>
          <View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
            <View style={styles.favLeftPart}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Button
                  style={{marginLeft: 0, width: '30%'}}
                  type="footerButtonNoicon"
                  iconName="phone"
                  onPress={() => this.callFromFavorite()}
                />
                <Button
                  style={{marginLeft: 0, width: '30%'}}
                  type="footerButtonNoicon"
                  iconName="envelope"
                  onPress={() => this.doEmail()}
                />
                <Button
                  style={{marginLeft: 0, width: '30%'}}
                  type="footerButtonNoicon"
                  iconName="comments"
                  onPress={() => this.openMessageScreen()}
                />
              </View>
            </View>
            <View style={styles.favRightPart}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <Pressable
                  style={({pressed}) => [
                    pressed ? styles.favButtonActive : styles.favButton,
                    {backgroundColor: Colors.white, maxHeight: 50},
                  ]}
                  onPress={() =>
                    this.setState({is_fav: 0, subtitle: 'Profile'})
                  }
                  onPressIn={() => VibrateStart()}
                  onPressOut={() => VibrateStop()}>
                  <Text
                    style={{
                      color: Colors.deepPurple,
                      textAlign: 'center',
                      textTransform: 'capitalize',
                      fontSize: FontSize.heading,
                    }}>
                    Profile View
                  </Text>
                </Pressable>
              </View>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              marginTop: 10,
              minHeight: 45,
            }}>
            <View style={styles.favLeftPart}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <Pressable
                  style={({pressed}) => [
                    pressed ? styles.favButtonActive : styles.favButton,
                    {
                      backgroundColor:
                        this.state.favData &&
                        this.state.favData.notes != '' &&
                        this.state.favData.notes != null
                          ? Colors.primary
                          : Colors.white,

                      minHeight: 40,
                    },
                  ]}
                  onPress={() => {
                    this.openFavNote();
                  }}
                  onPressIn={() => VibrateStart()}
                  onPressOut={() => VibrateStop()}>
                  <Text
                    style={{
                      color:
                        this.state.favData &&
                        this.state.favData.notes != '' &&
                        this.state.favData.notes != null
                          ? Colors.white
                          : Colors.deepPurple,
                      textAlign: 'center',
                      textTransform: 'capitalize',
                      fontSize: FontSize.heading,
                    }}>
                    {this.state.favData &&
                    this.state.favData.notes != '' &&
                    this.state.favData.notes != null
                      ? 'View notes'
                      : 'Add Notes'}
                  </Text>
                </Pressable>
              </View>
            </View>
            <View style={styles.favRightPart}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <Pressable
                  style={({pressed}) => [
                    pressed ? styles.favButtonActive : styles.favButton,
                    {backgroundColor: Colors.white, minHeight: 40},
                  ]}
                  onPress={() => {
                    this.openFavouriteSheet();
                  }}
                  onPressIn={() => VibrateStart()}
                  onPressOut={() => VibrateStop()}>
                  <Text
                    style={{
                      color: Colors.deepPurple,
                      textAlign: 'center',
                      textTransform: 'capitalize',
                      fontSize: FontSize.heading,
                    }}>
                    {this.checkName() != 'Contact name'
                      ? 'Edit contact details'
                      : 'Add contact details'}
                  </Text>
                </Pressable>
              </View>
            </View>
          </View>
        </View>
      );
    }
  }

  find_dimesions(layout) {
    this.setState({bottomSheetHeight: windowHeight});
  }

  _renderScreen() {
    if (this.state.isLoading) {
      return (
        <Container style={{justifyContent: 'center'}}>
          <StatusBar backgroundColor="#fff" barStyle="dark-content" />
          <SubHeader text={this.state.subtitle} />
          <Content>
            <Spinner color="green" style={{alignSelf: 'center'}} />
          </Content>
        </Container>
      );
    } else {
      if (this.state.company) {
        return (
          <Container style={{backgroundColor: '#FFF'}}>
            <SubHeader
              onLayout={(event) => {
                this.find_dimesions(event.nativeEvent.layout);
              }}
              text={this.state.subtitle}
            />
            <Header style={styles.headerStyle}>
              <CompanyShortProfile
                onRecoClick={() => this.setState({sheetOpen: false})}
                onStarClick={() => this.setState({sheetOpen: false})}
                onHeaderClick={() => this.setState({sheetOpen: false})}
                icon={this.state.company.icon}
                name={this.state.company.name}
                totalReco={this.state.totalRecommendation}
                onStarClick={() =>
                  setTimeout(() => {
                    this._OpenStarRating(0, 'star');
                  }, 50)
                }
                sheetOpen={this.state.sheetOpen}
                startDone={
                  this.state.lastRecomended
                    ? this.state.lastRecomended.is_delete == 1
                      ? false
                      : true
                    : false
                }
                lastRecomended={this.getLastRecommendDate()}
                onRecoClick={
                  this.state.lastRecomended
                    ? this.state.lastRecomended.is_delete == 1
                      ? () => this.openRecommendUs()
                      : () => {}
                    : () => this.openRecommendUs()
                }
                formatted_address={this.state.company.formatted_address}
                rating={
                  (this.state.estarCount1 +
                    this.state.estarCount2 +
                    this.state.estarCount3) /
                  3
                }
              />
              {/* All Buttons */}
              <View style={styles.iconContainer}>
                <TouchableOpacity
                  hitSlop={this.slop}
                  style={[styles.buttonStyle]}
                  onPress={() => this.goNone('yov')}>
                  <Text
                    style={[
                      styles.iconTextStyle,
                      {
                        color: this.state.yov
                          ? Colors.primary
                          : Colors.lightGrey,
                      },
                      {opacity: this.state.isYovHide ? 0 : 1},
                    ]}>
                    Yov
                  </Text>
                </TouchableOpacity>
                <Button
                  type="onlyIcon"
                  active={
                    this.state.favData && this.state.favData.is_delete == 0
                  }
                  iconName="heart"
                  onPress={() =>
                    this.state.favData && this.state.favData.is_delete == 0
                      ? this.setState({askRemoveFav: true})
                      : this.addFav()
                  }
                />
                <Button
                  type="onlyIcon"
                  active={this.state.showRating}
                  iconName="star"
                  onPress={() => this._OpenStarRating(0, 'star')}
                />
                <Button
                  type="onlyIcon"
                  active={this.checkLastRecomend()}
                  iconName="thumbs-up"
                  onPress={() =>
                    this.checkLastRecomend()
                      ? this.setState({removeRecomendAlert: true})
                      : this.openRecommendUs()
                  }
                />
                <Button
                  type="onlyIcon"
                  active={this.state.feedback ? true : false}
                  iconName="comments"
                  onPress={() => this.openFeedSheet()}
                />
                <Button
                  type="onlyIcon"
                  active={this.state.isMessage}
                  iconName="envelope"
                  onPress={() => this.openMessageScreen()}
                />
                <Button
                  type="onlyIcon"
                  iconName="phone"
                  onPress={() =>
                    callNumber(this.state.company.formatted_phone_number)
                  }
                />
              </View>
            </Header>
            <StatusBar backgroundColor="#fff" barStyle="dark-content" />
            <Content style={{marginTop: 0}}>
              {/* header */}
              {this._renderFavouriteScreen()}
              {/* Rating section */}
              <View style={{paddingLeft: 10, paddingRight: 10, marginTop: 5}}>
                {this.state.feedback ? (
                  <Button
                    type="FullWithButton"
                    text="View feedback submitted"
                    onPress={() => this.openFeedBackReview()}
                    onPressIn={() => VibrateStart()}
                    onPressOut={() => VibrateStop()}
                  />
                ) : (
                  <Button
                    type="FullWithButton"
                    text={
                      this.state.feedback
                        ? 'View My Feedback'
                        : 'Submit your feedback'
                    }
                    onPress={() => this.openFeedSheet()}
                    onPressIn={() => VibrateStart()}
                    onPressOut={() => VibrateStop()}
                  />
                )}
                <View style={{justifyContent: 'center', marginTop: 10}}>
                  {this.checkLastRecomend() ? (
                    <Text
                      style={{
                        flex: 1,
                        color: Colors.primary,
                        backgroundColor: Colors.white,
                        textAlign: 'center',
                      }}>
                      Recommended by you
                    </Text>
                  ) : (
                    <Button
                      type="trasnparent"
                      text="Recommend us"
                      onPress={() => this.openRecommendUs()}
                      textStyle={styles.transparentTextStyle}
                    />
                  )}
                  {this._ratingSection()}
                </View>
                {this.state.favData && this.state.favData.is_delete == 0 ? (
                  <View
                    full
                    iconRight
                    style={{
                      elevation: 2,
                      marginBottom: 0,
                      backgroundColor: '#F5F5F5',
                      height: 30,
                      justifyContent: 'center',
                    }}>
                    <View
                      style={{flexDirection: 'row', justifyContent: 'center'}}>
                      <Text
                        style={{
                          color: Colors.primary,
                          fontSize: FontSize.large,
                        }}>
                        Added to favourites
                      </Text>
                    </View>
                  </View>
                ) : (
                  <Button
                    type="FullWithButton"
                    text="Add us to your favourites"
                    onPress={() => this.addFav()}
                  />
                )}
                <View
                  style={[
                    styles.favButton,
                    {
                      minHeight: 160,
                      height: 160,
                      marginTop: 10,
                      justifyContent: 'flex-start',
                    },
                  ]}>
                  <Pressable onPress={() => this.openUserNote()}>
                    <View style={{padding: 10, justifyContent: 'flex-start'}}>
                      <Text
                        style={{
                          textAlign: 'center',
                          color: Colors.darkBlue,
                          fontSize: FontSize.heading,
                        }}>
                        {this.state.userNote
                          ? 'Edit / delete private notes'
                          : 'Add private notes'}
                      </Text>
                      {this.state.userNote ? (
                        <Text
                          numberOfLines={4}
                          style={{
                            color: Colors.lightGrey,
                            fontSize: FontSize.large,
                            marginTop: 10,
                          }}>
                          {this.state.userNote.note}
                        </Text>
                      ) : null}
                    </View>
                  </Pressable>
                </View>
                {/* <View full iconRight style={{ elevation: 2, marginBottom: 15, backgroundColor: '#F5F5F5', height: 30, justifyContent: 'center' }} >
                                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                        <Text style={{ color: Colors.lessDarkGrey, fontSize: FontSize.large }}>Your feedback is important to us</Text>
                                    </View>
                                </View> */}
              </View>
              {this._experienceAlert()}
              {this._thankUsAlert()}
              <View style={{height: 80}}></View>
            </Content>
            <View style={styles.footerStyle}>
              <View style={styles.footerContainer}>
                <Button
                  type="footerButton"
                  iconName="star"
                  text={
                    this.state.showRating ? 'Edit / Delete' : 'Star rate us'
                  }
                  active={this.state.showRating}
                  onPress={() => this._OpenStarRating(0, 'star')}
                />
                <Button
                  type="footerButton"
                  iconName="thumbs-up"
                  text={this.checkLastRecomend() ? 'Remove' : 'Recommend us'}
                  active={this.checkLastRecomend()}
                  onPress={() =>
                    this.checkLastRecomend()
                      ? this.setState({removeRecomendAlert: true})
                      : this.openRecommendUs()
                  }
                />
                <Button
                  type="footerButton"
                  iconName="comments"
                  text="Submit a review"
                  active={this.state.feedback ? true : false}
                  onPress={() => this.openFeedSheet()}
                />
              </View>
            </View>
          </Container>
        );
      } else {
        return null;
      }
    }
  }

  render() {
    return (
      <View style={{flex: 1}}>
        {this._renderScreen()}
        {this.feedSheet()}
        {this.starSheet()}
        {this.optionSheet()}
        {this.starHistorySheet()}
        {this.activitySheet()}
        {this.miniReport()}
        {this.feedbackReviewSheet()}
        {this.favouriteSheet()}
        {this.userNoteSheet()}
        {this.recommendHistorySheet()}
        {this.myFavouriteSheet()}
        {this.myFavouriteNoteSheet()}
        {this._askRecomendAlert()}
        {this._removeRecomendAlert()}
        {this._confirmRemoveRecomendAlert()}
        {this._removeFavAlert()}
        {this._confirmRemoveFavAlert()}
        {this._ChangeRecomendAlert()}
        {this._ConfirmchangeRecomendAlert()}
        {this._ChangeFavAlert()}
        {this._ConfirmchangeFavAlert()}
        {this._ChangeStarAlert()}
        {this._ConfirmchangeStarAlert()}
        {this._ShowFavRemovedAlert()}
        {this._recommendSentAlert()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  footerStyle: {
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    position: 'absolute',
    bottom: 0,
    flex: 1,
    padding: 10,
    marginBottom: Platform.OS == 'ios' ? 15 : 0,
    width: '100%',
  },
  footerContainer: {
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerStyle: {
    backgroundColor: Colors.white,
    flexDirection: 'column',
    height: 160,
    elevation: 0,
    paddingRight: 10,
    paddingLeft: 10,
    paddingTop: 10,
    shadowOpacity: 0,
    borderWidth: 0,
    borderBottomWidth: 0,
  },
  buttonStyle: {
    height: 25,
  },
  buttonText: {
    paddingRight: 4,
    paddingLeft: 4,
    textTransform: 'capitalize',
    fontSize: 10,
  },
  buttonActive: {
    borderWidth: 5,
    borderColor: '#008000',
  },
  iconTextStyle: {
    fontWeight: 'bold',
    fontSize: 23,
    marginTop: -5,
  },
  iconContainer: {
    flexDirection: 'row',
    marginTop: 12,
    justifyContent: 'space-between',
    marginBottom: 5,
  },

  starLabel: {
    color: Colors.lessDarkGrey,
    textAlign: 'right',
    fontSize: FontSize.heading,
  },
  starLabelActive: {
    color: Colors.white,
    textAlign: 'right',
    fontSize: FontSize.heading,
  },
  starRatingContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer: {
    flex: 1,
    marginLeft: 10,
    alignSelf: 'center',
  },
  transparentTextStyle: {
    alignSelf: 'center',
    fontSize: FontSize.heading,
  },
  card: {
    flex: 1,
    height: 140,
  },
  cardHeader: {
    flexDirection: 'row',
    backgroundColor: '#f5f5f5',
    padding: 5,
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  cardHeaderText: {
    fontSize: FontSize.medium,
    color: Colors.deepPurple,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: -3,
  },
  ratingContainer: {
    flex: 1,
    marginTop: 5,
    paddingTop: 3,
    paddingBottom: 20,
    backgroundColor: Colors.white,
    justifyContent: 'space-between',
  },
  ratingContainerActive: {
    flex: 1,
    backgroundColor: Colors.primary,
    paddingTop: 3,
    paddingBottom: 20,
    opacity: 0,
  },
  fullButtonContainer: {
    backgroundColor: '#F5F5F5',
    elevation: 2,
    height: 30,
    justifyContent: 'center',
  },
  fullButtonContainerActive: {
    backgroundColor: Colors.primary,
    elevation: 2,
    height: 30,
    justifyContent: 'center',
    opacity: 0,
  },
  favLeftPart: {
    width: '50%',
    marginRight: 2,
    marginBottom: 0.5,
  },
  favRightPart: {
    marginLeft: 2,
    width: '50%',
    marginBottom: 0.5,
  },
  favButtonActive: {
    opacity: 0,
    elevation: 1,
    borderRadius: 5,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    maxHeight: 32,
    flex: 1,
  },
  favButton: {
    elevation: 1,
    borderRadius: 5,
    borderWidth: 0.3,
    borderColor: '#CDCDCD',
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    maxHeight: 32,
    flex: 1,
    backgroundColor: Colors.white,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 2,
  },
});
// Exports
export default CompanyDetailScreen;
