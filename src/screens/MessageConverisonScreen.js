import moment from 'moment';
import {Container, Content, Footer, Header, Item, Text} from 'native-base';
import React, {PureComponent, useRef} from 'react';
import {
  FlatList,
  Pressable,
  Image,
  StatusBar,
  StyleSheet,
  TextInput,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {SubHeader} from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import {getWithLogin, postWithLogin} from '../api/api';
let intervals = null;
export default class MessageConverisonScreen extends PureComponent {
  state = {
    conversion: [],
  };

  componentDidMount() {
    this.getMessageConverison();
    intervals = setInterval(() => {
      console.log('Converiosn interval');
      this.getMessageConverison();
    }, 15000);
  }

  momentFormateDate(date) {
    if (date) {
      return moment(date + '', 'YYYY-MM-DD HH:mm:ss').format(
        'DD/MM/YYYY HH:mm',
      );
    } else {
      return moment().format('DD/MM/YYYY HH:mm');
    }
  }

  componentWillUnmount() {
    clearInterval(intervals);
  }
  async getMessageConverison() {
    try {
      const response = await getWithLogin('msg/usr/con');
      this.setState({conversion: response.data});
      console.log(response.data);
    } catch (e) {}
  }

  renderItem = ({item}) => {
    return (
      <Pressable
        onPress={() =>
          this.props.navigation.navigate('Message', {company: item.company})
        }>
        <View style={{flex: 1, padding: 10, flexDirection: 'row'}}>
          <View style={[styles.iconContainer, {flex: 0.6}]}>
            <Image
              style={styles.icon}
              resizeMode="contain"
              source={{uri: item.company.icon}}
            />
          </View>
          <View style={{flex: 3, marginStart: 10, flexDirection: 'column'}}>
            <Text
              style={{fontWeight: 'bold', color: Colors.darkBlue}}
              numberOfLines={1}>
              {item.company.name}
            </Text>
            <Text style={{color: Colors.darkGrey}} numberOfLines={1}>
              {item.msg}
            </Text>
            <Text
              style={{color: Colors.darkGrey, fontSize: 10}}
              numberOfLines={1}>
              {this.momentFormateDate(item.created_at)}
            </Text>
          </View>
          {item.count > 0 ? (
            <View
              style={{flex: 0.25, marginStart: 10, flexDirection: 'column'}}>
              <Text
                style={{
                  backgroundColor: Colors.primary,
                  color: Colors.white,
                  height: 25,
                  width: 25,
                  paddingTop: 3,
                  fontWeight: 'bold',
                  fontSize: 12,
                  textAlign: 'center',
                  borderRadius: 100,
                }}>
                {item.count}
              </Text>
            </View>
          ) : null}
        </View>
      </Pressable>
    );
  };

  render() {
    return (
      <Container>
        <SubHeader text="Messages" />
        <FlatList
          keyboardShouldPersistTaps="always"
          data={this.state.conversion}
          keyExtractor={(item) => item.id}
          renderItem={this.renderItem}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    flex: 1,
    color: Colors.deepPurple,
    textAlign: 'left',
    fontSize: FontSize.large,
    alignSelf: 'center',
    padding: 0,
    paddingLeft: 5,
  },
  receivedContainer: {
    borderRadius: 20,
    backgroundColor: Colors.primary,
    borderTopLeftRadius: 0,
    padding: 10,
    marginBottom: 5,
    minWidth: 10,
    maxWidth: 250,
    alignSelf: 'flex-start',
    elevation: 2,
  },
  receivedText: {
    textAlign: 'left',
    color: Colors.white,
  },
  sendContainer: {
    borderRadius: 20,
    backgroundColor: Colors.lightGrey2,
    borderTopRightRadius: 0,
    padding: 10,
    marginBottom: 5,
    minWidth: 10,
    maxWidth: 250,
    alignSelf: 'flex-end',
    elevation: 2,
  },
  sendText: {
    textAlign: 'left',
    color: Colors.dark,
  },
  fullButtonContainer: {
    flex: 1,
    borderRadius: 5,
    backgroundColor: '#F5F5F5',
    elevation: 2,
    height: 45,
    justifyContent: 'center',
    maxHeight: 45,
  },
  fullButtonContainerActive: {
    flex: 1,
    borderRadius: 5,
    backgroundColor: Colors.primary,
    elevation: 2,
    height: 45,
    justifyContent: 'center',
    opacity: 0,
    maxHeight: 45,
  },
  // iconContainer: {
  //   width: 63,
  //   height: 63,
  //   backgroundColor: Colors.lightGrey2,
  //   justifyContent: 'center',
  //   borderRadius: 100,
  // },
  // icon: {
  //   width: 40,
  //   height: 40,
  //   alignSelf: 'center',
  // },

  iconContainer: {
    width: 63,
    height: 63,
    // elevation: 5,
    backgroundColor: Colors.lightGrey2,
    justifyContent: 'center',
    borderRadius: 10,
    overflow: "hidden",
    alignItems: 'center',
  },
  icon: {
      height: '140%', width: undefined, aspectRatio: 1
  },
});
