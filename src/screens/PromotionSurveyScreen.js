import moment from 'moment';
import {Container, Content, Spinner, Text} from 'native-base';
import React, {PureComponent} from 'react';
import {StyleSheet, View} from 'react-native';
import {getWithLogin} from '../api/api';
import {Button, CompanyShortProfile, SubHeader} from '../component';
import Colors from '../config/colors';
import {
  ExistingUser,
  Favorite,
  Feedback,
  Recommendtion,
  StarRating,
} from './promotion';

export default class PromotionSurveyScreen extends PureComponent {
  state = {
    promotion: undefined,
    isLoading: true,
    existingUser: false,
    feedback: false,
    favorite: false,
    lastRecomendation: false,
    starRating: false,
  };

  async finishSurvey() {
    try {
      const respons = await getWithLogin(
        'promption/finish/' + this.state.promotion.id,
      );
    } catch (e) {
      console.log(e.response);
    }
  }
  momentFormateDate(date) {
    if (date) {
      return moment(date + '', 'YYYY-MM-DD').format('DD/MM/YYYY');
    } else {
      return moment().format('DD/MM/YYYY');
    }
  }

  getLastRecommendDate() {
    const company = this.state.promotion.company;
    if (company.totalRecommendation == 0) {
      return 'Recommend us';
    } else {
      if (company.lastRecommendationDate) {
        return (
          'Last recommend: ' +
          this.momentFormateDate(company.lastRecommendationDate.created_at)
        );
      } else {
        return 'Recommend us';
      }
    }
  }
  getStarRating() {
    const starRating = this.state.promotion.company.starRating;
    if (starRating && starRating.is_delete === 0) {
      return (
        (starRating.customer_service +
          starRating.quality_of_product +
          starRating.value_for_money) /
        3
      );
    }
    return 0;
  }

  _renderSteps() {
    const {
      existingUser,
      feedback,
      favorite,
      lastRecomendation,
      starRating,
    } = this.state;

    if (!existingUser) {
      return (
        <ExistingUser
          prom_id={this.state.promotion.id}
          comp_id={this.state.promotion.company.id}
          done={() => this.setState({existingUser: true})}></ExistingUser>
      );
    } else if (!feedback) {
      return (
        <Feedback
          promotion={this.state.promotion}
          prom_id={this.state.promotion.id}
          comp_id={this.state.promotion.company.id}
          done={() => this.setState({feedback: true})}></Feedback>
      );
    } else if (!lastRecomendation) {
      const company = this.state.promotion.company;
      return (
        <Recommendtion
          prom_id={this.state.promotion.id}
          comp_id={this.state.promotion.company.id}
          name={company.name}
          address={company.formatted_address}
          done={() => this.setState({lastRecomendation: true})}></Recommendtion>
      );
    } else if (!starRating) {
      return (
        <StarRating
          prom_id={this.state.promotion.id}
          comp_id={this.state.promotion.company.id}
          done={() => this.setState({starRating: true})}></StarRating>
      );
    } else if (!favorite) {
      const company = this.state.promotion.company;
      return (
        <Favorite
          prom_id={this.state.promotion.id}
          comp_id={this.state.promotion.company.id}
          name={company.name}
          address={company.formatted_address}
          done={() => this.setState({favorite: true})}></Favorite>
      );
    } else {
      this.finishSurvey();
      return (
        <View style={{marginTop: 10}}>
          <Text
            style={{
              backgroundColor: Colors.lightGrey2,
              padding: 5,
              textAlign: 'center',
              color: Colors.primary,
              fontSize: 16,
            }}>
            Congralaution !!
          </Text>
          <Text style={{marginTop: 10, textAlign: 'center'}}>
            Thanks for completing the survey
          </Text>
          <Text style={{marginTop: 10, textAlign: 'center'}}>
            You have earned a voucher.
          </Text>
          <Button
            type="border2"
            text="Go to vouchers"
            onPress={() => {
              this.props.navigation.navigate('VoucherList');
            }}></Button>
        </View>
      );
    }
  }

  componentDidMount() {
    this.setState({promotion: this.props.route.params.promotion}, () => {
      console.log(this.state.promotion);
      this.setState({
        existingUser: this.state.promotion.company.existingUser ? true : false,
        feedback: false,
        favorite: this.state.promotion.company.fav ? this.state.promotion.company.fav.is_delete == 1 ? false : true : false,
        lastRecomendation: this.state.promotion.company.lastRecomendation
          ? this.state.promotion.company.lastRecomendation.is_delete == 1 ? false : true
          : false,
        starRating: this.state.promotion.company.starRating
          ? this.state.promotion.company.starRating.is_delete == 1
            ? false
            : true
          : false,
        isLoading: false,
      });
    });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <Container>
          <Spinner color="green" style={{alignSelf: 'center'}} />
        </Container>
      );
    } else {
      const company = this.state.promotion.company;
      return (
        <Container>
          <SubHeader text="Promotion survey"></SubHeader>
          <Content>
            <View style={{padding: 10, height: 90}}>
              <CompanyShortProfile
                icon={company.icon}
                name={company.name}
                totalReco={company.totalRecommendation}
                formatted_address={company.formatted_address}
                rating={this.getStarRating()}
                lastRecomended={this.getLastRecommendDate()}
              />
            </View>
            <View style={{marginTop: 35}}>{this._renderSteps()}</View>
          </Content>
        </Container>
      );
    }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    zIndex: 999999,
    marginBottom: 2,
  },
  iconContainer: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    elevation: 5,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    borderRadius: 10,
  },
  icon: {
    width: 60,
    height: 60,
    alignSelf: 'center',
  },
});
