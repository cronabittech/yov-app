import { Container, Content, Text } from 'native-base';
import React, { PureComponent } from "react";
import { Pressable, StyleSheet, TextInput, View } from 'react-native';
import { postWithLogin } from "../api/api";
import Colors from '../config/colors';
import FontSize from '../config/fontSize';


export default class AddProfileNoteScreen extends PureComponent {

    state = {
        title: 'Add profile notes',
        company: undefined,
        focus: false,
        value: undefined,
    }

    componentDidMount() {
        this.setState({ company: this.props.route.params.company });
    }
    onFocus(){
        this.setState({ focus: true});
    }

    onBlur(){
        this.setState({ focus: false, title: this.state.value ? "Adding profile notes" : "Add profile notes" });
    }

    goBack(){
        // this.props.navigation.setParams();
        this.props.navigation.navigate('Recommend',{defaultOpen:'view_profile_note'});
    }

    async addData() {
        var data = {
            note: this.state.value,
            com_id: this.state.company.id,
        };
        var response = await postWithLogin('add/note', data);
        this.goBack();
    }

    render() {
        return (
            <Container>
                <View style={{ elevation: 2, marginBottom: 0, backgroundColor: '#F5F5F5', height: 30, justifyContent: 'center' }} >
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <Text style={{ color: Colors.primary, fontSize: FontSize.large }}>{this.state.title}</Text>
                    </View>
                </View>
                <Content padder>
                    {
                        this.state.company ? <View style={{ marginTop: 5, marginBottom: 5 }}>
                            <Text numberOfLines={1} style={{ textAlign: 'center', color: Colors.dark }}>
                                {
                                    this.state.company.name
                                }
                            </Text>
                            <Text numberOfLines={1} style={{ textAlign: 'center', color: Colors.lightGrey }}>
                                {
                                    this.state.company.formatted_address
                                }
                            </Text>
                        </View> :
                            null
                    }
                    <View style={{ marginTop: 5 }}>
                        <TextInput
                            multiline={true}
                            onFocus={() => this.onFocus()}
                            onBlur={() => this.onBlur()}
                            style={[styles.inputStyle, { borderColor: this.state.focus ? Colors.deepPurple : Colors.lightGrey }]}
                            placeholder="Enter your notes here"
                            onChangeText={(value) => this.setState({ value,title:'Adding profile notes?' })}
                        />
                    </View>
                    <View style={{ marginTop: 5, }}>
                        <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, justifyContent: this.state.value ? 'space-between' : 'center' }}>
                            {
                                this.state.value ?
                                    <Pressable style={({ pressed }) => [
                                        pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 120 }
                                    ]} onPress={() => this.addData()} >
                                        <Text style={{
                                            color: Colors.deepPurple, textAlign: 'center',
                                            textTransform: 'capitalize', fontSize: FontSize.heading
                                        }}>Done</Text>
                                    </Pressable>
                                    : null
                            }
                            <Pressable style={({ pressed }) => [
                                pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 120 }
                            ]} onPress={() => this.goBack()}>
                                <Text style={{
                                    color: Colors.deepPurple, textAlign: 'center',
                                    textTransform: 'capitalize', fontSize: FontSize.heading
                                }}>Close</Text>
                            </Pressable>
                        </View>
                    </View>
                </Content>
            </Container>);
    }
}

const styles = StyleSheet.create({
    inputStyle: {
        fontSize: 17,
        borderWidth: 1.5,
        borderRadius: 5,
        color:Colors.primary,
        borderColor: Colors.lightGrey
    },
    favButtonActive: {
        opacity: 0,
        elevation: 1,
        borderRadius: 5,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 40,
    },
    favButton: {
        elevation: 1,
        borderRadius: 5,
        borderWidth: 0.3,
        borderColor: "#CDCDCD",
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 50,
        backgroundColor: Colors.white,
        borderRadius: 5, shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
    },
});