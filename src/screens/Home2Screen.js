import {Container, Footer, Text} from 'native-base';
import React, {PureComponent, useRef} from 'react';
import {
  Animated,
  Image,
  StatusBar,
  StyleSheet,
  View,
  Platform,
  Modal,
} from 'react-native';
import {getWithLogin, postWithLogin} from '../api/api';
import {Button, HomeButton, ModelLayout} from '../component';
import FontSize from '../config/fontSize';
import {setSerachType} from '../config/global';
import messaging, {Notification} from '@react-native-firebase/messaging';
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable';

const FadeInView = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current; // Initial value for opacity: 0

  React.useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }, [fadeAnim]);

  React.useEffect(() => {
    messaging().setBackgroundMessageHandler(async (remoteMessage) => {
      console.log('Message handled in the background!', remoteMessage);
    });
  });

  return (
    <Animated.View
      style={{
        ...props.style,
        opacity: fadeAnim,
      }}>
      {props.children}
    </Animated.View>
  );
};

class Home2Screen extends PureComponent {
  state = {
    showExperiance: false,
    userState: undefined,
    is_activities: 0,
    is_favorite: 0,
    is_feedback: 0,
    is_recommend: 0,
    is_star: 0,
    is_search: 0,
    is_note: 0,
    is_qr: 0,
    is_phone: 0,
    is_message: 0,
    is_promotion: 0,
    is_voucher: 0,
    isSearchHide: false,
    isFeedHide: false,
    isRechHide: false,
    isStarHide: false,
    isScanHide: false,
    notification: undefined,
    showNotification: false,
  };

  componentDidMount() {
    this.props.navigation.setOptions({
      headerShown: false,
    });

    this.getUserState();

    this.props.navigation.addListener('focus', () => {
      this.getUserState();
    });

    this.updateToken();
    messaging()
      .getInitialNotification()
      .then((notificationOpen) => {
        if (notificationOpen) {
          this.handleNotification(notificationOpen);
        }
      });

    messaging().onNotificationOpenedApp((remoteMessage) => {
      if (remoteMessage) {
        // App was opened by a notification
        this.handleNotification(remoteMessage);
      }
    });

    messaging().onMessage(async (remoteMessage) => {
      console.log('foreground: ', remoteMessage);
      this.setState({notification: remoteMessage}, () => {
        this.setState({showNotification: true});
      });
    });

    messaging().onTokenRefresh(async (roken) => {
      this.updateToken();
    });
  }

  async getUserState() {
    var response = await getWithLogin('user/state');
    if (response.data != '') {
      this.setState(
        {
          userState: response.data,
          is_star: response.data.is_star,
          is_activities: response.data.is_activities,
          is_favorite: response.data.is_favorite,
          is_feedback: response.data.is_feedback,
          is_recommend: response.data.is_recommend,
          is_search: response.data.is_search,
          is_note: response.data.is_note,
          is_qr: response.data.is_qr,
          is_phone: response.data.is_phone,
          is_message: response.data.is_message,
          is_voucher: response.data.is_voucher,
          is_promotion: response.data.is_promotion,
        },
        () => {
          console.log(this.state.userState);
        },
      );
    }
  }

  async updateToken() {
    const firebase_token = await messaging().getToken();
    try {
      const response = await postWithLogin('fireabase/token', {
        token: firebase_token,
      });
    } catch (err) {
      console.log(err.response);
    }
  }

  _experienceAlert() {
    return (
      <ModelLayout
        visible={this.state.showExperiance}
        centeredViewStyle={style.centeredView}>
        <Text
          style={{
            textAlign: 'center',
            fontWeight: 'bold',
            fontSize: FontSize.heading,
          }}>
          Notice
        </Text>
        <Text
          style={{
            textAlign: 'center',
            marginTop: 15,
            fontSize: FontSize.large,
          }}>
          {' '}
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore
        </Text>
      </ModelLayout>
    );
  }

  handleNotification(notificationOpen) {
    console.log('handle notification called');
    const notification = notificationOpen.notification;
    const data = notificationOpen.data;
    switch (data.type) {
      case 'message_chat':
        this.gotoScreen('MessageConverison');
        break;
      case 'feedback_chat':
        this.props.navigation.navigate('FeedbackReview', {
          company: JSON.parse(data.company),
          feedback: null,
        });
        break;
      case 'promotion_chat':
        this.gotoScreen('PomotionList');
        break;
    }
    this.setState({showNotification: false});
  }

  _notificationAlert() {
    if (this.state.notification) {
      const {notification} = this.state.notification;
      return (
        <Modal
          animationType="fade"
          transparent={true}
          onTouchCancel={true}
          onRequestClose={() => this.setState({showNotification: false})}
          visible={this.state.showNotification}>
          <View
            style={style.centeredView2}
            onTouchEnd={() => this.setState({showNotification: false})}>
            <Pressable
              style={style.modalViewStyle}
              onPress={() => this.handleNotification(this.state.notification)}>
              <View>
                <Text
                  style={{
                    alignSelf: 'stretch',
                    textAlign: 'left',
                    fontWeight: 'bold',
                    fontSize: FontSize.heading,
                  }}>
                  {notification.title}
                </Text>
                <Text
                  style={{
                    textAlign: 'left',
                    marginTop: 5,
                    alignSelf: 'stretch',
                    fontSize: FontSize.large,
                  }}>
                  {notification.body}
                </Text>
              </View>
            </Pressable>
          </View>
        </Modal>
      );
    } else {
      return null;
    }
  }

  // componentDidMount() {
  //     // this.setState({showExperiance:true});
  //     // setTimeout(()=>{
  //     //     this.setState({showExperiance:false})
  //     // },2000);
  // }

  gotoSearchScreen(type) {
    setSerachType(type);
    this.props.navigation.navigate('Search');
  }

  noGo(route) {
    switch (route) {
      case 'rec':
        this.setState({isRechHide: true});
        break;
      case 'feed':
        this.setState({isFeedHide: true});
        break;
      case 'star':
        this.setState({isStarHide: true});
        break;
    }
    setTimeout(() => {
      this.setState({
        isSearchHide: false,
        isFeedHide: false,
        isRechHide: false,
        isScanHide: false,
        isStarHide: false,
      });
    }, 500);
  }

  _renderIcon() {
    if (this.state.is_star == 1 || this.state.is_search == 1) {
      return (
        <View>
          <FadeInView>
            <Image
              style={[style.logo]}
              source={require('../assets/img/logo2.png')}
            />
          </FadeInView>
        </View>
      );
    } else {
      return (
        <View style={[{flex: 1, height: '100%', justifyContent: 'center'}]}>
          <FadeInView>
            <Image
              style={[style.logo]}
              source={require('../assets/img/logo2.png')}
            />
          </FadeInView>
        </View>
      );
    }
  }

  gotoScreen(route) {
    this.props.navigation.navigate(route);
  }

  render() {
    return (
      <Container>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        {this._renderIcon()}
        <View style={{flex: 1, alignSelf: 'center', justifyContent: 'center'}}>
          <View
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
              alignItems: 'center',
            }}>
            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
              <HomeButton
                iconName="sticky-note"
                onPress={() => this.gotoSearchScreen('profile_note_search')}
              />
              <HomeButton
                iconName="product-hunt"
                onPress={() => this.gotoScreen('PromotionSearch')}
              />
              <HomeButton
                iconName="phone"
                onPress={() => this.gotoSearchScreen('phone_search')}
              />
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
              <HomeButton
                iconName="thumbs-up"
                onPress={() => this.gotoSearchScreen('recommend_search')}
              />
              <HomeButton
                iconName="search"
                onPress={() => this.gotoSearchScreen('search')}
              />
              <HomeButton
                iconName="star"
                onPress={() => this.gotoSearchScreen('star_rating_search')}
              />
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
              <HomeButton
                iconName="comments"
                onPress={() => this.gotoSearchScreen('feedback_search')}
              />
              <HomeButton
                iconName="heart"
                onPress={() => this.gotoSearchScreen('favorite_search')}
              />
              <HomeButton
                iconName="envelope"
                onPress={() => this.gotoSearchScreen('messages_search')}
              />
            </View>
          </View>
        </View>
        <Footer
          style={{
            backgroundColor: '#fff',
            height: 100,
            shadowOpacity: 0,
            borderWidth: 0,
            borderBottomWidth: 0,
            borderTopWidth: 0,
            elevation: 0,
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              paddingLeft: 8,
              paddingRight: 8,
            }}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              {this.state.is_favorite == 1 ? (
                <Button
                  type="iconButton"
                  iconName="heart"
                  onPress={() => this.gotoScreen('FavouriteHistory')}
                />
              ) : null}
              {this.state.is_note == 1 ? (
                <Button
                  type="iconButton"
                  iconName="sticky-note"
                  onPress={() => this.gotoScreen('ProfileNoteHistory')}
                />
              ) : null}
              {this.state.is_qr == 1 ? (
                <Button
                  type="iconButton"
                  iconName="qrcode"
                  onPress={() => this.gotoScreen('Scan')}
                />
              ) : null}
              {this.state.is_activities == 1 ? (
                <Button
                  type="iconButton"
                  iconName="list-ul"
                  onPress={() => this.gotoScreen('ActivityHistory')}
                />
              ) : null}
              {this.state.is_voucher == 1 ? (
                <Button
                  type="iconButton"
                  iconName="tag"
                  onPress={() => this.gotoScreen('VoucherList')}
                />
              ) : null}
              {this.state.is_promotion == 1 ? (
                <Button
                  type="iconButton"
                  iconName="product-hunt"
                  onPress={() => this.gotoScreen('PomotionList')}
                />
              ) : null}
            </View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              {this.state.is_feedback == 1 ? (
                <Button
                  type="iconButton"
                  iconName="comments"
                  onPress={() => this.gotoScreen('FeedbackHistory')}
                />
              ) : null}
              {this.state.is_recommend == 1 ? (
                <Button
                  type="iconButton"
                  iconName="thumbs-up"
                  onPress={() => this.gotoScreen('RecommendationHistory')}
                />
              ) : null}
              {this.state.is_search == 1 ? (
                <Button
                  type="iconButton"
                  iconName="search"
                  onPress={() => this.gotoScreen('SearchHistory')}
                />
              ) : null}
              {this.state.is_star == 1 ? (
                <Button
                  type="iconButton"
                  iconName="star"
                  onPress={() => this.gotoScreen('MyStarRating')}
                />
              ) : null}
              {this.state.is_message == 1 ? (
                <Button
                  type="iconButton"
                  iconName="envelope"
                  onPress={() => this.gotoScreen('MessageConverison')}
                />
              ) : null}
            </View>
          </View>
        </Footer>
        {this._experienceAlert()}
        {this._notificationAlert()}
      </Container>
    );
  }
}
export default Home2Screen;

const style = StyleSheet.create({
  logo: {
    width: 300,
    height: 170,
    marginTop: Platform.OS == 'ios' ? 40 : 10,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  cardHolder: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: 10,
  },
  card: {
    height: 160,
    flex: 1,
    marginRight: 10,
    flexDirection: 'column',
  },
  cardHeader: {
    fontWeight: '700',
    fontSize: 18,
  },
  iconHolder: {
    height: 65,
    width: 65,
    alignItems: 'center',
    borderRadius: 100,
  },
  button: {
    flex: 1,
    margin: 1,
    height: 45,
    backgroundColor: '#008000',
    marginTop: 5,
    justifyContent: 'center',
    alignContent: 'center',
    borderRadius: 10,
  },
  textCapatlize: {
    textTransform: 'capitalize',
    fontSize: 10,
    textAlign: 'center',
    color: '#fff',
  },
  centeredView: {
    flex: 1,
    alignItems: 'center',
    marginTop: 22,
    backgroundColor: '#000000ed',
  },
  centeredView2: {
    flex: 1,
    marginTop: 0,
    alignItems: 'flex-start',
    backgroundColor: '#fafafa00',
    justifyContent: 'flex-start',
    width: '100%',
    alignSelf: 'stretch',
  },
  modalViewStyle: {
    alignItems: 'flex-start',
    marginTop: 0,
    alignSelf: 'stretch',
    margin: 15,
    padding: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    elevation: 5,
    borderWidth: 0.3,
    borderColor: '#CDCDCD',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
  },
});
