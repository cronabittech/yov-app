import { Container, Text } from 'native-base';
import React, { PureComponent } from "react";
import { ScrollView } from 'react-native';


class RecomendedScreen extends PureComponent {

    render() {
        return (
            <ScrollView>
                <Container style={{ flex: 1, flexDirection: 'column', justifyContent:'center', height: '100%' }}>
                  <Text>Recomend a business</Text> 
                </Container>
            </ScrollView>

        );
    }
}
export default RecomendedScreen;



