import { Container, Content, Text } from 'native-base';
import React, { PureComponent } from "react";
import { StyleSheet, View } from 'react-native';
import { FavouriteItem } from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';


const VibrateStart = () => {
}

const VibrateStop = () => {
}


export default class MyFavouriteScreen extends PureComponent {

    render() {
        return (
            <Container>
                <View full iconRight style={{ elevation: 2, marginBottom: 0, backgroundColor: Colors.primary, height: 30, justifyContent: 'center' }} >
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <Text style={{ color: Colors.white, fontSize: FontSize.large }}>My favourites </Text>
                    </View>
                </View>
                <Content >{
                    this.props.delete ?
                        null
                        :
                        <FavouriteItem
                            cname="Business name Business name"
                            post="Plumbers"
                            name="Contact name"
                            title="Job title"
                        />
                    }
                    <View style={{ height: 1, backgroundColor: "#CDCDCD" }} />
                </Content>
            </Container>
        );
    }

}

const styles = StyleSheet.create({
    button: {
        borderRadius: 5,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: Colors.white,
        justifyContent: 'center',
        minHeight: 40,
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
        borderWidth: 0.3,
        borderColor: "#CDCDCD",
    },
    buttonActive: {
        opacity: 0,
        elevation: 1,
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 35,
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
    },
});