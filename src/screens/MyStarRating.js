import moment from 'moment';
import {Container, Content, Spinner} from 'native-base';
import React, {PureComponent} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {getWithLogin} from '../api/api';
import {Button, CompanyStarProfile, SubHeader} from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import {setSerachType} from '../config/global';
import {callNumber, doMail, getType} from '../config/util';

const VibrateStart = () => {};
const VibrateStop = () => {};
class MyStarRating extends PureComponent {
  state = {
    isLoading: false,
    starList: undefined,
  };

  _showMessage(msg) {
    Snackbar.show({
      text: msg,
      duration: Snackbar.LENGTH_INDEFINITE,
      action: {
        text: 'close',
        textColor: 'red',
        onPress: () => {
          Snackbar.dismiss();
        },
      },
    });
  }
  componentDidMount() {
    this.getStarRating();
    this.props.navigation.addListener('focus', () => {
      this.getStarRating();
    });
    this.props.navigation.setOptions({
      headerRight: () => (
        <Button
          type="header"
          text="Search"
          onPress={() =>
            this.props.navigation.navigate('SearchFromMyStarRatings')
          }
        />
      ),
    });
  }
  async getStarRating() {
    this.setState({isLoading: true});
    var data = await getWithLogin('user-star-rating');

    var newData = data.data.filter((x) => {
      if (x.starRating.is_delete == 0) {
        return true;
      } else {
        return false;
      }
    });

    console.log("Star rating",newData);

    this.setState({starList: newData});
    this.setState({isLoading: false});
  }

  momentFormateDate(date) {
    if (date) {
      return moment(date + '', 'YYYY-MM-DD HH:mm:ss').format(
        'DD/MM/YYYY HH:mm',
      );
    } else {
      return moment().format('DD/MM/YYYY hh:mm A');
    }
  }

  gotoCompany(item) {
    setSerachType('starHistory');
    this.props.navigation.navigate('Recommend', {
      place_id: item.compnay.place_id,
      defaultOpen: 'starHistory',
    });
  }

  _renderItem = ({item, index}) => {
    return (
      <CompanyStarProfile
        onHeaderClick={() => this.gotoCompany(item)}
        onStarClick={() => this.gotoCompany(item)}
        onRecoClick={() => this.gotoCompany(item)}
        icon={item.compnay.icon}
        name={item.compnay.name}
        starDate={this.momentFormateDate(item.starRating.created_at)}
        formatted_address={item.compnay.formatted_address}
        rating={
          (item.starRating.customer_service +
            item.starRating.quality_of_product +
            item.starRating.value_for_money) /
          3
        }
        type={getType(item.compnay.category)}
        phone={() => callNumber(item.compnay.formatted_phone_number)}
        mail={() => doMail(item.compnay.email)}
      />
    );
  };

  _renderScreen() {
    if (this.state.isLoading) {
      return (
        <Container>
          <SubHeader text="Star rated businesses" />
          <Spinner color="green" style={{alignSelf: 'center', marginTop: 20}} />
        </Container>
      );
    } else {
      return (
        <Container>
          <SubHeader text="Star rated businesses" />
          <Content>
            <FlatList
              keyboardShouldPersistTaps={'handled'}
              data={this.state.starList}
              renderItem={this._renderItem}
              keyExtractor={(item) => item.id}
              ItemSeparatorComponent={(props) => {
                return (
                  <View
                    style={{
                      height: 1,
                      backgroundColor: '#CDCDCD',
                      marginLeft: 100,
                      marginRight: 10,
                    }}
                  />
                );
              }}
            />
          </Content>
        </Container>
      );
    }
  }

  render() {
    return this._renderScreen();
  }
}

const style = StyleSheet.create({
  footerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttonText: {
    color: '#3962a9',
    paddingRight: 10,
    paddingLeft: 10,
    textTransform: 'capitalize',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 35,
    elevation: 5,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
  },
  ratingContainer: {
    paddingTop: 15,
    paddingBottom: 20,
    backgroundColor: Colors.white,
  },
  ratingContainerActive: {
    backgroundColor: Colors.primary,
    paddingTop: 15,
    paddingBottom: 20,
    opacity: 0,
  },
  fullButtonContainer: {
    marginTop: 0,
    backgroundColor: '#F5F5F5',
    elevation: 2,
    height: 30,
    justifyContent: 'center',
  },
  fullButtonContainerActive: {
    marginTop: 0,
    backgroundColor: Colors.primary,
    elevation: 2,
    height: 30,
    justifyContent: 'center',
    opacity: 0,
  },
  starLabel: {
    color: Colors.lessDarkGrey,
    textAlign: 'right',
    fontSize: FontSize.medium,
  },
  starLabelActive: {
    color: Colors.white,
    textAlign: 'right',
    fontSize: FontSize.medium,
  },
  starRatingContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  starContainer: {
    flex: 1,
    marginLeft: 10,
    alignSelf: 'center',
  },
  transparentTextStyle: {
    alignSelf: 'center',
    fontSize: FontSize.medium,
  },
  card: {
    flex: 1,
    height: 140,
  },
});

// Exports
export default MyStarRating;
