import moment from "moment";
import { Container, Content, Text } from 'native-base';
import React, { PureComponent } from "react";
import { Pressable, StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Button, StarRate } from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import { getWithLogin } from '../api/api'

const VibrateStart = () => {
}

const VibrateStop = () => {
}

export default class MiniReportSlider extends PureComponent {

    state = {
        starCount1: 0,
        starCount2: 0,
        starCount3: 0,
        lastLog: undefined,
        showTick: false,
        currentStarRating: undefined,
    }

    async getLog() {
        const response = await getWithLogin('user/company/log/' + this.props.lastLog.com_id);
        console.log(response.data);
        if (response.data.length > 0) {
            console.log(response.data[0]);
            this.setState({ lastLog: response.data[0] });
        }
    }

    getlogIconName(logtype) {
        var iconName = "";
        switch (logtype) {
            case "search":
                iconName = "search"
                break;
            case "star":
                iconName = "star"
                break;
            case "recomended":
                iconName = "thumbs-up"
                break;
            case "note":
                iconName = "sticky-note"
                break;
            case "favorite":
                iconName = "heart"
                break;
            default:
                break;
        }
        return iconName
    }

    momentFormateDate(date) {
        if (date) {
            return moment(date + "", "YYYY-MM-DD HH:mm:ss").format('DD/MM/YYYY HH:mm');
        } else {
            return moment().format('DD/MM/YYYY HH:mm');
        }
    }

    componentDidMount() {
        this.getLog();
        this.setState({
            starCount1: this.props.star1,
            starCount2: this.props.star2,
            starCount3: this.props.star3,
            lastLog: this.props.lastLog,
            showTick: this.props.showTick,
            currentStarRating: this.props.currentStarRating ? this.props.currentStarRating : undefined
        }, () => {

            console.log("Current Star Rating:", this.state.currentStarRating);
        });
        setTimeout(() => { this.setState({ showTick: false }) }, 1500);
    }

    openRecommend() {
        this.props.close();
        setTimeout(() => {
            this.props.openRecommend();
        }, 1000);
    }

    openStarHistory() {
        this.props.close();
        setTimeout(() => {
            this.props.openStarHistory()
        }, 1000);
    }
    openStarSheet() {
        this.props.close();
        setTimeout(() => {
            this.props.openStarSheet()
        }, 1000);
    }

    handleFav() {
        if (this.props.favData && this.props.favData.is_delete == 0) {
            this.props.close();
            setTimeout(() => {
                this.props.viewFavourites();
            }, 1000);
        } else {
            this.props.close();
            setTimeout(() => {
                this.props.AddFav();
            }, 1000);
        }
    }

    checkName() {
        var name = "";
        if (this.props.favData) {
            if (this.props.favData.name_title != "" && this.props.favData.name_title != null) {
                if (this.props.favData.first_name != null && this.props.favData.surname != null) {
                    name = name + " " + this.props.favData.name_title;
                }
            }
            if (this.props.favData.first_name != "" && this.props.favData.first_name != null) {
                name = name + " " + this.props.favData.first_name;
            }
            if (this.props.favData.surname != "" && this.props.favData.surname != null) {
                name = name + " " + this.props.favData.surname;
            }
        }
        if (name != "") {
            return name;
        } else {
            return "Contact name";
        }
    }

    render() {
        return (
            <Container>
                <Content style={{ paddingLeft: 10, paddingRight: 10 }}>

                    <Pressable onPress={() => this.props.lastRecommendation ? this.props.lastRecommendation.is_delete == 1 ? this.openRecommend() : {} : this.openRecommend()}>
                        <View >
                            <Button type="FullWithButton2" text={this.props.lastRecommendation ? this.props.lastRecommendation.is_delete == 1 ? "Recommend us to others" : "Recommended" : "Recommend us to others"}
                                onPress={() => this.props.lastRecommendation ? this.props.lastRecommendation.is_delete == 1 ? this.openRecommend() : {} : this.openRecommend()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()} />
                            <View style={{ height: 60, justifyContent: 'center' }}>{
                                this.props.lastRecommendation ?
                                    <Text style={{ textAlign: 'center', color: Colors.lightGrey }}>
                                        {this.props.lastRecommendation.is_delete == 0 ? "Recommended on:" : "Recommendation removed  on:"} {this.momentFormateDate(this.props.lastRecommendation.created_at)}
                                    </Text>
                                    :
                                    <Text style={{ textAlign: 'center', color: Colors.lightGrey }}> Recommended us</Text>
                            }
                            </View>
                            {
                                this.state.showTick ?
                                    <View style={{ textAlign: 'center', alignItems: 'center', position: 'absolute', top: 38, left: '45%' }}>
                                        <Icon style={{ alignSelf: 'center' }} color={this.props.lastRecommendation && this.props.lastRecommendation.is_delete == 0 ? Colors.primary : Colors.lightGrey}
                                            name={this.props.lastRecommendation && this.props.lastRecommendation.is_delete == 0 ? "thumbs-up" : "thumbs-down"} size={45} />
                                    </View>
                                    :
                                    null
                            }
                        </View>
                    </Pressable>
                    <View >
                        <Button type="FullWithButton2" active={this.props.currentStarRating ? true : false} text={this.props.currentStarRating ? "View my star rating" : "Star rate our services"}
                            onPress={() => this.props.currentStarRating ? this.openStarHistory() : this.openStarSheet()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()} />
                        <Pressable
                            onPress={() => this.props.currentStarRating ? this.openStarHistory() : this.openStarSheet()}
                            onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                            {({ pressed }) => (
                                <View style={[pressed ? style.ratingContainerActive : style.ratingContainer]}>
                                    <Text
                                        style={[style.transparentTextStyle, { color: pressed ? Colors.white : Colors.lessDarkGrey }]}>
                                        {this.props.currentStarRating ? this.props.currentStarRating.is_delete == 1 ? "You deleted our star rating on:" : "You star rated us on:" : "Star rate us"}
                                    </Text>
                                    {this.props.currentStarRating ?
                                        <Text
                                            style={[style.transparentTextStyle, { color: pressed ? Colors.white : Colors.lessDarkGrey, marginTop: 5 }]}>
                                            {this.props.currentStarRating ? this.momentFormateDate(this.props.currentStarRating.created_at) : this.momentFormateDate(undefined)}
                                        </Text>
                                        :
                                        null
                                    }

                                    <View style={{ marginTop: 10 }}>
                                        <View style={style.starRatingContainer}>
                                            <View style={{ flex: 1 }}>
                                                <Text style={pressed ? style.starLabelActive : style.starLabel} >Customer services</Text>
                                            </View>
                                            <View style={style.starContainer}>
                                                <StarRate
                                                    disabled={true}
                                                    starSize={20}
                                                    buttonStyle={{ marginLeft: 3 }}
                                                    rating={this.state.starCount1}
                                                />
                                            </View>
                                        </View>
                                        <View style={style.starRatingContainer}>
                                            <View style={{ flex: 1 }}>
                                                <Text style={pressed ? style.starLabelActive : style.starLabel} >Product(s)/ service</Text>
                                            </View>
                                            <View style={style.starContainer}>
                                                <StarRate
                                                    disabled={true}
                                                    starSize={20}
                                                    buttonStyle={{ marginLeft: 3 }}
                                                    rating={this.state.starCount2}
                                                />
                                            </View>
                                        </View>
                                        <View style={style.starRatingContainer}>
                                            <View style={{ flex: 1 }}>
                                                <Text style={pressed ? style.starLabelActive : style.starLabel} >Value for money</Text>
                                            </View>
                                            <View style={style.starContainer}>
                                                <StarRate
                                                    disabled={true}
                                                    starSize={20}
                                                    buttonStyle={{ marginLeft: 3 }}
                                                    rating={this.state.starCount3}
                                                />
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            )}
                        </Pressable>
                    </View>
                    <Pressable onPress={() => {
                        this.props.close();
                        if (this.props.feedback) {
                            setTimeout(() => {
                                this.props.openFeedBackReview();
                            }, 1000);
                        } else {
                            setTimeout(() => {
                                this.props.openFeedSheet();
                            }, 1000);
                        }
                    }}>
                        <View>
                            <Button type="FullWithButton2" active={this.props.feedback ? true : false} text={this.props.feedback ? "View my feedback / comments" : "Send us your feedback / comments"}
                                onPress={() => {
                                    this.props.close();
                                    if (this.props.feedback) {
                                        setTimeout(() => {
                                            this.props.openFeedBackReview();
                                        }, 1000);
                                    } else {
                                        setTimeout(() => {
                                            this.props.openFeedSheet();
                                        }, 1000);
                                    }
                                }}
                                onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()} />
                            <View style={{ height: 65, justifyContent: 'center' }}>
                                <Text numberOfLines={2} style={{ textAlign: 'center', color: Colors.lightGrey }}>
                                    {this.props.feedback ? this.props.feedback : "Submit your feedback"}
                                </Text>
                            </View>
                        </View>

                    </Pressable>
                    <View >
                        <Button type="FullWithButton2" text="View my activities"
                            active={true}
                            onPress={() => {
                                this.props.close();
                                setTimeout(() => {
                                    this.props.openActivitySheet()
                                }, 1000);
                            }}
                            onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()} />
                        <Pressable
                            onPress={() => {
                                this.props.close();
                                setTimeout(() => {
                                    this.props.openActivitySheet();
                                }, 1000);
                            }}
                            onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                            {
                                this.state.lastLog ?
                                    <View style={{ height: 60, justifyContent: 'center' }}>
                                        <View style={{ flexDirection: 'row', }}>
                                            <Icon style={{ marginLeft: 20, marginRight: 20 }} name={this.getlogIconName(this.state.lastLog.log_type)} size={25} color={Colors.primary} />
                                            <Text style={{ marginLeft: 10, alignSelf: 'center', color: Colors.lightGrey }}>{this.state.lastLog.log}{this.momentFormateDate(this.state.lastLog.created_at)}</Text>
                                        </View>
                                    </View>
                                    :
                                    <View style={{ height: 60, justifyContent: 'center' }}>
                                        <Text style={{ textAlign: 'center', color: Colors.lightGrey }}>My Activites</Text>
                                    </View>
                            }
                        </Pressable>
                    </View>
                    <Pressable onPress={() => {
                        this.props.close();
                        setTimeout(() => { this.props.openUserNote() }, 1000);
                    }}>
                        <Button type="FullWithButton2" text={this.props.userNote ? "View private notes" : "Add your private notes"}
                            onPress={() => {
                                this.props.close();
                                setTimeout(() => { this.props.openUserNote() }, 1000);
                            }}
                            onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()} />
                        <View style={{ height: 65, justifyContent: 'center' }}>
                            <Text numberOfLines={2} style={{ textAlign: 'center', color: Colors.lightGrey }}>
                                {this.props.userNote ?
                                this.props.userNote.note :
                                "Add private notes"}
                            </Text>
                        </View>
                    </Pressable>
                    <Pressable onPress={() => this.handleFav()}>
                        <View style={{ justifyContent: 'center', }}>
                            <Button type="FullWithButton2" text={this.props.favData && this.props.favData.is_delete == 0 ? "View favourites" : "Add to favourites"}
                                onPress={() => this.handleFav()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()} />
                            {
                                this.props.favData ?
                                    <View style={{ height: 65 }}>
                                        <Text style={{ marginTop: 10, color: Colors.lightGrey, textAlign: 'center', }}>
                                            {this.props.favData && this.props.favData.is_delete == 0 ?
                                                this.checkName()
                                                :
                                                "Contact name"
                                            }
                                        </Text>
                                        <Text style={{ marginTop: 5, color: Colors.lightGrey, marginBottom: 10, textAlign: 'center' }}>
                                            {this.props.favData && this.props.favData.is_delete == 0 ?
                                                this.props.favData.job_title != "" && this.props.favData.job_title != null ? this.props.favData.job_title : "Job title"
                                                :
                                                "Job title"
                                            }
                                        </Text>
                                    </View> :
                                    <View style={{ height: 200 }}>
                                        <Text style={{ marginTop: 20, marginBottom: 10, color: Colors.lightGrey, textAlign: 'center' }}>Add us to your favourites</Text>
                                    </View>
                            }
                        </View>
                    </Pressable>
                    <View style={{ height: 25, elevation: 2, backgroundColor: '#F5F5F5', }}>
                        <Text style={{ textAlign: 'center', color: Colors.darkGrey, marginTop: 1 }}>Your feedback is important to us</Text>
                    </View>

                    <View style={{ height: 250 }}>

                    </View>
                </Content>
            </Container>
        );

    }
}

const style = StyleSheet.create({

    StarLabel: {
        alignSelf: 'center', marginTop: 35,
        fontSize: FontSize.heading,
        color: Colors.darkGrey
    },
    buttonText: {
        color: Colors.deepPurple,
        paddingRight: 10,
        paddingLeft: 10,
        fontSize: FontSize.medium,
        textAlign: 'center',
        textTransform: 'capitalize'
    },
    button: {
        borderColor: Colors.primary,
        borderWidth: 2,
        justifyContent: 'center',
        borderRadius: 5,
        paddingLeft: 1,
        paddingRight: 1,
        paddingTop: 3,
        paddingBottom: 3,
        elevation: 2
    },
    transparentTextStyle: {
        alignSelf: 'center', fontSize: FontSize.heading
    }, fullButtonContainer: {
        flex: 1,
        backgroundColor: '#F5F5F5', elevation: 2, height: 30, justifyContent: 'center'
    }, fullButtonContainerActive: {
        flex: 1,
        backgroundColor: Colors.primary, elevation: 2, height: 30, justifyContent: 'center',
        opacity: 0,
    }, ratingContainer: {
        marginTop: 15, paddingBottom: 20,
        backgroundColor: Colors.white,
    }, ratingContainerActive: {
        backgroundColor: Colors.primary,
        marginTop: 15, paddingBottom: 20, opacity: 0
    },
    starLabel: {
        color: Colors.lessDarkGrey, textAlign: 'right', fontSize: FontSize.heading
    },
    starLabelActive: {
        color: Colors.white, textAlign: 'right', fontSize: FontSize.large
    },
    starRatingContainer: {
        flex: 1, flexDirection: 'row',
        marginTop: 8
    },
    starContainer: {
        flex: 1, marginLeft: 10,
        alignSelf: 'center'
    }, footerButtton: {
        marginTop: 5,
        marginBottom: 5,
        borderWidth: 0.3,
        borderColor: "#CDCDCD",
        height: 50,
        justifyContent: 'center',
        backgroundColor: Colors.white,
        borderRadius: 10, shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
    },
    footerButttonActivate: {
        borderWidth: 1,
        justifyContent: 'center',
        borderColor: "#CDCDCD",
        backgroundColor: Colors.primary,
        height: 50,
        elevation: 2,
        borderRadius: 10,
        opacity: 0
    },
});