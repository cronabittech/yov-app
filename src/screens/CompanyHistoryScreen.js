/*
Company history screen currently not in used
:-By kishan hadiyal
*/

import { Container, Content, Footer, Header, Spinner, Text } from 'native-base';
import React, { PureComponent } from "react";
import { Modal, Pressable, StatusBar, StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { Button, CompanyShortProfile, ModelLayout, StarRate, SubHeader } from '../component';
import Action from '../config/action';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';

const VibrateStart = () => {
}
const VibrateStop = () => {
}

class CompanyHistoryScreen extends PureComponent {

    state = {
        step: 0,
        showThankYou: false,
        data: undefined,
        noStarRating: false,
        yesStarRating: false,
        company: undefined,
        manageRating: undefined,
        starCount1: 0,
        starCount2: 0,
        starCount3: 0,
        starhasDone: false,
        onlineStar: false
    }
    _back() {
        setTimeout(() => {
            this.props.navigation.goBack();

        }, 50);
    }
    _thankUsAlert() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.showThankYou}>
                <View style={style.centeredView}>
                    <View style={style.modalView}>
                        <Text style={{ textAlign: 'center' }}>Thank you for taking time to let us know your opinion.</Text>
                    </View>
                </View>
            </Modal>
        );
    }


    _ratingSection() {
        return (
            <Pressable
                onPress={() => this.gotoHistory()}
                onPressIn={() => VibrateStart()}
                onPressOut={() => VibrateStop()}>

                {({ pressed }) => (
                    <View style={[pressed ? style.ratingContainerActive : style.ratingContainer]}>
                        <Text
                            style={[style.transparentTextStyle, { color: pressed ? Colors.white : Colors.lessDarkGrey }]}>
                            You Star rated us on:
                    </Text>
                        <Text
                            style={[style.transparentTextStyle, { color: pressed ? Colors.white : Colors.lessDarkGrey, marginTop: 5 }]}>
                            10/10/2020 at 2:30pm
                    </Text>
                        <View style={{ marginTop: 10 }}>
                            <View style={style.starRatingContainer}>
                                <View style={{ flex: 1 }}>
                                    <Text style={pressed ? style.starLabelActive : style.starLabel} >Customer services</Text>
                                </View>
                                <View style={style.starContainer}>
                                    <StarRate
                                        disabled={true}
                                        starSize={20}
                                        buttonStyle={{ marginLeft: 3 }}
                                        rating={this.state.starCount1}
                                    />
                                </View>
                            </View>
                            <View style={style.starRatingContainer}>
                                <View style={{ flex: 1 }}>
                                    <Text style={pressed ? style.starLabelActive : style.starLabel} >Quality of product / service</Text>
                                </View>
                                <View style={style.starContainer}>
                                    <StarRate
                                        disabled={true}
                                        starSize={20}
                                        buttonStyle={{ marginLeft: 3 }}
                                        rating={this.state.starCount2}
                                    />
                                </View>
                            </View>
                            <View style={style.starRatingContainer}>
                                <View style={{ flex: 1 }}>
                                    <Text style={pressed ? style.starLabelActive : style.starLabel} >Value for money</Text>
                                </View>
                                <View style={style.starContainer}>
                                    <StarRate
                                        disabled={true}
                                        starSize={20}
                                        buttonStyle={{ marginLeft: 3 }}
                                        rating={this.state.starCount3}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                )}
            </Pressable>
        );
    }

    componentDidMount() {
        this.props.getCompany(this.props.route.params.company.place_id);
        setTimeout(() => {
            this.props.navigation.setOptions({
                headerRight: () => (
                    <Button type="blink" text="Close" visible={true} onPress={() => this.props.navigation.pop(1)} />
                ),
                headerLeft: null
            });
        }, 100);
        this.props.navigation.addListener('focus', () => {
            setTimeout(() => {
                this.setState({ screenLoseFocus: false });
            }, 1000);
            this.props.getCompany(this.props.route.params.company.place_id);

        });
    }

    componentDidUpdate(prevProps) {
        console.log('History props', this.props);
        if (prevProps !== this.props) {
            switch (this.props.type) {
                case Action.GET_COMP_SUCCESS:
                    this.setState({ company: this.props.company, showExperiance: false },
                        () => {
                            if (this.state.company.starRating) {
                                this.setState({
                                    starCount1: this.state.company.starRating.customer_service,
                                    starCount2: this.state.company.starRating.quality_of_product,
                                    starCount3: this.state.company.starRating.value_for_money,
                                    starhasDone: true,
                                    onlineStar: true
                                })
                            }else{
                                this.setState({
                                    starCount1: 0,
                                    starCount2: 0,
                                    starCount3: 0,
                                    starhasDone: false,
                                    onlineStar: false
                                })
                            }
                        });
                    break;
                case Action.GET_COMP_FAIL:
                    this._showMessage(this.props.message);
                    break;

            }
        }
    }

    gotoHistory() {
        this.props.navigation.navigate('MyStarRating', {
            company: this.state.company,
            starRating: {
                star1: this.state.starCount1,
                star2: this.state.starCount2,
                star3: this.state.starCount3,
            }
        });
    }

    openStarRating() {
        setTimeout(() => {
            this.props.navigation.navigate('StarRating', { company: this.state.company });
        }, 50);
    }


    _yesStarRatingSelected() {
        return (
            <ModelLayout
                centeredViewStyle={{ margin: 0, marginTop: 90 }}
                visible={this.state.yesStarRating}>
                <Text style={{ textAlign: 'center' }}>Thank you submitting star rating. </Text>
            </ModelLayout>
        );
    }

    _RenderScreen() {
        if (this.props.isLoading) {
            return (<Container>
                <SubHeader text="History" />
            <Spinner color='green' style={{ alignSelf: 'center', marginTop:20 }} />
        </Container>);
        } else {
            return(
                <Container >
                <SubHeader text="History" />
                <Header style={{ backgroundColor: Colors.white, height: 130, padding: 10, elevation: 0 }}>
                    <CompanyShortProfile
                        onRecoClick={() => this.setState({ sheetOpen: false })}
                        onStarClick={() => this.setState({ sheetOpen: false })}
                        onHeaderClick={() => this.setState({ sheetOpen: false })}
                        icon={this.state.company ? this.state.company.icon : ''}
                        name={this.state.company ? this.state.company.name : ''}
                        // onStarClick={() => setTimeout(() => { this._OpenStarRating() }, 50)}
                        sheetOpen={this.state.sheetOpen}
                        startDone={this.state.starPerform}
                        formatted_address={this.state.company ? this.state.company.formatted_address : ''}
                        rating={((this.state.starCount1 + this.state.starCount2 + this.state.starCount3) / 3)}
                    />
                </Header>
                <StatusBar backgroundColor="white" barStyle="dark-content" />

                <Content padder>

                    <Pressable
                        style={({ pressed }) => [
                            pressed ? style.fullButtonContainerActive : style.fullButtonContainer
                        ]}
                        onPress={() => this.gotoHistory()}
                        onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={{ color: pressed ? Colors.white : Colors.deepPurple, fontSize: FontSize.large }}>My star Rating</Text>
                                <Icon style={{ marginTop: 5, marginLeft: 20 }} name='chevron-right' color={Colors.deepPurple} />
                            </View>
                        )}
                    </Pressable>
                    {
                        this._ratingSection()
                    }
                    <Pressable
                        style={({ pressed }) => [
                            pressed ? style.fullButtonContainerActive : style.fullButtonContainer, { marginTop: -5 }
                        ]}
                        onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={{ color: pressed ? Colors.white : Colors.deepPurple, fontSize: FontSize.large }}>My opinion </Text>
                                <Icon style={{ marginTop: 5, marginLeft: 20 }} name='chevron-right' color={Colors.deepPurple} />
                            </View>
                        )}
                    </Pressable>
                    <View style={{ height: 70, justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', color: Colors.lightGrey }}>Recommend us</Text>
                    </View>
                    <Pressable
                        style={({ pressed }) => [
                            pressed ? style.fullButtonContainerActive : style.fullButtonContainer
                        ]}
                        onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={{ color: pressed ? Colors.white : Colors.deepPurple, fontSize: FontSize.large }}>My feedback comments</Text>
                                <Icon style={{ marginTop: 5, marginLeft: 20 }} name='chevron-right' color={Colors.deepPurple} />
                            </View>
                        )}
                    </Pressable>
                    <View style={{ height: 100, justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', color: Colors.lightGrey }}>Submit your feedback</Text>
                    </View>
                    <Pressable
                        style={({ pressed }) => [
                            pressed ? style.fullButtonContainerActive : style.fullButtonContainer
                        ]}
                        onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                        {({ pressed }) => (
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={{ color: pressed ? Colors.white : Colors.deepPurple, fontSize: FontSize.large }}>My private notes</Text>
                                <Icon style={{ marginTop: 5, marginLeft: 20 }} name='chevron-right' color={Colors.deepPurple} />
                            </View>
                        )}
                    </Pressable>
                    <View style={{ height: 100, justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', color: Colors.lightGrey }}>Add private notes</Text>
                    </View>
                </Content>



                <Footer style={{ backgroundColor: Colors.white, padding: 10, height: 80 }}>
                    <View style={style.footerContainer}>
                        <Button type="footerButton" iconName="star" text={this.state.starhasDone ? "Edit / Delete" : "Star rate us"} active={this.state.starhasDone}
                            onPress={() => this.openStarRating()} />
                        <Button type="footerButton" iconName="check" text="Recommend us" active={false}
                        />
                        <Button type="footerButton" iconName="comments" text="Submit a review" active={false}
                        />
                    </View>
                </Footer>
            </Container >
            );
        }
    }


    render() {
        return (
            this._RenderScreen()
        );
    }
}


const style = StyleSheet.create({
    footerContainer: {

        flex: 1, flexDirection: 'row', justifyContent: 'space-between'
    },
    buttonText: {
        color: '#3962a9',
        paddingRight: 10,
        paddingLeft: 10,
        textTransform: 'capitalize'
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 10,
        padding: 35,
        elevation: 5,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        }
    }, ratingContainer: {
        paddingTop: 15, paddingBottom: 20,
        backgroundColor: Colors.white,
    }, ratingContainerActive: {
        backgroundColor: Colors.primary,
        paddingTop: 15, paddingBottom: 20, opacity: 0
    }, fullButtonContainer: {
        marginTop: 10,
        backgroundColor: '#F5F5F5', elevation: 2, height: 30, justifyContent: 'center'
    }, fullButtonContainerActive: {
        marginTop: 10,
        backgroundColor: Colors.primary, elevation: 2, height: 30, justifyContent: 'center', opacity: 0
    },
    starLabel: {
        color: Colors.lessDarkGrey, textAlign: 'right', fontSize: FontSize.medium
    },
    starLabelActive: {
        color: Colors.white, textAlign: 'right', fontSize: FontSize.medium
    },
    starRatingContainer: {
        flex: 1, flexDirection: 'row'
    },
    starContainer: {
        flex: 1, marginLeft: 10,
        alignSelf: 'center'
    },
    transparentTextStyle: {
        alignSelf: 'center', fontSize: FontSize.medium
    },
    card: {
        flex: 1, height: 140
    },
});


const mapStateToProps = (state) => {
    // console.log('Star Rating State:');
    // console.log(state.StarRating , state.companyDetail);

    return {
        error: state.companyDetail.error,
        isLoading: state.companyDetail.isLoading,
        message: state.companyDetail.message,
        company: state.companyDetail.company,
        type: state.companyDetail.type,

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getCompany: (data) => {
            dispatch({
                type: Action.GET_COMP,
                value: data
            })
        }
    };
};

// Exports
export default connect(mapStateToProps, mapDispatchToProps)(CompanyHistoryScreen);



