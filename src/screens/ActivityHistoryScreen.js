/*
Bottom screen for activity history screen
:-By kishan hadiyal
*/

import moment from "moment";
import { Container, Content } from 'native-base';
import React, { PureComponent } from "react";
import { FlatList, StyleSheet, View } from 'react-native';
import { getWithLogin } from "../api/api";
import { Button, LogHistoryItem, SubHeader } from '../component';


const VibrateStart = () => {
}

const VibrateStop = () => {
}



export default class ActivityHistoryScreen extends PureComponent {

    state = {
        logList: undefined,
        deleteLog: undefined,
        deletLogPopup: false,
        lastLog: undefined
    }

    componentDidMount() {
        this.getLog();
        this.props.navigation.setOptions({
            headerRight: () => (
                <Button type="header" text="Search" onPress={() => this.props.navigation.navigate('SearchFromMyActivities')} />
            ),
        });
    }
    gotoCompany(item) {
        this.props.navigation.navigate('Recommend', {
            place_id: item.company.place_id,
            defaultOpen: "view_activities"
        });
    }


    momentFormateDate(date) {
        if (date) {
            return moment(date + "", "YYYY-MM-DD HH:mm:ss").format('DD/MM/YYYY HH:mm');
        } else {
            return moment().format('DD/MM/YYYY HH:mm');
        }
    }

    getlogIconName(logtype) {
        var iconName = "";
        switch (logtype) {
            case "qr":
                iconName = "qrcode"
                break;
            case "search":
                iconName = "search"
                break;
            case "star":
                iconName = "star"
                break;
            case "recomended":
                iconName = "thumbs-up"
                break;
            case "note":
                iconName = "sticky-note"
                break;
            case "favorite":
                iconName = "heart"
                break;
            default:
                break;
        }
        return iconName
    }

    _renderItem = ({ item, index }) => {
        return (
            <LogHistoryItem
                onHeaderClick={() => this.gotoCompany(item)}
                onStarClick={() => this.gotoCompany(item)}
                onRecoClick={() => this.gotoCompany(item)}
                name={item.company.name}
                icon={item.company.icon}
                formatted_address={item.company.formatted_address}
                log={item.log.log}
                logIcon={this.getlogIconName(item.log.log_type)}
                starDate={this.momentFormateDate(item.log.created_at)}
            />
        );
    }

    async getLog() {
        const response = await getWithLogin('user/all-log');
        console.log(response.data);
        if (response.data.length > 0) {
            this.setState({ logList: response.data });
        } else {
            this.setState({ logList: response.data });
        }
    }
    render() {
        return (
            <Container>
                <SubHeader text="Latest business activity" />
                <Content>
                    <FlatList
                        keyboardShouldPersistTaps={'handled'}
                        data={this.state.logList}
                        renderItem={this._renderItem}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={(props) => {
                            return (<View style={{ height: 1, backgroundColor: "#EEEEEE", marginRight: 10, marginLeft: 83 }} />);
                        }}
                    />
                    <View style={{ height: 1, backgroundColor: "#EEEEEE", marginRight: 10, marginLeft: 83 }} />
                </Content>
            </Container>
        );
    }

}

const styles = StyleSheet.create({

});