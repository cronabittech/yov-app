import { Container, Item, Spinner, Text } from 'native-base';
import React, { PureComponent } from "react";
import { FlatList, LogBox, Pressable, StatusBar, StyleSheet, TextInput, View } from 'react-native';
import { getWithLogin } from '../api/api';
import { Button, SubHeader } from '../component';
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import { getSerachType } from "../config/global";

export default class SearchFromFavoriteScreen extends PureComponent {

    state = {
        query: '',
        list: undefined,
        hideItem: undefined,
        allCompany: true,
        blinkItem: undefined,
        subtitle: "",
        isLoading: false
    }
    constructor() {
        super();
    }

    _searchQuery() {
        // this.props.search(this.state.query);
    }

    componentDidMount() {
        LogBox.ignoreLogs(['Warning: ...']);
        LogBox.ignoreAllLogs();

        this.props.navigation.setOptions({
            headerRight: () => (
                <Button type="header" text="Search" onPress={() => this._searchQuery()} />
            ),
        });
    }


    async searchFav(text) {
        this.setState({ isLoading: true });
        try {
            var result = await getWithLogin('user/search/fav/' + text);
            console.log(result);
            this.setState({ isLoading: false, list: result.data });
        } catch (error) {
            console.log(error.response);
            this.setState({ isLoading: false });
        }

    }

    gotoDetail(placeId) {
        this.setState({ blinkItem: placeId });
        setTimeout(() => {
            this.setState({ blinkItem: null });
        }, 3000);
        setTimeout(() => {
            var type = getSerachType();
            this.props.navigation.navigate('Recommend', { place_id: placeId, defaultOpen: '' });
        }, 500);
    }


    renderItem = ({ item }) => {

        if (this.state.allCompany) {
            return (
                <Pressable style={({ pressed }) => [
                    pressed ? styles.listItemActive : styles.listItem
                ]} onPress={() => this.gotoDetail(item.company_place_id)}>
                    {({ pressed }) => (
                        <View>
                            <Text
                                style={pressed ? styles.itemHeaderActive : this.state.blinkItem == item.place_id ? styles.itemHeaderActive2 : styles.itemHeader}>
                                {item.company_name}</Text>
                            <Text note numberOfLines={2}
                                style={pressed ? styles.itemTextActive : styles.itemText}>
                                {item.company_formatted_address}</Text>
                        </View>
                    )}
                </Pressable>
            );

        } else {
            return (
                <Pressable style={({ pressed }) => [
                    pressed ? styles.listItemActive : styles.listItem
                ]} onPress={() => this.gotoDetail(item.place_id)} >
                    {({ pressed }) => (
                        <View>
                            <Text
                                style={pressed ? styles.itemHeaderActive : this.state.blinkItem == item.place_id ? styles.itemHeaderActive2 : styles.itemHeader}>
                                {item.structured_formatting.main_text}</Text>
                            <Text note numberOfLines={2}
                                style={pressed ? styles.itemTextActive : styles.itemText}>
                                {item.company_formatted_address}</Text>
                        </View>
                    )}
                </Pressable>
            );
        }
    };

    getSubTitle() {

    }

    renderSuggestionList = () => {
        if (this.state.list != undefined) {
            return (
                <FlatList
                    keyboardShouldPersistTaps={'always'}
                    data={this.state.list.slice(0, 3)}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.place_id}
                    ItemSeparatorComponent={(props) => {
                        return (<View style={{ height: 1, backgroundColor: "#CDCDCD" }} />);
                    }}
                />
            );
        } else {
            return null;
        }
    }

    _renderSpinner() {
        if (this.state.isLoading) {
            return (<Spinner color='green' />);
        } else {
            return this.renderSuggestionList();
        }
    }
    _back() {
        setTimeout(() => {
            this.props.navigation.goBack();
        }, 1000);
    }
    _goHome() {
        setTimeout(() => {
            this.props.navigation.navigate('Home');
        }, 1000);
    }


    render() {
        return (
            <Container>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <SubHeader text="Search from businesses in my favourites" />
                <View style={styles.searchContainer}>
                    <Item regular style={{ height: 35, borderRadius: 5 }} >
                        <TextInput placeholder='Enter the name of the business here' returnKeyType="done"
                            autoFocus={true}
                            blurOnSubmit={false}
                            ref={ref => this.ref = ref}
                            style={styles.textInput}
                            onChangeText={(text) => this.searchFav(text)} />
                    </Item>
                </View>
                <View style={{ marginLeft: 8, marginRight: 8 }}>
                    {this._renderSpinner()}
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    textInput: {
        flex: 1,
        color: Colors.deepPurple,
        textAlign: 'center',
        fontSize: FontSize.large,
        alignSelf: 'center',
        padding: 0,
    },
    searchContainer: {
        paddingLeft: 8,
        paddingRight: 8,
        marginTop: 10
    },
    listItem: {
        backgroundColor: Colors.white,
        padding: 8,
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    listItemActive: {
        backgroundColor: Colors.primary,
        padding: 8, flex: 1,
        opacity: 0,
    },
    itemHeader: {
        flex: 1,
        fontSize: FontSize.heading,
        color: Colors.deepPurple,
        textAlign: 'left'
    },
    itemHeaderActive: {
        fontSize: FontSize.heading,
        color: Colors.white,
        textAlign: 'left'
    },
    itemHeaderActive2: {
        fontSize: FontSize.heading,
        color: '#585858',
        textAlign: 'left'
    },
    itemText: {
        fontSize: FontSize.medium,
        color: Colors.lessDarkGrey,
        textAlign: 'left',
        flex: 1,
    },
    itemTextActive: {
        fontSize: FontSize.medium,
        color: Colors.white,
        textAlign: 'left'
    }

});
