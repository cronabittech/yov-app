import { Container, Content, Footer, Text, Header } from 'native-base';
import React, { PureComponent } from "react";
import { Pressable, StyleSheet, TextInput, View, StatusBar, Dimensions } from 'react-native';
import { deleteWithLogin, getWithLogin, postWithLogin } from "../api/api";
import Colors from '../config/colors';
import FontSize from '../config/fontSize';
import {
    Button,
    ModelLayout
} from '../component';

const windowHeight = Dimensions.get('window').height;


const VibrateStart = () => {
}

const VibrateStop = () => {
}

export default class ProfileNoteScreen extends PureComponent {

    state = {
        title: 'Add private notes',
        value: '',
        height: 40,
        isEditMode: 1,
        isUpdate: false,
        note: undefined,
        addFocus: false,
        editFocus: false,
        removeNoteAlert: false
    }

    updateSize = (height) => {
        this.setState({
            height
        });
    }

    componentDidMount() {
        if (this.props.userNote) {
            this.setState({
                value: this.props.userNote.note, note: this.props.userNote,
                isEditMode: 0, title: 'My private notes',
            });
        }
    }

    onAddFocus() {
        this.setState({ addFocus: true });
    }

    onAddBlur() {
        this.setState({ addFocus: false, title: this.state.value ? "Adding private notes" : "Add private notes" });
    }

    onEditFocus() {
        this.setState({ editFocus: true });
    }

    onEditBlur() {
        this.setState({ editFocus: false, title: this.state.value ? "Editing private notes" : "Edit private notes" });
    }

    _removeProfileAlert() {
        return (
            <ModelLayout
                centeredViewStyle={{ marginTop: 35 }}
                visible={this.state.removeNoteAlert}>
                <Text style={{ textAlign: 'center' }}>Are you sure you want to delete your notes?</Text>
                <View style={{ width: 150, flexDirection: 'row', marginTop: 15, justifyContent: 'space-between' }}>
                    <Button type="textButton" text="Yes" onPress={() => this.deleteData()} />
                    <Button type="textButton" text="No" onPress={() => this.setState({ removeNoteAlert: false })} />
                </View>
            </ModelLayout>
        );
    }
    async addData() {
        var data = {
            note: this.state.value,
            com_id: this.props.company.id,
        };
        var response = await getWithLogin('user/state/' + "note");
        if (this.state.isUpdate) {
            var response = await postWithLogin('update/note/' + this.state.note.id, data);
            this.setState({ isEditMode: 0, isUpdate: false, title: 'My private notes', note: response.data });
            this.props.updateUserNote(response.data);
        } else {
            var response = await postWithLogin('add/note', data);
            this.setState({ isEditMode: 0, title: 'My private notes', note: response.data });
            this.props.updateUserNote(response.data);
        }
    }

    async deleteData() {
        var response = await deleteWithLogin('delete/note/' + this.state.note.id);
        this.setState({ isEditMode: true, title: 'Add private notes', value: "" });
        this.props.updateUserNote(null);
        this.setState({ removeNoteAlert: false });
    }

    _renderContent() {
        if (this.state.isEditMode == 1) {
            return (
                <View >
                    <View style={{ marginTop: 0 }}>
                        <TextInput
                            multiline={true}
                            autoFocus={true}
                            onFocus={() => this.onAddFocus()}
                            onBlur={() => this.onAddBlur()}
                            style={[styles.inputStyle, { borderColor: this.state.addFocus ? Colors.deepPurple : Colors.lightGrey }]}
                            placeholder="Enter your notes here"
                            onChangeText={(value) => this.setState({ value, title: 'Adding private notes?' })}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, justifyContent: this.state.value.length > 0 ? 'space-between' : 'space-around' }}>
                        {
                            this.state.value.length > 0 ?
                                <Pressable style={({ pressed }) => [
                                    pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 100 }
                                ]} onPress={() => this.addData()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                                    <Text style={{
                                        color: Colors.deepPurple, textAlign: 'center',
                                        textTransform: 'capitalize', fontSize: FontSize.heading
                                    }}>Done</Text>
                                </Pressable>
                                :
                                null
                        }
                        <Pressable style={({ pressed }) => [
                            pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 100 }
                        ]} onPress={() => this.props.close()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                            <Text style={{
                                color: Colors.deepPurple, textAlign: 'center',
                                textTransform: 'capitalize', fontSize: FontSize.heading
                            }}>Close</Text>
                        </Pressable>
                    </View>
                </View>
            );
        } else if (this.state.isEditMode == 2) {
            return (
                <View >
                    <View style={{ marginTop: 0 }}>
                        <TextInput
                            multiline={true}
                            autoFocus={true}
                            value={this.state.value}
                            onFocus={() => this.onEditFocus()}
                            onBlur={() => this.onEditBlur()}
                            style={[styles.inputStyle, { borderColor: this.state.editFocus ? Colors.deepPurple : Colors.lightGrey }]}
                            placeholder="Enter your notes here"
                            onChangeText={(value) => this.setState({ value, title: 'Editing private notes?' })}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, justifyContent: this.state.value.length > 0 ? 'space-between' : 'space-around' }}>
                        {
                            this.state.value.length > 0 ?
                                <Pressable style={({ pressed }) => [
                                    pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 100 }
                                ]} onPress={() => this.setState({ removeNoteAlert: true })} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                                    <Text style={{
                                        color: Colors.deepPurple, textAlign: 'center',
                                        textTransform: 'capitalize', fontSize: FontSize.heading
                                    }}>Delete</Text>
                                </Pressable>
                                :
                                null
                        }
                        {
                            this.state.value.length > 0 ?
                                <Pressable style={({ pressed }) => [
                                    pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 100 }
                                ]} onPress={() => this.addData()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                                    <Text style={{
                                        color: Colors.deepPurple, textAlign: 'center',
                                        textTransform: 'capitalize', fontSize: FontSize.heading
                                    }}>Done</Text>
                                </Pressable>
                                :
                                null
                        }
                        <Pressable style={({ pressed }) => [
                            pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 100 }
                        ]} onPress={() => this.props.close()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                            <Text style={{
                                color: Colors.deepPurple, textAlign: 'center',
                                textTransform: 'capitalize', fontSize: FontSize.heading
                            }}>Close</Text>
                        </Pressable>
                    </View>
                </View>
            );
        } else {
            return (
                <View >
                    <Text onPress={() => this.setState({ isEditMode: 2, isUpdate: true, title: 'Edit private notes' })} style={{ color: Colors.dark, marginTop: 0, height: windowHeight, fontSize: 16 }}>
                        {this.state.value}
                    </Text>
                </View>
            );
        }
    }

    render() {
        return (<Container>
            <View style={{ elevation: 2, marginBottom: 0, backgroundColor: '#F5F5F5', height: 30, justifyContent: 'center' }} >
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ color: Colors.primary, fontSize: FontSize.large }}>{this.state.title}</Text>
                </View>
            </View>
            <Header style={styles.headerStyle}>
                <View style={{ justifyContent: 'center', }}>
                    <Text style={{ textAlign: 'center', color: '#585858', fontSize: FontSize.heading, fontWeight: 'bold', }}>{this.props.company.name}</Text>
                    <Text style={{ textAlign: 'center', color: Colors.lightGrey, fontSize: FontSize.medium, fontWeight: 'bold', }}>{this.props.company.formatted_address}</Text>
                </View>
            </Header>
            <StatusBar
                backgroundColor="#fff" barStyle="dark-content" />
            <Content padder>

                {
                    this._renderContent()
                }
            </Content>
            {
                this.state.isEditMode == 0 ?
                    <Footer style={styles.footerStyle}>
                        <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
                            <Pressable style={({ pressed }) => [
                                pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 110 }
                            ]} onPress={() => this.setState({ removeNoteAlert: true })} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                                <Text style={{
                                    color: Colors.deepPurple, textAlign: 'center',
                                    textTransform: 'capitalize', fontSize: FontSize.heading
                                }}>Delete</Text>
                            </Pressable>
                            <Pressable style={({ pressed }) => [
                                pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 110 }
                            ]} onPress={() => this.setState({ isEditMode: 2, isUpdate: true, title: 'Edit private notes' })} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                                <Text style={{
                                    color: Colors.deepPurple, textAlign: 'center',
                                    textTransform: 'capitalize', fontSize: FontSize.heading
                                }}>Edit</Text>
                            </Pressable>
                            <Pressable style={({ pressed }) => [
                                pressed ? styles.favButtonActive : styles.favButton, { backgroundColor: Colors.white, width: 110 }
                            ]} onPress={() => this.props.close()} onPressIn={() => VibrateStart()} onPressOut={() => VibrateStop()}>
                                <Text style={{
                                    color: Colors.deepPurple, textAlign: 'center',
                                    textTransform: 'capitalize', fontSize: FontSize.heading
                                }}>Close</Text>
                            </Pressable>
                        </View>
                    </Footer>
                    :
                    null
            }
            {
                this._removeProfileAlert()
            }
        </Container>);
    }
}

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: Colors.white,
        flexDirection: 'column',
        height: 60,
        elevation: 0,
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 10,
        shadowOpacity: 0,
        borderWidth: 0,
        borderBottomWidth: 0
    },
    footerStyle: {
        backgroundColor: Colors.white, height: 115,
        paddingLeft: 10, paddingRight: 10,
        paddingTop: 10, shadowOpacity: 0,
        borderTopWidth: 0,
        elevation: 0
    },
    inputStyle: {
        fontSize: 16,
        borderWidth: 0,
        borderRadius: 5,
        color: Colors.darkGrey,
        borderColor: Colors.lightGrey,
        minHeight: 45,
        maxHeight: 200,
        padding: 0
    },
    favButtonActive: {
        opacity: 0,
        elevation: 1,
        borderRadius: 5,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 40,
    },
    favButton: {
        elevation: 1,
        borderRadius: 5,
        borderWidth: 0.3,
        borderColor: "#CDCDCD",
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        height: 60,
        backgroundColor: Colors.white,
        borderRadius: 5, shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
    }
});