import {Container, Content, Text} from 'native-base';
import React from 'react';
import {StyleSheet} from 'react-native';
import {TermCondition} from '../component';
import Colors from '../config/colors';
import {doMail} from '../config/util';

function TermsConditionViewScreen() {
  return (
    <Container>
      <Text
        style={{
          backgroundColor: '#008000',
          color: '#fff',
          textAlign: 'center',
          padding: 5,
        }}>
        Terms &amp; conditions
      </Text>
      <Content padder>
        <TermCondition></TermCondition>
      </Content>
    </Container>
  );
}

const style = StyleSheet.create({
  m5: {
    marginTop: 10,
    textAlign: 'justify',
  },
  h1: {
    marginTop: 15,
    fontWeight: 'bold',
    fontSize: 18,
  },
  link: {
    color: Colors.darkBlue,
  },
});

export default TermsConditionViewScreen;
