import { Container, Text } from 'native-base';
import React, { PureComponent } from "react";
import { ScrollView } from 'react-native';


class HistoryScreen extends PureComponent {

    render() {
        return (
            <ScrollView>
                <Container style={{ flex: 1, flexDirection: 'column', justifyContent:'center', height: '100%' }}>
                  <Text>You don't have history</Text> 
                </Container>
            </ScrollView>

        );
    }
}
export default HistoryScreen;



