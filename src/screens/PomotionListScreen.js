import {Container, Spinner, Content, Card, Badge} from 'native-base';
import React, {useEffect, useState} from 'react';
import {FlatList, View, Text, Pressable} from 'react-native';
import {getWithLogin} from '../api/api';
import {SubHeader} from '../component';
import Colors from '../config/colors';
import moment from 'moment';

function PomotionListScreen(props) {
  const [promotion, setPromotion] = useState('');
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    props.navigation.addListener('focus', () => {
      getPromotion();
    });
  }, []);
  
  const momentFormateDate = (date) => {
    if (date) {
      return moment(date + '', 'YYYY-MM-DD').format('DD/MM/YYYY');
    } else {
      return moment().format('DD/MM/YYYY');
    }
  };

  const getPromotion = async () => {
    try {
      setLoading(true);
      const response = await getWithLogin('usr/promotion/result');
      console.log(response);
      setPromotion(response.data);
      setLoading(false);
    } catch (e) {
      console.log(e.response);
    }
  };

  const renderItem = ({item}) => {
    return (
      <Card style={{marginTop: 5, marginBottom: 5}}>
        <Pressable
          onPress={() => props.navigation.navigate('PromotionMessage', {item})}>
          <View style={{padding: 10}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>
                Sent at: {momentFormateDate(item.prom_result_updated_at)}
              </Text>
              {item.isNewReply > 0 ? (
                <Badge primary>
                  <Text style={{fontWeight: 'bold', color: Colors.white}}>
                    {item.totalMessage} Replies
                  </Text>
                </Badge>
              ) : (
                <Text style={{fontWeight: 'bold', color: Colors.darkBlue}}>
                  {item.totalMessage} Replies
                </Text>
              )}
            </View>
            <Text numberOfLines={1} style={{fontWeight: 'bold'}}>
              {item.name}
            </Text>
            <Text numberOfLines={2} style={{color: Colors.lightGrey}}>
              {item.formatted_address}
            </Text>
            <Text style={{marginTop: 5, fontSize: 16}}>{item.question}</Text>
            <Text style={{marginTop: 5, color: Colors.primary, fontSize: 16}}>
              A.{item.full_answer}
            </Text>
          </View>
        </Pressable>
      </Card>
    );
  };
  return (
    <Container>
      <SubHeader text="Promotion submitted" />
      {isLoading ? (
        <Spinner color="green" style={{alignSelf: 'center'}} />
      ) : (
        <Content>
          <FlatList
            contentContainerStyle={{marginBottom: 100}}
            keyboardShouldPersistTaps="always"
            data={promotion}
            keyExtractor={(item) => item.voucher_id}
            renderItem={renderItem}
          />
        </Content>
      )}
    </Container>
  );
}

export default PomotionListScreen;
