import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {Animated, Image, Pressable, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../config/colors';
import {
  ActivityHistoryScreen,
  CompanyDetailScreen,
  CompanyHistoryScreen,
  CreatedScreen,
  FavouriteHistoryScreen,
  FavouriteScreen,
  FeedbackHistoryScreen,
  ForgetPasswordScreen,
  HistoryScreen,
  Home2Screen,
  LoginScreen,
  MessageScreen,
  MyStarRating,
  FeedbackReviewScreen,
  QrScanScreen,
  RatedScreen,
  AddFavoriteNoteScreen,
  TermsConditionViewScreen,
  EditFavoriteNoteScreen,
  RecomendedScreen,
  EditProfileNoteScreen,
  RecommendationHistoryScreen,
  RegisterScreen,
  PromotionDetailsScreen,
  SearchHistoryScreen,
  SearchScreen,
  SettingScreen,
  SplashScreen,
  StarRatingScreen,
  SubmitFeedbackScreen,
  ProfileNoteHistoryScreen,
  FeedbackMessageScreen,
  YovScreen,
  SearchFromFavoriteScreen,
  AddProfileNoteScreen,
  MessageConverisonScreen,
  SearchFromMyNotes,
  SearchFromMyRecommned,
  SearchFromMyActivities,
  SearchFromMyStarRatings,
  PromotionSurveyScreen,
  VoucherListScreen,
  RedeemVoucherScreen,
  PomotionListScreen,
  PromotionMessageScreen,
  PromotionSearchScreen,
  ChangePasswordScreen,
  AcceptTermsConditionScreen,
} from '../screens';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const commonOption = ({navigation, route}) => ({
  headerShown: true,
  headerTintColor: '#000',
  headerColor: '#FFF',
  headerStyle: {
    backgroundColor: '#fff',
  },
  headerTitleAlign: 'center',
  headerBackTitle: () => null,
  HeaderMode: 'float',
  headerTitle: (props) => (
    <LogoTitle
      onPress={() =>
        setTimeout(() => {
          navigation.navigate('Home', {screen: 'Home'});
        }, 50)
      }
    />
  ),
  headerBackImage: (props) => (
    <Image
      style={{tintColor: Colors.deepPurple, width: 45, height: 45}}
      source={require('../assets/img/back.png')}></Image>
  ),
});

const config = {
  animation: '',
  config: {
    stiffness: 1000,
    damping: 500,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};
const StackSearch = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {backgroundColor: '#5eb44d'},
        headerTintColor: '#fff',
      }}>
      <Stack.Screen
        name="Search"
        component={SearchScreen}
        options={commonOption}
      />
    </Stack.Navigator>
  );
};

const StackHome = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {backgroundColor: '#5eb44d'},
        headerTintColor: '#fff',
      }}>
      <Stack.Screen
        name="Home"
        component={Home2Screen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const StackSetting = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {backgroundColor: '#5eb44d'},
        headerTintColor: '#fff',
      }}>
      <Stack.Screen
        name="setting"
        component={SettingScreen}
        options={commonOption}
      />
    </Stack.Navigator>
  );
};
const StackQr = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {backgroundColor: '#5eb44d'},
        headerTintColor: '#fff',
      }}>
      <Stack.Screen
        name="Qr Scan"
        component={QrScanScreen}
        options={commonOption}
      />
    </Stack.Navigator>
  );
};
const StackYov = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {backgroundColor: '#5eb44d'},
        headerTintColor: '#fff',
      }}>
      <Stack.Screen name="Yov" component={YovScreen} options={commonOption} />
    </Stack.Navigator>
  );
};

function Home() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let name;

          if (route.name === 'Home') {
            name = 'home';
            return <Icon name={name} size={size} color={color} />;
          } else if (route.name === 'Search') {
            name = 'search';
            return <Icon name={name} size={size} color={color} />;
          } else if (route.name === 'Settings') {
            name = 'cog';
            return <Icon name={name} size={size} color={color} />;
          } else if (route.name === 'Scan') {
            name = 'qrcode';
            return <Icon name={name} size={size} color={color} />;
          } else if (route.name === 'Yov') {
            var style = focused
              ? {fontSize: 23, color: '#5eb44d'}
              : {fontSize: 23, color: 'gray'};
            return <Text style={style}>Yov</Text>;
          }
          // You can return any component that you like here!
        },
      })}
      tabBarOptions={{
        activeTintColor: '#5eb44d',
        inactiveTintColor: 'gray',
        showLabel: false,
      }}>
      <Tab.Screen
        name="Home"
        component={StackHome}
        options={{
          title: 'home',
        }}
      />
      <Tab.Screen
        name="Search"
        component={StackSearch}
        options={{
          title: 'Search',
        }}
      />
      <Tab.Screen
        name="Scan"
        component={StackQr}
        options={{
          title: 'Qr Scan',
        }}
      />
      <Tab.Screen
        name="Settings"
        component={StackSetting}
        options={{
          title: 'Settings',
        }}
      />
      <Tab.Screen
        name="Yov"
        component={StackYov}
        options={{
          title: 'Yov',
        }}
      />
    </Tab.Navigator>
  );
}

const forSlide = ({current, next, inverted, layouts: {screen}}) => {
  const progress = Animated.add(
    current.progress.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    }),
    next
      ? next.progress.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1],
          extrapolate: 'clamp',
        })
      : 0,
  );

  return {
    cardStyle: {
      transform: [
        {
          translateX: Animated.multiply(
            progress.interpolate({
              inputRange: [0, 1, 2],
              outputRange: [
                screen.width, // Focused, but offscreen in the beginning
                0, // Fully focused
                screen.width * -0.3, // Fully unfocused
              ],
              extrapolate: 'clamp',
            }),
            inverted,
          ),
        },
      ],
    },
  };
};

const LogoTitle = (props) => {
  return (
    <Pressable
      style={({pressed}) => [pressed ? {opacity: 0} : {opacity: 1}]}
      onPress={props.onPress}>
      <Image
        resizeMethod="resize"
        style={{
          width: 120,
          height: 42,
          alignSelf: 'center',
          marginBottom: Platform.OS === 'ios' ? 10 : 0,
        }}
        source={require('../assets/img/logo.png')}
      />
    </Pressable>
  );
};

export function ScreenRender() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="splash"
        screenOptions={{headerStyle: {backgroundColor: '#5eb44d'}}}>
        <Stack.Screen
          name="splash"
          component={SplashScreen}
          options={(commonOption, {headerShown: false})}
        />
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={(commonOption, {headerShown: false})}
        />
        <Stack.Screen
          name="terms"
          component={AcceptTermsConditionScreen}
          options={(commonOption, {headerShown: false})}
        />
        <Stack.Screen
          name="Register"
          component={RegisterScreen}
          options={(commonOption)}
        />
        <Stack.Screen
          name="Home"
          component={Home}
          options={(commonOption, {headerShown: false})}
        />
        <Stack.Screen
          name="reset-password"
          component={ForgetPasswordScreen}
          options={(commonOption, {headerShown: false})}
        />
        <Stack.Screen
          name="Recommend"
          component={CompanyDetailScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="Favourites"
          component={FavouriteScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="History"
          component={HistoryScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="Rated"
          component={RatedScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="Recommended"
          component={RecomendedScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="Created"
          component={CreatedScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="Feedback"
          component={SubmitFeedbackScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="StarRating"
          component={StarRatingScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="CompanyHistory"
          component={CompanyHistoryScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="MyStarRating"
          component={MyStarRating}
          options={commonOption}
        />
        <Stack.Screen
          name="SearchHistory"
          component={SearchHistoryScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="Message"
          component={MessageScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="FavouriteHistory"
          component={FavouriteHistoryScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="FeedbackHistory"
          component={FeedbackHistoryScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="RecommendationHistory"
          component={RecommendationHistoryScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="ActivityHistory"
          component={ActivityHistoryScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="ProfileNoteHistory"
          component={ProfileNoteHistoryScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="SearchFromFavorite"
          component={SearchFromFavoriteScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="AddProfileNote"
          component={AddProfileNoteScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="EditProfileNote"
          component={EditProfileNoteScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="AddFavoriteNote"
          component={AddFavoriteNoteScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="EditFavoriteNote"
          component={EditFavoriteNoteScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="SearchFromMyNotes"
          component={SearchFromMyNotes}
          options={commonOption}
        />
        <Stack.Screen
          name="SearchFromMyRecommned"
          component={SearchFromMyRecommned}
          options={commonOption}
        />
        <Stack.Screen
          name="SearchFromMyActivities"
          component={SearchFromMyActivities}
          options={commonOption}
        />
        <Stack.Screen
          name="SearchFromMyStarRatings"
          component={SearchFromMyStarRatings}
          options={commonOption}
        />
        <Stack.Screen
          name="FeedbackReview"
          component={FeedbackReviewScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="MessageConverison"
          component={MessageConverisonScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="PromotionDetails"
          component={PromotionDetailsScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="FeedbackMessage"
          component={FeedbackMessageScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="PromotionSurvey"
          component={PromotionSurveyScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="VoucherList"
          component={VoucherListScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="RedeemVoucher"
          component={RedeemVoucherScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="PomotionList"
          component={PomotionListScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="PromotionMessage"
          component={PromotionMessageScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="PromotionSearch"
          component={PromotionSearchScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="ChangePassword"
          component={ChangePasswordScreen}
          options={commonOption}
        />
        <Stack.Screen
          name="TermsConditionView"
          component={TermsConditionViewScreen}
          options={commonOption}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
