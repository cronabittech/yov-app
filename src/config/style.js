import { StyleSheet, Dimensions } from "react-native";
import Colors from '../config/colors';

const { width } = Dimensions.get("window");

export default StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor:'white',
    paddingLeft:16,
    paddingRight:16,
  },
  mt20:{
    marginTop:20,
    height:35,
    borderRadius:5
  }, buttonActive: {
    opacity: 0,
    elevation: 1,
    borderRadius: 3,
    marginTop: 20,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    height: 35
},
button: {
    elevation: 1,
    borderRadius: 3,
    marginTop: 20,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    height: 35
}
});
