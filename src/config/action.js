export default Action = {

    ADD_STAR_RAING:'add star rating',
    ADD_STAR_RAING_FAIL:'add star rating fail',
    ADD_STAR_RAING_SUCCESS:'add star rating success',
    ADD_STAR_RAING_RESET:'add star rating reset',

    GET_COMP :'com detail',
    GET_COMP_SUCCESS:'com detail success',
    GET_COMP_FAIL:'com detail fail',
    GET_COMP_LIST :'com list',
    GET_COMP_LIST_SUCCESS:'com list success',
    GET_COMP_LIST_FAIL:'com list fail',
    REGISTER :'register',
    REGISTER_SUCCESS:'register success',
    REGISTER_FAIL:'register fail',
    SEARCH :'search',
    SEARCH_SUCCESS:'search success',
    SEARCH_FAIL:'search fail',
    LOGIN :'login',
    LOGIN_SUCCESS:'login success',
    LOGIN_FAIL:'login fail',
    RESETP :'resetp',
    RESETP_SUCCESS:'resetp success',
    RESETP_FAIL:'resetp fail',
    FORGET_PASSWORD :'forget passowrd',
    LOGIN :'login'
}