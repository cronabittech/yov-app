export function changeProfileTitle(title) {
    var newTitle = "Profile";
    switch (title) {
        case "Mini report":
            newTitle = "Mini report";
            break;
        case "Recommend us":
            newTitle = "Recommend us?";
            break;
        case "Star rate us":
            newTitle = "Star rate us";
            break;
        case "Edit / Delete star ratings":
            newTitle = "Edit / Delete star ratings";
            break;
        case "Send us feedback":
            newTitle = "Feedback / comments";
            break;
        case "Add us to favourites":
            newTitle = "Added to favourites";
            break;
        case "Recommendations":
            newTitle = "My recommendations";
            break;
        case "My star ratings":
            newTitle = "Star ratings";
            break;
        case "View in favourites":
            newTitle = "View in favourites";
            break;
        case "Already added to favourites":
            newTitle = "Already added to favourites";
            break;
        case "Added to your favourites":
            newTitle = "Added to your favourites";
            break;
        case "Already recommended by you":
            newTitle = "Already recommended by you";
            break;
        case "Already star rated by you":
            newTitle = "Already star rated by you";
            break;
        case "Recommended ?":
            newTitle = "Recommended ?";
            break;
        case "Add contact details":
            newTitle = "Add / Edit favourite contact details";
            break;
        case "My activities":
            newTitle = "Activities";
            break;
        case "My feedback":
            newTitle = "My feedback";
            break;
        case "Add Note":
            newTitle = "Profile notes";
            break;
        case "Add profile notes":
            newTitle = "Add profile notes";
            break;
        case "Edit profile notes":
            newTitle = "Edit profile notes";
            break;
        case "Add favourite notes":
            newTitle = "Add favourite notes";
            break;
        case "Favourite notes":
            newTitle = "Favourite notes";
            break;
        default:
            newTitle = "Profile";
            break;
    }
    return newTitle;
}