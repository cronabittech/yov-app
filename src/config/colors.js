 const Colors = {
    darkGrey:'#7e7e7e',
    lessDarkGrey:'#a4a4b0',
    lightGrey:'#bdbdbd',
    lightGrey2:'#F5F5F5',
    orange:'#ff9800',
    primary:'#008000',
    white:'#fff',
    deepPurple:'#465f8e',
    darkBlue:'#465f8e',
    dark:'#666666',
    red:'#f9450d'
    
}; 

export default  Colors;