const FontSize = {
    heading:16,
    large:15,
    semi_large:14,
    extraLarge:18,
    medium:13,
    small:11,
    
};

export default  FontSize;