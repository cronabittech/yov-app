import {Linking, Alert, Platform} from 'react-native';

export const callNumber = (phone) => {
  console.log("util",phone);
  let phoneNumber = '';
  if (Platform.OS !== 'android') {
    phoneNumber = `telprompt:${phone}`;
  } else {
    phoneNumber = `tel:${phone}`;
  }

  Linking.canOpenURL(phoneNumber)
    .then((supported) => {
      if (!supported) {
        Alert.alert('Phone number is not available');
      } else {
        return Linking.openURL(phoneNumber);
      }
    })
    .catch();
};

export const doMail = (to) => {
  let url = `mailto:${to}`;
  Linking.canOpenURL(url)
    .then((supported) => {
      if (!supported) {
        Alert.alert('Email address is not available');
      } else {
        return Linking.openURL(url);
      }
    })
    .catch();
};

export const getType = (type)=>{
  if(type === null || type === "" ||  type === undefined ){
    return "To be confirmed";
  }else{
    if(type.includes(',')){
      return type.split(",")[0];
    }else{
      return type;
    }
  }
}
