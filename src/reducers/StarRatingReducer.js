import Action from '../config/action';

// Initial State
const initialState = {
    isLoading: false,
    error: false,
    result: null,
    type: '',
    message:null,
};


const StarRatingReducer = (state = initialState, action) => {
    switch (action.type) {
        case Action.ADD_STAR_RAING: {
            return {
                ...state,
                error: false,
                type: action.type,
                isLoading: true,
            };
        }
        case Action.ADD_STAR_RAING_SUCCESS: {
            return {
                ...state,
                error: false,
                isLoading: false,
                type: action.type,
                message: action.value
            };
        } case Action.ADD_STAR_RAING_FAIL: {
            return {
                ...state,
                error: true,
                isLoading: false,
                type: action.type,
                message: action.value
            };
        } 
        case Action.ADD_STAR_RAING_RESET:{
            return {
                ...state,
                type: action.type,
            };
        }
        default: {
            return state;
        }
    }
};

// Exports
export default StarRatingReducer;