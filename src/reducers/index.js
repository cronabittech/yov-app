// Imports: Dependencies
import { combineReducers } from 'redux';

// Imports: Reducers
import registerReducer from './registerReducer';
import  loginReducer from './loginReducer';
import  resetPasswordReducer from './resetPasswordReducer';
import searchReducer  from './searchRedcers';
import CompanyDetailReducer  from './CompanyDetailReducer';
import StarRatingReducer  from './StarRatingReducer';

// Redux: Root Reducer
const rootReducer = combineReducers({
  register: registerReducer,
  login: loginReducer,
  resetPassword :resetPasswordReducer,
  search :searchReducer,
  companyDetail :CompanyDetailReducer,
  StarRating :StarRatingReducer,
});

// Exports
export default rootReducer;