import Action from '../config/action';

// Initial State
const initialState = {
  isLoading: false,
  error: false,
  message: null,
  type:'',
};


const registerReducer = (state = initialState, action) => {
  switch (action.type) {
    case Action.REGISTER: {
      return {
        ...state,
        error: false,
        type:action.type,
        isLoading: true,
      };
    }
    case Action.REGISTER_SUCCESS: {
      return {
        ...state,
        error: false,
        isLoading: false,
        type:action.type,
        message: action.value
      };
    } case Action.REGISTER_FAIL: {
      return {
        ...state,
        error: true,
        isLoading: false,
        type:action.type,
        message: action.value
      };
    }
    default: {
      return state;
    }
  }
};

// Exports
export default registerReducer;