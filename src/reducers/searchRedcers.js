import Action from '../config/action';

// Initial State
const initialState = {
    isLoading: false,
    error: false,
    result: null,
    type: '',
    message:null,
    companyList:null
};


const searchReducer = (state = initialState, action) => {
    switch (action.type) {
        case Action.SEARCH: {
            return {
                ...state,
                error: false,
                type: action.type,
                isLoading: true,
            };
        }
        case Action.SEARCH_SUCCESS: {
            return {
                ...state,
                error: false,
                isLoading: false,
                type: action.type,
                companyList:null,
                result: action.value
            };
        } case Action.SEARCH_FAIL: {
            return {
                ...state,
                error: true,
                isLoading: false,
                type: action.type,
                companyList:null,
                message: action.value
            };
        } case Action.GET_COMP_LIST: {
            return {
                ...state,
                error: false,
                type: action.type,
                isLoading: true,
            };
        }
        case Action.GET_COMP_LIST_SUCCESS: {
            return {
                ...state,
                error: false,
                isLoading: false,
                type: action.type,
                companyList: action.value
            };
        } case Action.GET_COMP_LIST_FAIL: {
            return {
                ...state,
                error: true,
                isLoading: false,
                type: action.type,
                message: action.value
            };
        }
        default: {
            return state;
        }
    }
};

// Exports
export default searchReducer;