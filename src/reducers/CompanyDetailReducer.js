import Action from '../config/action';

// Initial State
const initialState = {
  isLoading: false,
  error: false,
  message: null,
  company:null,
  type:'',
};


const CompanyDetailReducer = (state = initialState, action) => {
  switch (action.type) {
    case Action.GET_COMP: {
      return {
        ...state,
        error: false,
        type:action.type,
        isLoading: true,
      };
    }
    case Action.GET_COMP_SUCCESS: {
      return {
        ...state,
        error: false,
        isLoading: false,
        type:action.type,
        company: action.value
      };
    } case Action.GET_COMP_FAIL: {
      return {
        ...state,
        error: true,
        isLoading: false,
        type:action.type,
        message: action.value
      };
    }
    default: {
      return state;
    }
  }
};

// Exports
export default CompanyDetailReducer;