import React, {PureComponent} from 'react';
import {
  AppState,
  Button,
  StyleSheet,
  Text,
  View,
  Image,
  Pressable,
} from 'react-native';
import {Provider} from 'react-redux';
import {ScreenRender} from './src/navigation/navigation';
import {store} from './src/store/store';
import {getToken} from './src/config/global';
import {getWithLogin} from './src/api/api';
import messaging from '@react-native-firebase/messaging';

messaging().setAutoInitEnabled(true);

messaging().registerDeviceForRemoteMessages();



class App extends PureComponent {
  state = {
    isActive: true,
  };
  componentDidMount() {
    AppState.addEventListener('change', (nextAppState) => {
      console.log('Site ', this.props);
      if (getToken() != "" && getToken() != null && getToken() != undefined) {
        this.getUserState();
        console.log("Checking app");
      }
    });
    // setTimeout(() => {
    //   this.setState({ isActive: false });
    // }, 10);
  }

  async getUserState() {
    try {
      var response = await getWithLogin('user/state');
    } catch (error) {
      this.setState({isActive: false});
    }
  }

  render() {
    return (
      <Provider store={store}>
        {this.state.isActive ? (
          <ScreenRender></ScreenRender>
        ) : (
          <View
            style={{
              alignSelf: 'stretch',
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
            }}>
            <Pressable
              onPress={() => this.setState({isActive: true})}
              style={{
                width: '100%',
                height: '100%',
                flexDirection:'column',
                alignContent:'center',
                alignItems:'center',
                justifyContent: 'center',
                backgroundColor: '#CDCDCD',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 150,
                  fontWeight: 'bold',
                  color: '#FFF',
                }}>
                YOV
              </Text>
            </Pressable>
            {/* <Image style={[style.logo]}
                source={require('./src/assets/img/logo.png')}
              /> 
            <Text style={{ textAlign: 'center', fontSize: 19,color:"#282c34", padding: 10 }}>Due to long period of inactivity. you are logged out automatically please click "Start App" for restart the app </Text>
             <Pressable onPress={() => this.setState({ isActive: true })} style={({ pressed }) => [
                pressed ? style.buttonActive : style.button
              ]} >
                <Text style={{ fontWeight: 'bold', color: "#fff", textAlign: 'center', textTransform: 'none' }}>Start app</Text>
              </Pressable> */}
          </View>
        )}
      </Provider>
    );
  }
}

export default App;
const style = StyleSheet.create({
  logo: {
    width: 250,
    height: 120,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  buttonActive: {
    opacity: 0,
    elevation: 1,
    borderRadius: 3,
    marginTop: 20,
    backgroundColor: '#008000',
    justifyContent: 'center',
    height: 35,
    width: 200,
  },
  button: {
    elevation: 1,
    borderRadius: 3,
    marginTop: 20,
    backgroundColor: '#008000',
    justifyContent: 'center',
    height: 35,
    width: 200,
  },
});
